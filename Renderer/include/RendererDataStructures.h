#pragma once
#include "../../Core/include/globalDefs.h"
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Mesh;
class Material;
class Shader;
class Texture;

/// <summary>
/// basic information about renderer
/// </summary>
struct RendererModuleCreateInfo
{
    /// <summary>
    /// value of color for clearing scene
    /// </summary>
    glm::vec3 clearColor;
    /// <summary>
    /// is face culling enabled
    /// </summary>
    bool cullFace;
    /// <summary>
    /// is depth test enabled
    /// </summary>
    bool depthTest;
    /// <summary>
    /// is wire frame mode enabled
    /// </summary>
    bool wireframeMode;
    /// <summary>
    /// should be gamma correction
    /// </summary>
    bool gammaCorrection;
    /// <summary>
    /// mode of face culling
    /// </summary>
    uint cullFaceMode;
    uint cullFrontFace;
    uint clearFlags;
};

enum class RenderMode
{
    NORMAL,
    SHADOW_MAPPING,
    CUBEMAP_MAPPING,
    SSAO,
};

/// <summary>
/// base rendering packet class
/// </summary>
struct RenderPacket
{
    RenderPacket(Mesh* mesh, Material* material) : mesh(mesh), material(material) {}
    RenderPacket() = default;
    virtual ~RenderPacket() = default;

    virtual void render(glm::mat4& VP, RenderMode mode, Texture* directionalShadowMap, Texture* spotShadowMaps[], Texture* irradianceMap, Texture* prefilteredMap, Texture* brdf) = 0;
    virtual void renderWithShader(Shader* shader, glm::mat4& VP) = 0;

    Mesh* mesh;
    Material* material;
};