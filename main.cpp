#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "Renderer/include/Shader.h"
#include "Core/include/globalDefs.h"
#include "Renderer/include/Mesh.h"
#include "Renderer/include/MeshDataStructures.h"
#include "Core/include/Core.h"
#include "Core/include/Message.inl"

int main()
{
    Core core;
    if (core.init() == 0)
    {
        core.mainLoop();
    }
    return 0;
}

