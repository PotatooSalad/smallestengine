#pragma once
#include "../../Base/include/Component.inl"
#include <glm/vec3.hpp>
#include "../../../Core/include/globalDefs.h"
#include "CameraStructures.h"

enum class LightType : uint
{
	Directional, Spot
};

struct Light : public Component
{
	Light() = default;
	virtual ~Light() = default;
	LightType lightType = LightType::Directional;

	glm::vec4 ambient = glm::vec4(0);
	glm::vec4 color = glm::vec4(0); //color + attenuation

	glm::vec3 direction = glm::vec3(0);
	glm::vec3 positionInWorld = glm::vec3(0);

	glm::vec3 lightRange = glm::vec3(0); //x - constant, y - linear, z - quadratic

	glm::mat4 lightViewMatrix = glm::mat4(1);
	glm::mat4 lightProjectionMatrix = glm::mat4(1);

	ViewFrustum lightFrustum = ViewFrustum();

	//for spotlight
	float cutOff = 0.0f;
	float outerCutoff = 0.0f;
};