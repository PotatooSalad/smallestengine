#include "../include/SceneMaker.h"
#include "../include/Core.h"
#include "../include/ObjectModule.h"
#include "../include/PbrShaders.inl"
#include "../include/InCodeShaders.inl"
#include "../include/LightRange.inl"
#include "../../Renderer/include/Material.h"
#include "../../Renderer/include/Mesh.h"
#include "../../Generator/mesh/include/Plane.h"
#include "../../Generator/mesh/include/Cylinder.h"
#include "../../ECS/Base/include/Entity.h"
#include "../../ECS/Components/Transform.inl"
#include "../../ECS/Components/Render/MeshRenderer.h"
#include "../../ECS/Components/Render/Light.h"
#include "../../ECS/Components/Render/Window.h"
#include "../../Generator/texture/include/Wood.h"
#include "../../Generator/texture/include/Marble.h"
#include "../../Generator/texture/include/NormalMap.h"
#include "../../Generator/texture/include/Wallpaper.h"
#include "../../Generator/texture/include/Checker.h"
#include "../../Generator/mesh/include/Box.h"
#include "../../Generator/mesh/include/Sphere.h"

int SceneMaker::materialIndex = 0;
int SceneMaker::shelfIndex = 0;

Mesh* SceneMaker::boxMesh(glm::vec3 center, glm::vec3 size, glm::vec3 holePos, glm::vec3 holeSize)
{
    Box b(center, size);
    if (holeSize.x > 0.01 || holeSize.y > 0.01 || holeSize.z > 0.01)
    {
        b.getHoledBox(holePos, holeSize, vertices, indices);
    }
    else
    {
        b.getBox(vertices, indices);
    }
    objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    return objectModule->getLastMesh();
}

Mesh* SceneMaker::sphereMesh(float radius, int sectors, int stacks)
{
    Sphere s(radius, sectors, stacks);
    s.getSphere(vertices, indices);
    Mesh* m = objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    return objectModule->getLastMesh();
}

glm::quat SceneMaker::eulerToQuaternion(glm::vec3 angle)
{
    return glm::quat(glm::radians(angle));
}

void SceneMaker::initScene(ObjectModule* objectModule)
{
    this->objectModule = objectModule;
    objectModule->newEntity(2, "directional");
    {
        auto dirLight = objectModule->newEmptyComponentForLastEntity<Light>();
        dirLight->lightType = LightType::Directional;
        dirLight->ambient = glm::vec4(0.1f);
        dirLight->color = RendererModule::DIRECTIONAL_COLOR;
        dirLight->lightFrustum.farPlane = 65.0f;
        dirLight->lightFrustum.nearPlane = 0.1f;
        dirLight->lightFrustum.fov = 180.0f;
        dirLight->lightFrustum.aspectRatio = (float)RendererModule::SHADOW_WIDTH / (float)RendererModule::SHADOW_HEIGHT;
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = { -5, 25, -40 };
        //t->getLocalPositionModifiable() = { 0, 30, 0 };
        t->getLocalRotationModifiable() = eulerToQuaternion({ -40, 0, 0 });
        //t->getLocalRotationModifiable() = SceneUtils::eulerToQuaternion({ 5, 0, 0 });

        /*auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = objectModule->newMaterial(objectModule->newShader(Shaders::baseFragmentCode, Shaders::baseVertexCode), "lightMat");
        mr->material->setVec4("color", glm::vec4(1));
        mr->mesh = boxMesh({ 0, 2, 0 }, { 1, 4, 1 });*/
    }


    cubemapPBR = objectModule->newShader(Shaders::pbrToMapFrag, Shaders::pbrVert);
    cubemapPBRInstanced = objectModule->newShader(Shaders::pbrToMapFrag, Shaders::pbrVertInstanced);
    standardPBR = objectModule->newShader(Shaders::pbrFrag, Shaders::pbrVert);
    standardPBRInstanced = objectModule->newShader(Shaders::pbrFrag, Shaders::pbrVertInstanced);
    shadowShader = objectModule->newShader(Shaders::simpleShadowFrag, Shaders::simpleShadowVert);
    glassShader = objectModule->newShader(Shaders::glassFrag, Shaders::glassVert);
    lightWoodMaterials = new Material * [MAX_WOOD];
    darkWoodMaterials = new Material * [MAX_WOOD];
    lampMat = objectModule->newMaterial(glassShader, "lamp", RenderType::Transparent);

    texInfo.generateMipmaps = true;
    texInfo.magFilter = GL_NEAREST;
    texInfo.minFilter = GL_NEAREST_MIPMAP_LINEAR;
    texInfo.wrapMode = GL_REPEAT;
    texInfo.format = GL_RGB;
    texInfo.internalFormat = GL_RGB;

    glm::vec3 shelfSizeVariant1 = { 12, 10, 12 };
    glm::vec3 doorSizeVariant1 = { 12, 10, 0.5 };
    glm::vec3 thinShelfSize = { 8, 8, 8 };
    glm::vec3 thinShelfVariant1 = { 10, 7, 10 };
    glm::vec3 bigShelfSize = { 8, 24, 12 };
    glm::vec3 workTopXSize = { floorSize.x, 0.5, unitShelf.z };
    glm::vec3 workTopZSize = { unitShelf.x, 0.5, floorSize.y - 2 * unitShelf.x };
    glm::vec3 workTopX2Size = { floorSize.x, 0.5, unitShelf.z };
    glm::vec3 drawerSize = { thinShelfSize.x - 0.5, 2, doorSize.z };
    glm::vec3 thinShelfDoorSize = { thinShelfVariant1.x, thinShelfVariant1.y, 0.2 };

    setup.generateNoise = false;
    objectModule->newEntity(2, "floor");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);

        auto* p = new Plane(glm::vec3(0), glm::vec2(1, 1), floorSize);
        p->getPlanePoints(vertices, indices);
        auto* checkerTex = new Checker(floorSize * 10, 12, glm::vec3(0.1f), glm::vec3(0.95f), setup);
        texInfo.width = floorSize.x * 10;
        texInfo.height = floorSize.y * 10;
        auto* floorTex = objectModule->newTexture(checkerTex->getTextureData(), texInfo);
        setup.generateNoise = false;
        auto* floorNormalMap = new NormalMap(floorSize.x * 10, floorSize.y * 10, setup);
        auto* floorNormalTex = objectModule->newTexture(floorNormalMap->getTextureData(), texInfo);

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = objectModule->newMesh(vertices, indices);
        mr->material = objectModule->newMaterial(standardPBR, "floorMat");
        mr->material->setShadowMappingShader(shadowShader);
        mr->material->setCubemapShader(cubemapPBR);
        mr->material->setTexture("texAlbedo", floorTex);
        mr->material->setTexture("texNormal", floorNormalTex);
        mr->material->setFloat("metallic", 0.8f);
        mr->material->setFloat("roughness", 0.3f);

        vertices.clear();
        indices.clear();
        delete floorNormalMap, checkerTex, p;
    }

    texInfo.magFilter = GL_LINEAR;
    texInfo.minFilter = GL_LINEAR_MIPMAP_LINEAR;

    setup.generateNoise = true;
    setup.perlinAmplitude = 0.2f;
    setup.perlinOctaves = 3;
    setup.seed = 2;
    setup.range = { 0, 1.0f };


    makeWoodMaterials(MAX_WOOD, lightWoodMaterials, "darkWoodMat", { 400, 400 }, woodColors);
    makeWoodMaterials(MAX_WOOD, darkWoodMaterials, "lightWoodMat", { 400, 400 }, darkWoodColors);

    doorMesh = boxMesh({ 0, doorSize.y / 2, 0 }, doorSize);
    shelfHalfMesh = boxMesh({ 0, shelfDoubleSize.y / 2, 0 }, { shelfDoubleSize.x, shelfDoubleSize.y, shelfDoubleSize.z });

    Mesh* smallShelfMesh = boxMesh({ 0, unitShelf.y / 2, 0 }, unitShelf);
    Mesh* fridgeMesh = boxMesh({ 0, bigShelfSize.y / 2, 0 }, bigShelfSize);
    Mesh* thinShelfMesh = boxMesh({ 0, thinShelfSize.y / 2, 0 }, thinShelfSize);
    Mesh* smallShelfVariant1Mesh = boxMesh({ 0, shelfSizeVariant1.y / 2, 0 }, shelfSizeVariant1);
    Mesh* doorVariant1Mesh = boxMesh({0, doorSizeVariant1.y / 2, 0}, doorSizeVariant1);
    Mesh* drawerMesh = boxMesh({ 0, drawerSize.y / 2, 0 }, drawerSize);
    Mesh* thinShelfVariant1Mesh = boxMesh({ 0, thinShelfVariant1.y / 2, 0 }, thinShelfVariant1);
    Mesh* thinDoorMesh = boxMesh({ 0, thinShelfDoorSize.y / 2, 0 }, thinShelfDoorSize);

    auto fakeShelfX = objectModule->newEntity(1, "fakeShelfX");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->getLocalPositionModifiable() = { 0, 0, workTopXSize.z - floorSize.y / 2 - shelfDoubleSize.z };
        t->setParent(&GetCore().sceneGraph.rootNode);
    }

    auto fakeShelfZ = objectModule->newEntity(1, "fakeShelfZ");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->getLocalPositionModifiable() = { lc.x - workTopZSize.x, 0, unitShelf.z - workTopZSize.z / 2 - unitShelf.z / 2 };
        t->setParent(&GetCore().sceneGraph.rootNode);
    }

    woodenBox({ unitShelf.x - lc.x - unitShelf.x / 2, 0, unitShelf.z - lc.z - unitShelf.z / 2 }, smallShelfMesh, "emptyShelf");
    woodenBox({ lc.x - unitShelf.x / 2, 0, unitShelf.z - lc.z - unitShelf.z / 2 }, smallShelfMesh, "emptyShelf");
    
    doubleDooredShelf({ -shelfDoubleSize.x + doorSize.x + spaceSize, 0, 0 }, fakeShelfX);
    doubleDooredShelf({ shelfDoubleSize.x - doorSize.x - bigSpaceSize, 0, 0 }, fakeShelfX);
    doubleDooredShelf({ shelfDoubleSize.z / 2 + doorSize.z, 0, 0 }, fakeShelfZ, {0, -90, 0});
    doubleDooredShelf({ shelfDoubleSize.z / 2 + doorSize.z, 0, shelfDoubleSize.x + bigSpaceSize * 2 + thinShelfSize.x }, fakeShelfZ, {0, -90, 0});

    // ================== drawer ===========================
    singleDooredShelf({ thinShelfSize.z / 2 + doorSize.z, 0, shelfDoubleSize.x + bigSpaceSize - thinShelfSize.x / 4 }, thinShelfMesh, thinShelfSize, drawerMesh, drawerSize, KnobType::Drawer, 1, fakeShelfZ, { 0, -90, 0 });

    // ======================== RIGHT ===============================
    singleDooredShelf({ shelfDoubleSize.z / 2 + doorSize.z, 0, shelfDoubleSize.x * 2 + bigSpaceSize * 3 + thinShelfSize.x }, shelfHalfMesh, shelfDoubleSize, doorMesh, doorSize, KnobType::RightDoor, 1, fakeShelfZ, { 0, -90, 0 });

    for (int i = 0; i < 5; ++i)
    {
        auto lightShelf = singleDooredShelf({ lc.x - thinShelfVariant1.x / 2, bigShelfSize.y - thinShelfVariant1.y, thinShelfVariant1.z / 2 - lc.z + (thinShelfVariant1.z + bigSpaceSize) * i }, thinShelfVariant1Mesh, thinShelfVariant1, thinDoorMesh, thinShelfDoorSize, KnobType::DownCenter, 1, nullptr, { 0, -90, 0 });
        if (i > 0 && i < 4)
        {
            auto light = objectModule->newEntity(2, "spotLight");
            {
                auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
                t->getLocalPositionModifiable() = glm::vec3(0, 0.1, 0);
                t->getLocalRotationModifiable() = eulerToQuaternion({ 2 - 2 * (i -1), 0, 0.01 });
                t->setParent(lightShelf->getComponentPtr<Transform>());
                auto l = objectModule->newEmptyComponentForLastEntity<Light>();
                l->lightType = LightType::Spot;
                l->ambient = glm::vec4(0.1f);
                l->color = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
                l->lightRange = LightRange::_50;
                l->lightFrustum.farPlane = 10.0f;
                l->lightFrustum.nearPlane = 0.1f;
                l->lightFrustum.aspectRatio = (float)RendererModule::SHADOW_WIDTH / (float)RendererModule::SHADOW_HEIGHT;
                float angle = 45.0f;
                l->cutOff = glm::cos(glm::radians(30.0f));
                l->outerCutoff = glm::cos(glm::radians(angle));
                l->lightFrustum.fov = 2 * angle;

                auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
                mr->mesh = sphereMesh(0.5, 15, 15);
                mr->material = lampMat;
                cylinder({ 0, -0.15, 0 }, {0.7, 0.2, 0.7}, light, knobMaterial, "light");
            }
        }
    }
    
    //=====================LEFT=============================================
    singleDooredShelf({ shelfSizeVariant1.x - lc.x - shelfSizeVariant1.x / 2, bigShelfSize.y - shelfSizeVariant1.y, shelfSizeVariant1.z - lc.z - shelfSizeVariant1.z / 2 }, smallShelfVariant1Mesh, shelfSizeVariant1, doorVariant1Mesh, doorSizeVariant1, KnobType::DownCenter, 2, nullptr, {0, 90, 0});
    singleDooredShelf({ unitShelf.x - lc.x - thinShelfSize.x / 2, 0, unitShelf.z - lc.z + thinShelfSize.z / 2 + bigSpaceSize }, thinShelfMesh, thinShelfSize, nullptr, {0,0,0}, KnobType::DownCenter, 0);
    singleDooredShelf({ unitShelf.x - lc.x - thinShelfSize.x / 2, bigShelfSize.y - unitShelf.y, unitShelf.z - lc.z + thinShelfSize.z / 2 + bigSpaceSize }, thinShelfMesh, thinShelfSize, nullptr, { 0,0,0 }, KnobType::DownCenter, 0);

    objectModule->newEntity(2, "oven");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = { unitShelf.x - lc.x, unitShelf.y, unitShelf.z - lc.z + thinShelfSize.z / 2 + bigSpaceSize };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = knobMaterial;
        mr->mesh = boxMesh({ 0, thinShelfSize.y / 2, 0 }, { 1, thinShelfSize.y, thinShelfSize.z }, { 0, 0.75, -thinShelfSize.z / 2 + 0.75 }, { 0, thinShelfSize.y - 2.5f, thinShelfSize.z - 1.5f });

        knob(t->entityPtr, KnobType::UpCenter, { 1, thinShelfSize.y, thinShelfSize.z }, "ovenKnob", true);


        setup.range = { 0.9, 1 };
        auto* wp = new Wallpaper({ 200, 200 }, glm::vec3(0.2), setup);
        setup.generateNoise = false;
        auto* nm = new NormalMap(200, 200, setup);
        texInfo.width = 200;
        texInfo.height = 200;
        
        auto* grayTex = objectModule->newTexture(wp->getTextureData(), texInfo);
        auto* grayNormal = objectModule->newTexture(nm->getTextureData(), texInfo);
        delete wp, nm;

        Material* grayMat = objectModule->newMaterial(standardPBR, "grayMat");
        grayMat->setShadowMappingShader(shadowShader);
        grayMat->setCubemapShader(cubemapPBR);
        grayMat->setTexture("texAlbedo", grayTex);
        grayMat->setTexture("texNormal", grayNormal);
        grayMat->setFloat("metallic", 0.4f);
        grayMat->setFloat("roughness", 0.8f);

        objectModule->newEntity(2, "ovenLeft");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x / 2, unitShelf.y, unitShelf.z - lc.z + bigSpaceSize };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({0, thinShelfSize.y * 0.5f, 0}, { thinShelfSize.x, thinShelfSize.y, 0.1f });
        }
        objectModule->newEntity(2, "ovenRight");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x / 2, unitShelf.y, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z};

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({ 0, thinShelfSize.y * 0.5f, 0 }, { thinShelfSize.x, thinShelfSize.y, 0.1f });
        }
        objectModule->newEntity(2, "ovenup");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x / 2, unitShelf.y + thinShelfSize.y - 0.1f, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z / 2 };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({ 0, 0.05f, 0 }, { thinShelfSize.x, 0.1f, thinShelfSize.z });
        }
        objectModule->newEntity(2, "ovendown");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x / 2, unitShelf.y, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z / 2 };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({ 0, 0.05f, 0 }, { thinShelfSize.x, 0.1f, thinShelfSize.z });
        }

        objectModule->newEntity(2, "ovendown2");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x / 2 - 1, unitShelf.y + thinShelfSize.y / 3, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z / 2 };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({ 0, 0.05f, 0 }, { thinShelfSize.x - 2, 0.1f, thinShelfSize.z });
        }
        objectModule->newEntity(2, "ovenback");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x - thinShelfSize.x, unitShelf.y, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z / 2 };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = grayMat;
            mrr->mesh = boxMesh({ 0, thinShelfSize.y * 0.5f, 0 }, { 0.1f, thinShelfSize.y, thinShelfSize.z });
        }

        objectModule->newEntity(2, "ovenGlass");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(&GetCore().sceneGraph.rootNode);
            tr->getLocalPositionModifiable() = { unitShelf.x - lc.x + 0.1f, unitShelf.y + 0.8f, unitShelf.z - lc.z + bigSpaceSize + thinShelfSize.z / 2 };

            auto mrr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mrr->material = objectModule->newMaterial(glassShader, "ovenGlassMat", RenderType::Transparent);
            mrr->material->setVec4("glassColor", { 0.01, 0.01, 0.01, 0.5 });
            mrr->mesh = boxMesh({ 0, (thinShelfSize.y - 2.5f) * 0.5f, 0 }, { 0.1f, thinShelfSize.y - 2.5f, thinShelfSize.z - 1.5f });
        }
    }

    // FRIDGE
    singleDooredShelf({ unitShelf.x - lc.x - unitShelf.x / 2, 0, -lc.z + unitShelf.z + thinShelfSize.z + bigShelfSize.x / 2 + bigSpaceSize * 2 }, fridgeMesh, bigShelfSize, nullptr, { 0,0,0 }, KnobType::RightDoor, 2, nullptr, {0, 90, 0});
    singleDooredShelf({ unitShelf.x - lc.x - unitShelf.x / 2, 0, -lc.z + unitShelf.z + thinShelfSize.z + bigShelfSize.x / 2 + bigSpaceSize * 2 + bigShelfSize.x + spaceSize }, fridgeMesh, bigShelfSize, nullptr, { 0,0,0 }, KnobType::LeftDoor, 2, nullptr, {0, 90, 0});

    // ========================== WORK TOP ===================================
    setup.range = { 0, 1 };
    workTop({ workTopXSize.x / 2 - lc.x, shelfDoubleSize.y, workTopXSize.z / 2 - lc.z }, workTopXSize, "worktop1", marbleColors, nullptr, { 0,0,0 }, { -10, 0, -2 }, { 9, 1, 5 });
    workTop({ lc.x - workTopZSize.x / 2, unitShelf.y, 0 }, workTopZSize, "worktop2", marbleColors, nullptr);

    {
        glm::ivec2 texSize = {500, 500};
        auto* woodTex = new Wood(texSize, darkWoodColors[0], darkWoodColors[1], darkWoodColors[2], setup);
        workTop3Mat = objectModule->newMaterial(standardPBR, "workTop3Mat");
        texInfo.width = texSize.x;
        texInfo.height = texSize.y;
        auto* tex = objectModule->newTexture(woodTex->getTextureData(), texInfo);

        workTop3Mat->setCubemapShader(cubemapPBR);
        workTop3Mat->setShadowMappingShader(shadowShader);
        workTop3Mat->setTexture("texAlbedo", tex);
        workTop3Mat->setTexture("texNormal", woodTex->getNormalTex(texInfo));
        delete woodTex;
    }
    workTop({ 0, shelfDoubleSize.y, lc.z - workTopX2Size.z / 2 }, workTopX2Size, "worktop3", marbleColors, workTop3Mat);

    cylinder({ -unitShelf.z / 2, -unitShelf.y / 2, workTopX2Size.z / 5 }, { 0.8, unitShelf.y, 0 }, objectModule->getEntityPtryByName("worktop3"), knobMaterial, "support1");
    cylinder({ -workTopX2Size.x / 2 + 1.5, -unitShelf.y / 2, -workTopX2Size.z / 3 }, { 0.8, unitShelf.y, 0 }, objectModule->getEntityPtryByName("worktop3"), knobMaterial, "support2");
    cylinder({ -workTopX2Size.x / 2 + 1.5, -unitShelf.y / 2, workTopX2Size.z / 3 }, { 0.8, unitShelf.y, 0 }, objectModule->getEntityPtryByName("worktop3"), knobMaterial, "support3");

    //=================== ADDITIONAL ========================================
    stool({ 0, 0, lc.z - workTopX2Size.z }, { 2, 0.5 }, 5, "stool1");
    stool({ -10, 0, lc.z - workTopX2Size.z + 0.5f }, { 2, 0.5 }, 5, "stool2");
    stool({ 7, 0, lc.z - workTopX2Size.z + 3 }, { 2, 0.5 }, 5, "stool3");
    sink({ -5.5, -3, 0.5 }, { 9.5, 3, 5.5 }, objectModule->getEntityPtryByName("worktop1"));
    tap({ -5.5, workTopXSize.y, -3 }, objectModule->getEntityPtryByName("worktop1"));

    objectModule->newEntity(2, "heatingBoard");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(objectModule->getEntityPtryByName("worktop2")->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = {0, 0.2, -5};

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = boxMesh({ 0, 0, 0 }, {10, 1, 10});
        mr->material = objectModule->newMaterial(standardPBR, "heatingMat");
        mr->material->setCubemapShader(cubemapPBR);
        mr->material->setShadowMappingShader(shadowShader);
        setup.range = { 1, 1 };
        texInfo.width = 100;
        texInfo.height = 100;
        auto* w = new Wallpaper({ 100, 100 }, glm::vec3(0.05f), setup);
        auto* tex = objectModule->newTexture(w->getTextureData(), texInfo);
        setup.generateNoise = false;
        auto* n = new NormalMap(100, 100, setup);
        auto* norm = objectModule->newTexture(n->getTextureData(), texInfo);
        mr->material->setTexture("texAlbedo", tex);
        mr->material->setTexture("texNormal", norm);
        mr->material->setFloat("metallic", 0.9f);
        mr->material->setFloat("roughness", 0.5f);
        delete w, n;

        auto* cyl = new Cylinder(1.01f, 1.6f, 1.6f, 20, 1, 0.05f);
        cyl->getCylinderMesh(vertices, indices);
        Mesh* circleMesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete cyl;

        Material* mat = objectModule->newMaterial(standardPBR, "circleMat");
        mat->setCubemapShader(cubemapPBR);
        mat->setShadowMappingShader(shadowShader);
        setup.range = { 1, 1 };
        texInfo.width = 100;
        texInfo.height = 100;

        auto* w2 = new Wallpaper({ 100, 100 }, glm::vec3(0.403, 0.015, 0.039), setup);
        auto* tex2 = objectModule->newTexture(w2->getTextureData(), texInfo);
        auto* n2 = new NormalMap(100, 100, setup);
        auto* norm2 = objectModule->newTexture(n2->getTextureData(), texInfo);
        mat->setTexture("texAlbedo", tex2);
        mat->setTexture("texNormal", norm2);
        mat->setFloat("metallic", 0.9f);
        mat->setFloat("roughness", 0.5f);

        delete w2, n2;
        objectModule->newEntity(2, "circle");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(t);
            tr->getLocalPositionModifiable() = { -1.8, 0, -2.3 };
            
            auto mr2 = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr2->mesh = circleMesh;
            mr2->material = mat;
        }

        objectModule->newEntity(2, "circle");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(t);
            tr->getLocalPositionModifiable() = { 2.2, 0, -2.3 };

            auto mr2 = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr2->mesh = circleMesh;
            mr2->material = mat;
        }

        objectModule->newEntity(2, "circle");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(t);
            tr->getLocalPositionModifiable() = { 2.2, 0, 2.3 };

            auto mr2 = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr2->mesh = circleMesh;
            mr2->material = mat;
        }
        objectModule->newEntity(2, "circle");
        {
            auto tr = objectModule->newEmptyComponentForLastEntity<Transform>();
            tr->setParent(t);
            tr->getLocalPositionModifiable() = { -1.8, 0, 2.3 };

            auto mr2 = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr2->mesh = circleMesh;
            mr2->material = mat;
        }
    }

    objectModule->newEntity(2, "bowl");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = { 10, shelfDoubleSize.y + 2, -25 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        auto* cyl = new Cylinder(1.8f, 2.5f, 3.0f, 20, 3, 0.1f);
        cyl->getCylinderMeshBowl(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete cyl;
        mr->material = objectModule->newMaterial(glassShader, "bowl", RenderType::Transparent);
        mr->material->setVec4("glassColor", glm::vec4(0.01, 0.01, 0.01, 0.5));
    }
    //oranges
    {
        setup.range = { 0.9, 1 };
        auto* orangeCol = new Wallpaper({ 300, 300 }, { 0.854, 0.486, 0.086 }, setup);
        texInfo.width = 300;
        texInfo.height = 300;
        auto* orangeTex = objectModule->newTexture(orangeCol->getTextureData(), texInfo);

        setup.range = { 0, 1 };
        setup.generateNoise = true;
        auto* normal = new NormalMap(200, 200, setup);
        texInfo.width = 200;
        texInfo.height = 200;
        auto* orangeNormTex = objectModule->newTexture(normal->getTextureData(), texInfo);
        auto* orangeMat = objectModule->newMaterial(standardPBR, "orangeMat");
        orangeMat->setTexture("texAlbedo", orangeTex);
        orangeMat->setTexture("texNormal", orangeNormTex);
        orangeMat->setCubemapShader(cubemapPBR);
        orangeMat->setShadowMappingShader(shadowShader);
        orangeMat->setFloat("roughness", 0.9f);
        orangeMat->setFloat("metallic", 0.1f);

        delete orangeCol, normal;

        sphereTextured({ 10.0f, shelfDoubleSize.y + 2, -25.0f }, 1.0f,  orangeMat, "orange1");
        sphereTextured({ 10.0f, shelfDoubleSize.y + 2, -23.2f }, 0.8f, orangeMat, "orange2");
        sphereTextured({ 11.0f, shelfDoubleSize.y + 2, -26.5f }, 0.85f, orangeMat, "orange3");
        sphereTextured({ 11.5f, shelfDoubleSize.y + 2, -24.0f }, 0.8f, orangeMat, "orange4");
        sphereTextured({ 9.5f, shelfDoubleSize.y + 2, -26.5f }, 0.7f, orangeMat, "orange5");
        sphereTextured({ 8.4f, shelfDoubleSize.y + 2.3, -24.3f }, 0.8f, orangeMat, "orange6");
    }
    //melons
    {
        setup.range = { 0.9, 1 };
        auto* orangeCol = new Wallpaper({ 300, 300 }, { 0.145, 0.627, 0.094 }, setup);
        texInfo.width = 300;
        texInfo.height = 300;
        auto* orangeTex = objectModule->newTexture(orangeCol->getTextureData(), texInfo);

        setup.range = { 0, 1 };
        auto* normal = new NormalMap(50, 50, setup);
        texInfo.width = 50;
        texInfo.height = 50;
        auto* orangeNormTex = objectModule->newTexture(normal->getTextureData(), texInfo);

        auto* orangeMat = objectModule->newMaterial(standardPBR, "melonMat");
        orangeMat->setTexture("texAlbedo", orangeTex);
        orangeMat->setTexture("texNormal", orangeNormTex);
        orangeMat->setCubemapShader(cubemapPBR);
        orangeMat->setShadowMappingShader(shadowShader);

        orangeMat->setFloat("roughness", 0.9f);
        orangeMat->setFloat("metallic", 0.1f);
        delete orangeCol, normal;

        sphereTextured({ 8.5f, shelfDoubleSize.y + 2.8, -25.8 }, 0.9f, orangeMat, "melon1");
        sphereTextured({ 11.5f, shelfDoubleSize.y + 2.9, -25 }, 0.9f, orangeMat, "melon2");
        sphereTextured({ 8.6f, shelfDoubleSize.y + 2.7, -23.6 }, 0.8f, orangeMat, "melon3");
    }
    //watermelons
    {
        setup.range = { 1, 1 };
        auto* watermelon = new Wallpaper({ 300, 300 }, { 0.105, 0.352, 0.027 }, { 0.396, 0.521, 0.039 }, setup);
        setup.generateNoise = false;
        auto* normal = new NormalMap(300, 300, setup);
        texInfo.width = 300;
        texInfo.height = 300;
        auto* watermelonTex = objectModule->newTexture(watermelon->getTextureData(), texInfo);
        auto* normalTex = objectModule->newTexture(normal->getTextureData(), texInfo);

        auto* watermelonMat = objectModule->newMaterial(standardPBR, "watermelon");
        watermelonMat->setTexture("texAlbedo", watermelonTex);
        watermelonMat->setTexture("texNormal", normalTex);
        watermelonMat->setCubemapShader(cubemapPBR);
        watermelonMat->setShadowMappingShader(shadowShader);
        watermelonMat->setFloat("roughness", 0.9f);
        watermelonMat->setFloat("metallic", 0.1f);

        sphereTextured({ -lc.x + 3, shelfDoubleSize.y + 2.5f, -lc.z + 3 }, 2.5f, watermelonMat, "watermelon1", {0, 45, 0});
        sphereTextured({ -lc.x + 7.5, shelfDoubleSize.y + 2.5f, -lc.z + 3 }, 2.5f, watermelonMat, "watermelon2", { 0, -45, 90 });
        sphereTextured({ lc.x - 7, shelfDoubleSize.y + 0.75f, 8 }, 2.0f, watermelonMat, "watermelon3", { 0, 15, 90 });

        delete watermelon, normal;
    }

    //cutting board
    {
        objectModule->newEntity(2, "cuttingBoard");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(objectModule->getEntityPtryByName("worktop2")->getComponentPtr<Transform>());
            t->getLocalPositionModifiable() = { -1.5, 0.2, 8 };

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->mesh = boxMesh({ 0, 0.25, 0 }, { 5, 0.5, 7 });
            mr->material = lightWoodMaterials[0];
        }

        auto knife = objectModule->newEntity(1, "knife");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(objectModule->getEntityPtryByName("worktop2")->getComponentPtr<Transform>());
            t->getLocalPositionModifiable() = { -1.5, 1, 15 };
            t->getLocalRotationModifiable() = eulerToQuaternion({ 0, 15, 0 });
        }

        cylinder({ -2.2, 0, -0.32 }, { 0.2f, 1.5f, 0.2f }, knife, lightWoodMaterials[1], "handle", { 0, 0, 90 });

        objectModule->newEntity(2, "blade");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(knife->getComponentPtr<Transform>());
            t->getLocalRotationModifiable() = eulerToQuaternion({ 5, 0, 0 });

            auto mat = objectModule->newMaterial(standardPBR, "knifeMat");
            auto* w = new Wallpaper({ 100, 100 }, glm::vec3(0.5f), setup);
            texInfo.width = 100;
            texInfo.height = 100;
            auto* tex = objectModule->newTexture(w->getTextureData(), texInfo);
            auto* nm = new NormalMap(100, 100, setup);
            auto* normal = objectModule->newTexture(nm->getTextureData(), texInfo);
            mat->setTexture("texAlbedo", tex);
            mat->setTexture("texNormal", normal);
            mat->setFloat("metallic", 0.3f);
            mat->setFloat("roughness", 0.8f);
            mat->setCubemapShader(cubemapPBR);
            mat->setShadowMappingShader(shadowShader);
            delete w, nm;

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = mat;
            mr->mesh = boxMesh({ 0, 0.05, 0 }, { 3, 0.05, 1.2 });
        }
    }

    //FlowerPot
    {
        auto pot = objectModule->newEntity(2, "flowerPot");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(&GetCore().sceneGraph.rootNode);
            t->getLocalPositionModifiable() = { lc.x - 5, shelfDoubleSize.y + 1.5, lc.z - 5 };

            auto* cyl = new Cylinder(2.0f, 2.0f, 2.2f, 15, 3, 0.3f);
            cyl->getCylinderMeshBowl(vertices, indices);
            Mesh* potMesh = objectModule->newMesh(vertices, indices, true);
            delete cyl;

            setup.range = { 1, 1 };
            texInfo.width = 300;
            texInfo.height = 300;
            auto* wp = new Wallpaper({ 300, 300 }, { 0.380, 0.050, 0.588 }, setup);
            auto* nm = new NormalMap(300, 300, setup);
            Texture* potTex = objectModule->newTexture(wp->getTextureData(), texInfo);
            Texture* potNormal = objectModule->newTexture(nm->getTextureData(), texInfo);
            vertices.clear(); indices.clear();
            delete wp, nm;

            auto potMat = objectModule->newMaterial(standardPBR, "potMat");
            potMat->setTexture("texAlbedo", potTex);
            potMat->setTexture("texNormal", potNormal);
            potMat->setFloat("metallic", 0.8f);
            potMat->setFloat("roughness", 0.3f);
            potMat->setShadowMappingShader(shadowShader);
            potMat->setCubemapShader(cubemapPBR);

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = potMat;
            mr->mesh = potMesh;
        }

        objectModule->newEntity(2, "ground");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(pot->getComponentPtr<Transform>());
            t->getLocalPositionModifiable() = { 0, 0.1, 0 };

            auto* cyl = new Cylinder(1.6f, 1.7f, 1.9f, 10, 3, 0);
            cyl->getCylinderMeshBowl(vertices, indices);
            Mesh* groundMesh = objectModule->newMesh(vertices, indices, true);
            delete cyl;

            setup.range = { 0.95, 1 };
            texInfo.width = 300;
            texInfo.height = 300;
            auto* wp = new Wallpaper({ 300, 300 }, { 0.301, 0.141, 0 }, setup);
            setup.generateNoise = true;
            auto* nm = new NormalMap(300, 300, setup);
            Texture* potTex = objectModule->newTexture(wp->getTextureData(), texInfo);
            Texture* potNormal = objectModule->newTexture(nm->getTextureData(), texInfo);
            vertices.clear(); indices.clear();
            delete wp, nm;

            auto groundMat = objectModule->newMaterial(standardPBR, "groundMat");
            groundMat->setTexture("texAlbedo", potTex);
            groundMat->setTexture("texNormal", potNormal);
            groundMat->setFloat("metallic", 0.1f);
            groundMat->setFloat("roughness", 0.9f);
            groundMat->setShadowMappingShader(shadowShader);
            groundMat->setCubemapShader(cubemapPBR);

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = groundMat;
            mr->mesh = groundMesh;
        }
        grass({0, 3.3f, 0}, pot, 60, 1.25f);
    }
    //flowerpot 2
    {
        auto pot = objectModule->newEntity(2, "flowerPot2");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(&GetCore().sceneGraph.rootNode);
            t->getLocalPositionModifiable() = { lc.x - 4, shelfDoubleSize.y + 1.5, -lc.z + 5 };

            auto* cyl = new Cylinder(2.0f, 2.0f, 2.2f, 15, 3, 0.3f);
            cyl->getCylinderMeshBowl(vertices, indices);
            Mesh* potMesh = objectModule->newMesh(vertices, indices, true);
            delete cyl;

            setup.range = { 1, 1 };
            texInfo.width = 300;
            texInfo.height = 300;
            auto* wp = new Wallpaper({ 300, 300 }, { 0, 0.145, 0.301 }, { 0.007, 0.356, 0.733 }, setup);
            auto* nm = new NormalMap(300, 300, setup);
            Texture* potTex = objectModule->newTexture(wp->getTextureData(), texInfo);
            Texture* potNormal = objectModule->newTexture(nm->getTextureData(), texInfo);
            vertices.clear(); indices.clear();
            delete wp, nm;

            auto potMat = objectModule->newMaterial(standardPBR, "potMat2");
            potMat->setTexture("texAlbedo", potTex);
            potMat->setTexture("texNormal", potNormal);
            potMat->setFloat("metallic", 0.8f);
            potMat->setFloat("roughness", 0.3f);
            potMat->setShadowMappingShader(shadowShader);
            potMat->setCubemapShader(cubemapPBR);

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = potMat;
            mr->mesh = potMesh;
        }

        objectModule->newEntity(2, "ground2");
        {
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(pot->getComponentPtr<Transform>());
            t->getLocalPositionModifiable() = { 0, 0.1, 0 };

            auto* cyl = new Cylinder(1.6f, 1.7f, 1.9f, 10, 3, 0);
            cyl->getCylinderMeshBowl(vertices, indices);
            Mesh* groundMesh = objectModule->newMesh(vertices, indices, true);
            delete cyl;

            setup.range = { 0.95, 1 };
            texInfo.width = 300;
            texInfo.height = 300;
            auto* wp = new Wallpaper({ 300, 300 }, { 0.301, 0.141, 0 }, setup);
            setup.generateNoise = true;
            auto* nm = new NormalMap(300, 300, setup);
            Texture* potTex = objectModule->newTexture(wp->getTextureData(), texInfo);
            Texture* potNormal = objectModule->newTexture(nm->getTextureData(), texInfo);
            vertices.clear(); indices.clear();
            delete wp, nm;

            auto groundMat = objectModule->newMaterial(standardPBR, "groundMat2");
            groundMat->setTexture("texAlbedo", potTex);
            groundMat->setTexture("texNormal", potNormal);
            groundMat->setFloat("metallic", 0.1f);
            groundMat->setFloat("roughness", 0.9f);
            groundMat->setShadowMappingShader(shadowShader);
            groundMat->setCubemapShader(cubemapPBR);

            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = groundMat;
            mr->mesh = groundMesh;
        }
        grass({ 0, 3.3f, 0 }, pot, 70, 1.25f);
    }

    room();

    delete[] lightWoodMaterials, darkWoodMaterials;
}

void SceneMaker::room()
{
    setup.range = { 0.9, 1 };
    auto* wp1 = new Wallpaper(glm::ivec2(floorSize.x, wallHeight) * 10, glm::vec3(0.6f), setup);
    texInfo.width = floorSize.x * 10;
    texInfo.height = wallHeight * 10;
    auto* wallpaper1Tex = objectModule->newTexture(wp1->getTextureData(), texInfo);
    setup.generateNoise = false;
    auto* wp1Normal = new NormalMap(floorSize.x * 10, wallHeight * 10, setup);
    setup.generateNoise = true;
    auto* wallpaper1Normal = objectModule->newTexture(wp1Normal->getTextureData(), texInfo);
    auto* wallHoriz = objectModule->newMaterial(standardPBR, "wallHorizontal");
    wallHoriz->setShadowMappingShader(shadowShader);
    wallHoriz->setCubemapShader(cubemapPBR);
    wallHoriz->setTexture("texAlbedo", wallpaper1Tex);
    wallHoriz->setTexture("texNormal", wallpaper1Normal);
    wallHoriz->setFloat("metallic", 0.1f);
    wallHoriz->setFloat("roughness", 0.9f);
    delete wp1, wp1Normal;
    // ---------------------------------------------
    auto* wp2 = new Wallpaper(glm::ivec2(floorSize.y * 10, wallHeight * 10), glm::vec3(0.6f), setup);
    texInfo.width = floorSize.y * 10;
    texInfo.height = wallHeight * 10;
    auto* wallpaper2Tex = objectModule->newTexture(wp2->getTextureData(), texInfo);
    setup.generateNoise = false;
    auto* wp2Normal = new NormalMap(floorSize.y * 10, wallHeight * 10, setup);
    setup.generateNoise = true;
    auto* wallpaper2Normal = objectModule->newTexture(wp2Normal->getTextureData(), texInfo);
    auto* wallVertic = objectModule->newMaterial(standardPBR, "wallVertical");
    wallVertic->setShadowMappingShader(shadowShader);
    wallVertic->setCubemapShader(cubemapPBR);
    wallVertic->setTexture("texAlbedo", wallpaper2Tex);
    wallVertic->setTexture("texNormal", wallpaper2Normal);
    wallVertic->setFloat("metallic", 0.1f);
    wallVertic->setFloat("roughness", 0.8f);

    delete wp2, wp2Normal;

    wall({ 0, wallHeight / 2.0f, floorSize.y / 2.0f + 1 }, { floorSize.x + 4, wallHeight , 2 }, wallHoriz, "wall1");
    wall({ 0, wallHeight / 2.0f, -floorSize.y / 2.0f - 1 }, { floorSize.x + 4, wallHeight , 2 }, wallHoriz, "wall2", { -11, 9, 0 }, { 16, 15, 2 });
    wall({ floorSize.x / 2.0f + 1, wallHeight / 2.0f, 0 }, { 2, wallHeight, floorSize.y }, wallVertic, "wall3");
    wall({ -floorSize.x / 2.0f - 1, wallHeight / 2.0f, 0 }, { 2, wallHeight, floorSize.y }, wallVertic, "wall4", { 0, 0, floorSize.y / 2 - 22 }, { 2, 20, 10 });

    auto door = woodenBox({ 0, 0, floorSize.y / 2 - 17 }, boxMesh({ 0, 10, 0 }, { 10, 20, 1 }), "door", objectModule->getEntityPtryByName("wall4"), true, {0, 90, 0});
    knob(door, KnobType::RightDoor, {10, 23, 1}, "doorKnob", true);

    Shader* windowShader = objectModule->newShader(Shaders::windowFrag, Shaders::glassVert);
    auto* windowMat = objectModule->newMaterial(windowShader, "windowMat", RenderType::Transparent);
    objectModule->newEntity(3, "window");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = { -3, wallHeight / 2.0f + 1.5f - 15 / 2.0f, -floorSize.y / 2.0f - 1 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = windowMat;
        mr->mesh = boxMesh({0, 15 / 2.0f, 0}, { 16, 15, 2 });

        objectModule->newEmptyComponentForLastEntity<Window>();
    }

    // -------------------------------------------
    auto* wp3 = new Wallpaper(glm::ivec2(floorSize.x * 10, floorSize.y * 10), glm::vec3(0.8f), setup);
    texInfo.width = floorSize.x * 10;
    texInfo.height = floorSize.y * 10;
    auto* wallpaper3Tex = objectModule->newTexture(wp3->getTextureData(), texInfo);
    setup.generateNoise = false;
    auto* wp3Normal = new NormalMap(floorSize.x * 10, floorSize.y * 10, setup);
    setup.generateNoise = true;
    auto* wallpaper3Normal = objectModule->newTexture(wp3Normal->getTextureData(), texInfo);
    Material * ceiling = objectModule->newMaterial(standardPBR, "ceiling");
    ceiling->setShadowMappingShader(shadowShader);
    ceiling->setCubemapShader(cubemapPBR);
    ceiling->setTexture("texAlbedo", wallpaper3Tex);
    ceiling->setTexture("texNormal", wallpaper3Normal);
    ceiling->setFloat("metallic", 0.1f);
    ceiling->setFloat("roughness", 0.8f);

    delete wp3, wp3Normal;

    wall({ 0, wallHeight - 1, 0 }, { floorSize.x, 2, floorSize.y }, ceiling, "ceiling");
    auto light1 = objectModule->newEntity(3, "spotLight");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(objectModule->getEntityPtryByName("ceiling")->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = glm::vec3(0, wallHeight - 2, 10);
        t->getLocalRotationModifiable() = eulerToQuaternion({ 0.1, 0, 0 });
        auto l = objectModule->newEmptyComponentForLastEntity<Light>();
        l->lightType = LightType::Spot;
        l->ambient = glm::vec4(0.1f);
        l->color = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        l->lightRange = LightRange::_100;
        l->lightFrustum.farPlane = 30.0f;
        l->lightFrustum.nearPlane = 1.0f;
        l->lightFrustum.aspectRatio = (float)RendererModule::SHADOW_WIDTH / (float)RendererModule::SHADOW_HEIGHT;
        float angle = 70.0f;
        l->cutOff = glm::cos(glm::radians(30.0f));
        l->outerCutoff = glm::cos(glm::radians(angle));
        l->lightFrustum.fov = angle;

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = sphereMesh(0.7f, 15, 15);
        mr->material = lampMat;
        cylinder({ 0, -0.1, 0 }, { 1, 0.5, 1 }, light1, knobMaterial, "light");
    }

    auto light2 = objectModule->newEntity(3, "spotLight");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(objectModule->getEntityPtryByName("ceiling")->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = glm::vec3(0, wallHeight - 2, -10);
        t->getLocalRotationModifiable() = eulerToQuaternion({ 0.1, 0, 0 });
        auto l = objectModule->newEmptyComponentForLastEntity<Light>();
        l->lightType = LightType::Spot;
        l->ambient = glm::vec4(0.1f);
        l->color = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        l->lightRange = LightRange::_100;
        l->lightFrustum.farPlane = 30.0f;
        l->lightFrustum.nearPlane = 1.0f;
        l->lightFrustum.aspectRatio = (float)RendererModule::SHADOW_WIDTH / (float)RendererModule::SHADOW_HEIGHT;
        float angle = 70.0f;
        l->cutOff = glm::cos(glm::radians(30.0f));
        l->outerCutoff = glm::cos(glm::radians(angle));
        l->lightFrustum.fov = angle;

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = sphereMesh(0.7f, 15, 15);
        mr->material = lampMat;
        cylinder({ 0, -0.1, 0 }, { 1, 0.5, 1 }, light2, knobMaterial, "light");
    }
    setup.range = { 0, 1 };
}

void SceneMaker::wall(glm::vec3 pos, glm::vec3 size, Material * wallMaterial, const char* name, glm::vec3 holePos, glm::vec3 holeSize)
{
    objectModule->newEntity(2, name);
    auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
    t->setParent(&GetCore().sceneGraph.rootNode);
    t->getLocalPositionModifiable() = { pos.x, 0, pos.z };

    auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
    mr->material = wallMaterial;

    auto* boxMesh = new Box(glm::vec3(0, pos.y, 0), size);
    if (holeSize.x > 0 && holeSize.y > 0 && holeSize.z > 0)
    {
        boxMesh->getHoledBox(holePos, holeSize, vertices, indices);
    }
    else
    {
        boxMesh->getBox(vertices, indices);
    }
    mr->mesh = objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    delete boxMesh;
}

Entity* SceneMaker::woodenBox(glm::vec3 pos, Mesh* mesh, const char* name, Entity* parent, bool dark, glm::vec3 rot)
{
    auto ent = objectModule->newEntity(2, name);
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(parent != nullptr ? parent->getComponentPtr<Transform>() : &GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = pos;
        t->getLocalRotationModifiable() = eulerToQuaternion(rot);

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = dark ? darkWoodMaterials[materialIndex++ % MAX_WOOD] : lightWoodMaterials[materialIndex++ % MAX_WOOD];
        mr->mesh = mesh;
    }
    return ent;
}

void SceneMaker::doubleDooredShelf(glm::vec3 pos, Entity* parent, glm::vec3 rot)
{
    std::string name = "door" + std::to_string(shelfIndex++);
    auto door1 = woodenBox(pos, shelfHalfMesh, name.c_str(), parent, false, rot);
    auto doorLeft = woodenBox({ -doorSize.x / 2 - spaceSize / 2, unitShelf.y - doorSize.y, shelfDoubleSize.z / 2 }, doorMesh, (name + "_left").c_str(), door1, true);
    knob(doorLeft, KnobType::LeftDoor, doorSize, (name + "_leftKnob").c_str());
    auto doorRight = woodenBox({ doorSize.x / 2 + spaceSize / 2, unitShelf.y - doorSize.y, shelfDoubleSize.z / 2 }, doorMesh, (name + "_right").c_str(), door1, true);
    knob(doorRight, KnobType::RightDoor, doorSize, (name + "_rightKnob").c_str());
}

Entity* SceneMaker::singleDooredShelf(glm::vec3 pos, Mesh* shelfMesh, glm::vec3 meshSize, Mesh* doorMesh, glm::vec3 doorSize, KnobType knobType, int knobMode, Entity* parent, glm::vec3 rot)
{
    std::string name = "shelf" + std::to_string(shelfIndex++);
    auto ent = woodenBox(pos, shelfMesh, name.c_str(), parent, false, rot);
    Entity* door;
    if (knobType == KnobType::Drawer)
    {
        for (int i = 0; i < 3; ++i)
        {
            if (doorMesh != nullptr)
            {
                door = woodenBox(glm::vec3(0, meshSize.y - (doorSize.y + spaceSize) * (i+1), meshSize.z / 2), doorMesh, (name + "_door").c_str(), ent, true);
                switch (knobMode)
                {
                case 1:
                    knob(door, knobType, doorSize, (name + "_doorKnob").c_str(), false);
                    break;
                case 2:
                    knob(door, knobType, doorSize, (name + "_doorKnob").c_str(), true);
                    break;
                }
            }

        }
    }
    else
    {
        Entity* par;
        glm::vec3 size;
        if (doorMesh != nullptr)
        {
            size = doorSize;
            par = woodenBox(glm::vec3(-meshSize.x / 2 + doorSize.x / 2, meshSize.y - doorSize.y, meshSize.z / 2), doorMesh, (name + "_door").c_str(), ent, true);
        }
        else
        {
            par = ent;
            size = meshSize;
        }

        switch (knobMode)
        {
        case 1:
            knob(par, knobType, size, (name + "_doorKnob").c_str(), false);
            break;
        case 2:
            knob(par, knobType, size, (name + "_doorKnob").c_str(), true);
            break;
        }
    }
    return ent;
}

void SceneMaker::makeWoodMaterials(int amount, Material** mats, const char* name, glm::ivec2 texSize, glm::mat3 color)
{
    setup.generateNoise = true;
    for (int i = 0; i < amount; ++i)
    {
        std::string n = name + std::to_string(i);
        auto* woodTex = new Wood(texSize, color[0], color[1], color[2], setup);
        mats[i] = objectModule->newMaterial(standardPBR, n);
        texInfo.width = texSize.x;
        texInfo.height = texSize.y;
        auto* tex = objectModule->newTexture(woodTex->getTextureData(), texInfo);

        mats[i]->setTexture("texAlbedo", tex);
        mats[i]->setTexture("texNormal", woodTex->getNormalTex(texInfo));
        mats[i]->setFloat("metallic", 0.3f);
        mats[i]->setFloat("roughness", 0.7f);
        mats[i]->setCubemapShader(cubemapPBR);
        mats[i]->setShadowMappingShader(shadowShader);
        delete woodTex;
    }
}

void SceneMaker::workTop(glm::vec3 pos, glm::vec3 size, const char* name, glm::mat3 color, Material* mat, glm::vec3 rot, glm::vec3 holePos, glm::vec3 holeSize)
{
    objectModule->newEntity(2, name);
    auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
    t->setParent(&GetCore().sceneGraph.rootNode);
    t->getLocalPositionModifiable() = pos;
    t->getLocalRotationModifiable() = eulerToQuaternion(rot);

    auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
    if (mat == nullptr)
    {
        mr->material = objectModule->newMaterial(standardPBR, "workTopMat");
        mr->material->setCubemapShader(cubemapPBR);
        mr->material->setShadowMappingShader(shadowShader);
        glm::vec2 texSize = glm::vec2(size.x, size.z) * 25.0f;
        setup.generateNoise = true;
        Marble* mar = new Marble(texSize, color[0], color[1], setup);
        setup.generateNoise = false;
        auto* normalMap = new NormalMap(texSize.x, texSize.y, setup);
        setup.generateNoise = true;
        texInfo.width = (int)texSize.x;
        texInfo.height = (int)texSize.y;
        auto* marble = objectModule->newTexture(mar->getTextureData(), texInfo);
        auto* normal = objectModule->newTexture(normalMap->getTextureData(), texInfo);
        mr->material->setTexture("texAlbedo", marble);
        mr->material->setTexture("texNormal", normal);
        delete mar, normalMap;
    }
    else
    {
        mr->material = mat;
    }

    mr->material->setFloat("metallic", 0.1f);
    mr->material->setFloat("roughness", 0.5f);

    auto* boxMesh = new Box(glm::vec3(0, size.y / 2, 0), size);
    if (holeSize.x > 0.01f && holeSize.y > 0.01 && holeSize.z > 0.01)
    {
        boxMesh->getHoledBox(holePos, holeSize, vertices, indices);
    }
    else
    {
        boxMesh->getBox(vertices, indices);
    }
    mr->mesh = objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    delete boxMesh;
}

void SceneMaker::knob(Entity* shelfEnt, KnobType type, glm::vec3 shelfSize, const char* name, bool big)
{
    vertices.clear();
    indices.clear();
    float length = big ? 2.5f : 1.5f;
    float radius = big ? 0.2f : 0.1f;

    if (big && knobBigMeshes == nullptr)
    {
        knobBigMeshes = new Mesh*[2];
        auto* base = new Cylinder(length, radius, 20, 2);
        base->getCylinderMesh(vertices, indices);
        knobBigMeshes[0] = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        auto* left = new Cylinder(length / 3, radius / 2, 10, 2);
        left->getCylinderMesh(vertices, indices);
        knobBigMeshes[1] = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete base, left;
    }
    else if (!big && knobSmallMeshes == nullptr)
    {
        knobSmallMeshes = new Mesh*[2];
        auto* base = new Cylinder(length, radius, 20, 2);
        base->getCylinderMesh(vertices, indices);
        knobSmallMeshes[0] = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        auto* left = new Cylinder(length / 3, radius / 2, 10, 2);
        left->getCylinderMesh(vertices, indices);
        knobSmallMeshes[1] = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete base, left;
    }

    auto b = objectModule->newEntity(2, name);
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(shelfEnt->getComponentPtr<Transform>());
        glm::vec3 pos = glm::vec3(0);
        glm::quat rot = eulerToQuaternion({ 0, 0, 0 });
        switch (type)
        {
        case KnobType::Drawer:
            pos = glm::vec3(0, shelfSize.y - 1, shelfSize.z / 2 + length / 3);
            rot = eulerToQuaternion({ 0, 0, 90 });
            break;
        case KnobType::LeftDoor:
            pos = { shelfSize.x / 2 - length / 2, shelfSize.y / 2, shelfSize.z / 2 + length / 3 };
            break;
        case KnobType::RightDoor:
            pos = { -shelfSize.x / 2 + length / 2, shelfSize.y / 2, shelfSize.z / 2 + length / 3 };
            break;
        case KnobType::DownCenter:
            pos = glm::vec3(0, 1, shelfSize.z / 2 + length / 3);
            rot = eulerToQuaternion({ 0, 0, 90 });
            break;
        case KnobType::UpCenter:
            pos = glm::vec3(1.5f, shelfSize.y - 0.7, 0);
            rot = eulerToQuaternion({ 90, 0, 90 });
        }
        t->getLocalPositionModifiable() = pos;
        t->getLocalRotationModifiable() = rot;
        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        if (knobMaterial == nullptr)
        {
            knobMaterial = objectModule->newMaterial(standardPBR, "knobMat");
            ProceduralEngineSetup s;
            s.range = { 1, 1 };
            s.generateNoise = false;
            auto* w = new Wallpaper({ 100, 100 }, glm::vec3(0.25f), s);
            texInfo.width = 100;
            texInfo.height = 100;
            auto* tex = objectModule->newTexture(w->getTextureData(), texInfo);
            auto* nm = new NormalMap(100, 100, s);
            auto* normal = objectModule->newTexture(nm->getTextureData(), texInfo);
            knobMaterial->setTexture("texAlbedo", tex);
            knobMaterial->setTexture("texNormal", normal);
            knobMaterial->setFloat("metallic", 1.0f);
            knobMaterial->setFloat("roughness", 0.2f);
            knobMaterial->setCubemapShader(cubemapPBR);
            knobMaterial->setShadowMappingShader(shadowShader);
            delete w, nm;
        }
        mr->material = knobMaterial;
        mr->mesh = big ? knobBigMeshes[0] : knobSmallMeshes[0];
    }


    std::string n = name;
    n += "_left";
    objectModule->newEntity(2, n.c_str());
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(b->getComponentPtr<Transform>());
        t->getLocalRotationModifiable() = eulerToQuaternion({ 90, 0, 0 });
        t->getLocalPositionModifiable() = { 0, 0.3 * length, -length / 6 };
        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = knobMaterial;
        mr->mesh = big ? knobBigMeshes[1] : knobSmallMeshes[1];
    }

    n = name;
    n += "_right";
    objectModule->newEntity(2, n.c_str());
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(b->getComponentPtr<Transform>());
        t->getLocalRotationModifiable() = eulerToQuaternion({ 90, 0, 0 });
        t->getLocalPositionModifiable() = { 0, -0.3 * length, -length / 6 };
        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = knobMaterial;
        mr->mesh = big ? knobBigMeshes[1] : knobSmallMeshes[1];
    }
    n = "";
}

void SceneMaker::cylinder(glm::vec3 pos, glm::vec3 size, Entity* parent, Material* mat, const char* name, glm::vec3 rot)
{
    auto* cyl = new Cylinder(size.y, size.x, 30, 2);
    cyl->getCylinderMesh(vertices, indices);
    Mesh* m = objectModule->newMesh(vertices, indices, true);
    vertices.clear();
    indices.clear();
    delete cyl;

    objectModule->newEntity(2, name);
    auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
    if (parent != nullptr)
    {
        t->setParent(parent->getComponentPtr<Transform>());
    }
    else
    {
        t->setParent(&GetCore().sceneGraph.rootNode);
    }
    t->getLocalPositionModifiable() = pos;
    t->getLocalRotationModifiable() = eulerToQuaternion(rot);

    auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
    mr->material = mat;
    mr->mesh = m;
}

void SceneMaker::stool(glm::vec3 pos, glm::vec2 sitSize, float height, const char* name)
{
    auto* base = new Cylinder(sitSize.y, sitSize.x, 20, 1);
    base->getCylinderMesh(vertices, indices);
    Mesh* baseMesh = objectModule->newMesh(vertices, indices, true);
    vertices.clear();
    indices.clear();
    delete base;

    auto* leg = new Cylinder(height, sitSize.x / 6, 10, 1);
    leg->getCylinderMesh(vertices, indices);
    Mesh* legMesh = objectModule->newMesh(vertices, indices, true);
    vertices.clear();
    indices.clear();
    delete leg;

    auto mat = lightWoodMaterials[materialIndex++ % MAX_WOOD];

    auto e = objectModule->newEntity(2, name);
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalPositionModifiable() = { pos.x, pos.y + height - 0.1f, pos.z };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        mr->mesh = baseMesh;
    }
    float shift = 0.8f;
    float angle = 10.0f;

    objectModule->newEntity(2, "leg1");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(e->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = { shift, -height / 2, shift };
        t->getLocalRotationModifiable() = eulerToQuaternion(glm::vec3(-angle, 0, angle));

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        mr->mesh = legMesh;
    }

    objectModule->newEntity(2, "leg2");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(e->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = { shift, -height / 2, -shift };
        t->getLocalRotationModifiable() = eulerToQuaternion(glm::vec3(angle, 0, angle));

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        mr->mesh = legMesh;
    }

    objectModule->newEntity(2, "leg3");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(e->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = { -shift, -height / 2, shift };
        t->getLocalRotationModifiable() = eulerToQuaternion(glm::vec3(-angle, 0, -angle));

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        mr->mesh = legMesh;
    }

    objectModule->newEntity(2, "leg4");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(e->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = { -shift, -height / 2, -shift };
        t->getLocalRotationModifiable() = eulerToQuaternion(glm::vec3(angle, 0, -angle));

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        mr->mesh = legMesh;
    }
}

void SceneMaker::sink(glm::vec3 pos, glm::vec3 size, Entity* parent)
{
    setup.range = { 0.7, 1 };
    auto* sinkColor = new Wallpaper({ 100, 100 }, glm::vec3(0.3f), setup);
    auto* normalMap = new NormalMap(100, 100, setup);
    texInfo.width = 100;
    texInfo.height = 100;

    auto* sinkTex = objectModule->newTexture(sinkColor->getTextureData(), texInfo);
    auto* normal = objectModule->newTexture(normalMap->getTextureData(), texInfo);
    delete sinkColor, normalMap;

    auto* b1 = new Box({ 0, size.y / 2, 0 }, { size.x, size.y, 0.5 });
    auto* b2 = new Box({ 0, size.y / 2, 0 }, { 0.5, size.y, size.z });
    auto* b3 = new Box({ 0, 0, 0 }, { size.x, 0.5, size.z });

    vertices.clear();
    indices.clear();
    b1->getBox(vertices, indices);
    Mesh* m1 = objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    b2->getBox(vertices, indices);
    Mesh* m2 = objectModule->newMesh(vertices, indices);
    vertices.clear();
    indices.clear();
    delete b1, b2;

    auto sink = objectModule->newEntity(2, "sink");
    auto sinkT = objectModule->newEmptyComponentForLastEntity<Transform>();

    auto sinkMat = objectModule->newMaterial(standardPBR, "sinkMat");
    sinkMat->setShadowMappingShader(shadowShader);
    sinkMat->setCubemapShader(cubemapPBR);
    sinkMat->setTexture("texAlbedo", sinkTex);
    sinkMat->setTexture("texNormal", normal);
    sinkMat->setFloat("metallic", 0.5f);
    sinkMat->setFloat("roughness", 0.3f);

    sinkT->setParent(parent->getComponentPtr<Transform>());
    sinkT->getLocalPositionModifiable() = pos;
    {
        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        b3->getBox(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices);
        vertices.clear();
        indices.clear();
        delete b3;
        mr->material = sinkMat;
    }

    objectModule->newEntity(2, "sink_left");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(sinkT);
        t->getLocalPositionModifiable() = { 0, 0, size.z / 2 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = m1;
        mr->material = sinkMat;
    }

    objectModule->newEntity(2, "sink_right");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(sinkT);
        t->getLocalPositionModifiable() = { 0, 0, -size.z / 2 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = m1;
        mr->material = sinkMat;
    }

    objectModule->newEntity(2, "sink_back");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(sinkT);
        t->getLocalPositionModifiable() = { size.x / 2, 0, 0 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = m2;
        mr->material = sinkMat;
    }

    objectModule->newEntity(2, "sink_forward");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(sinkT);
        t->getLocalPositionModifiable() = { -size.x / 2, 0, 0 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->mesh = m2;
        mr->material = sinkMat;
    }
}

void SceneMaker::tap(glm::vec3 pos, Entity* parent)
{
    auto* base = new Cylinder(0.2f, 0.4f, 20, 1);
    auto* height = new Cylinder(3.0f, 0.25f, 20, 1);
    auto* b = new Box({ 0, 0.15, 0 }, { 0.6, 0.3, 3.5 });

    auto* tap = new Cylinder(0.4f, 0.3f, 20, 1);
    auto* b2 = new Box({ 0, 0.15, 0 }, { 0.6, 0.3, 1.5 });

    ProceduralEngineSetup s;
    s.range = { 1, 1 };
    s.generateNoise = false;
    auto* w = new Wallpaper({ 100, 100 }, glm::vec3(0.25f), s);
    texInfo.width = 100;
    texInfo.height = 100;
    auto* tex = objectModule->newTexture(w->getTextureData(), texInfo);
    auto* nm = new NormalMap(100, 100, setup);
    auto* normal = objectModule->newTexture(nm->getTextureData(), texInfo);
    delete w, nm;

    auto* mat = objectModule->newMaterial(standardPBR, "tapMat");
    mat->setCubemapShader(cubemapPBR);
    mat->setShadowMappingShader(shadowShader);
    mat->setTexture("texAlbedo", tex);
    mat->setTexture("texNormal", normal);
    mat->setFloat("metallic", 1.0f);
    mat->setFloat("roughness", 0.1f);

    vertices.clear();
    indices.clear();
    objectModule->newEntity(2, "tapBase");
    auto tapBaseT = objectModule->newEmptyComponentForLastEntity<Transform>();
    {
        tapBaseT->setParent(parent->getComponentPtr<Transform>());
        tapBaseT->getLocalPositionModifiable() = pos;

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        base->getCylinderMesh(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete base;
    }

    objectModule->newEntity(2, "tapCyl");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(tapBaseT);
        t->getLocalPositionModifiable() = { 0, 1, 0 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        height->getCylinderMesh(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete height;
    }

    auto box = objectModule->newEntity(2, "tapBox");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(tapBaseT);
        t->getLocalPositionModifiable() = { 0, 2.5, 1.3 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        b->getBox(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices);
        vertices.clear();
        indices.clear();
        delete b;
    }

    objectModule->newEntity(2, "tapCyl2");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(box->getComponentPtr<Transform>());
        t->getLocalPositionModifiable() = { 0, 0, 1.3 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        tap->getCylinderMesh(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
    }

    objectModule->newEntity(2, "tapBase2");
    auto tapBase2T = objectModule->newEmptyComponentForLastEntity<Transform>();
    {
        tapBase2T->setParent(parent->getComponentPtr<Transform>());
        tapBase2T->getLocalPositionModifiable() = pos + glm::vec3(2, 0.4, 0);

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        tap->getCylinderMesh(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices, true);
        vertices.clear();
        indices.clear();
        delete tap;
    }

    objectModule->newEntity(2, "tapBox");
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(tapBase2T);
        t->getLocalPositionModifiable() = { 0, 0, 0.4 };

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        b2->getBox(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices);
        vertices.clear();
        indices.clear();
        delete b2;
    }
}

void SceneMaker::sphereTextured(glm::vec3 pos, float radius, Material* mat, const char* name, glm::vec3 rot)
{
    objectModule->newEntity(2, name);
    {
        auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
        t->setParent(&GetCore().sceneGraph.rootNode);
        t->getLocalRotationModifiable() = eulerToQuaternion(rot);
        t->getLocalPositionModifiable() = pos;

        auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
        mr->material = mat;
        auto* sp = new Sphere(radius, 20, 10);
        sp->getSphere(vertices, indices);
        mr->mesh = objectModule->newMesh(vertices, indices);
        vertices.clear();
        indices.clear();
        delete sp;
    }
}

void SceneMaker::grass(glm::vec3 pos, Entity* parent, int grassSize, float radius)
{
    auto* cyl = new Cylinder(4.0f, 0.05f, 0, 10, 3, 0);
    cyl->getCylinderMeshBowl(vertices, indices);
    Mesh* grassMesh = objectModule->newMesh(vertices, indices, true);
    vertices.clear();
    indices.clear();
    delete cyl;
    setup.range = { 1,1 };
    setup.generateNoise = false;
    auto* wp = new Wallpaper({ 200, 200 }, { 0.168, 0.474, 0.043 }, setup);
    auto* nm = new NormalMap(200, 200, setup);
    texInfo.width = 200;
    texInfo.height = 200;
    auto* grassTex = objectModule->newTexture(wp->getTextureData(), texInfo);
    auto* normal = objectModule->newTexture(nm->getTextureData(), texInfo);
    delete nm, wp;

    setup.range = { 0.0f, 0.105f };
    auto* rand = new Wallpaper({ 1, 1 }, {0, 0, 0}, setup);

    auto* grassMat = objectModule->newMaterial(standardPBRInstanced, "grassMat", RenderType::Opaque, true);
    grassMat->setShadowMappingShader(shadowShader);
    grassMat->setCubemapShader(cubemapPBRInstanced);
    grassMat->setTexture("texAlbedo", grassTex);
    grassMat->setTexture("texNormal", normal);
    grassMat->setFloat("roughness", 1.0f);
    grassMat->setFloat("metallic", 0.2f);
    Transform* parentTrans = parent->getComponentPtr<Transform>();
    for (int i = 0; i < grassSize / 2; ++i)
    {
        for (int j = 0; j < grassSize / 2; ++j)
        {
            objectModule->newEntity(2, ("grass" + std::to_string(i)).c_str());
            auto t = objectModule->newEmptyComponentForLastEntity<Transform>();
            t->setParent(parentTrans);
            glm::vec3 shift = glm::vec3(((float)i - (float)grassSize / 4.0f) * rand->randomValue(), -rand->randomValue() * 8, ((float)j - (float)grassSize / 4.0f) * rand->randomValue());
            shift = glm::clamp(shift, glm::vec3(-radius), glm::vec3(radius));
            t->getLocalPositionModifiable() = pos + shift;
            glm::vec3 angle = {(j - (grassSize / 4.0f)) / (grassSize / 4.0f), 0, (i - (grassSize / 4.0f)) / (grassSize / 4.0f)};
            angle *= 100.0f * rand->randomValue();
            t->getLocalRotationModifiable() = eulerToQuaternion(angle);
            auto mr = objectModule->newEmptyComponentForLastEntity<MeshRenderer>();
            mr->material = grassMat;
            mr->mesh = grassMesh;
        }
    }
    delete rand;
}