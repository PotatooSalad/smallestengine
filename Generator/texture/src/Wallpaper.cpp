#include "../include/Wallpaper.h"

void Wallpaper::countTexture()
{
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			texData[y * size.x * 3 + x * 3 + 0] = floatToByte(baseColor.r * randomValue());
			texData[y * size.x * 3 + x * 3 + 1] = floatToByte(baseColor.g * randomValue());
			texData[y * size.x * 3 + x * 3 + 2] = floatToByte(baseColor.b * randomValue());
		}
	}
	//saveData();
}

void Wallpaper::countDoubleColoredTexture()
{
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			texData[y * size.x * 3 + x * 3 + 0] = floatToByte(glm::mix(baseColor.r, secondColor.r, glm::sin(x * 0.5)) * randomValue());
			texData[y * size.x * 3 + x * 3 + 1] = floatToByte(glm::mix(baseColor.g, secondColor.g, glm::sin(x * 0.5)) * randomValue());
			texData[y * size.x * 3 + x * 3 + 2] = floatToByte(glm::mix(baseColor.b, secondColor.b, glm::sin(x * 0.5)) * randomValue());
		}
	}
	//saveData();
}