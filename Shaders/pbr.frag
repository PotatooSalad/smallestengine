#version 430 core
out vec4 FragColor;

#define MAX_SPOTLIGHTS 5
struct DirLight { // 4 * vec4
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 color;
};

struct SpotLight { // 5* vec4 + 2 * float
    float cutOff;
    float outerCutOff;
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 color;
    vec3 lightRange; //x - constant, y- linear, z- quadratic
};

in vec3 oNormal;
in vec2 oTexCoords;
in vec3 FragPos;
in vec4 oFragPosDirLightSpace;
in vec4 oFragPosSpotLightSpace[MAX_SPOTLIGHTS];

layout (std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

layout (std140, binding = 1) uniform Light
{
    DirLight dirLight;
    SpotLight spotLights0;
    SpotLight spotLights1;
    SpotLight spotLights2;
    SpotLight spotLights3;
    SpotLight spotLights4;
};

uniform sampler2D texAlbedo;
uniform sampler2D texNormal;
uniform float metallic;
uniform float roughness;
uniform float ao;
uniform sampler2D shadowMaps0;
uniform sampler2D shadowMaps1;
uniform sampler2D shadowMaps2;
uniform sampler2D shadowMaps3;
uniform sampler2D shadowMaps4;
uniform sampler2D shadowMaps5;
uniform samplerCube irradianceMap;
uniform samplerCube prefilterMap;
uniform sampler2D brdfLUT; 

const float PI = 3.14159265359;

vec3 getNormalFromMap()
{
    vec3 tangentNormal = texture(texNormal, oTexCoords).xyz * 2.0 - 1.0;

    vec3 Q1  = dFdx(FragPos);
    vec3 Q2  = dFdy(FragPos);
    vec2 st1 = dFdx(oTexCoords);
    vec2 st2 = dFdy(oTexCoords);

    vec3 N   = normalize(oNormal);
    vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B  = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

// ----------------------------------------------------------------------------
float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
// ----------------------------------------------------------------------------
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}
// ----------------------------------------------------------------------------
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}
// -----------------------------------------------------------------------------
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}  
// ----------------------------------------------------------------------------

float ShadowCalculation(vec4 fragPosLightSpace, vec3 lightPos, sampler2D shadowMap)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // calculate bias (based on depth map resolution and slope)
    vec3 normal = normalize(oNormal);
    vec3 lightDir = normalize(lightPos - FragPos);
    //float bias = max(0.005 * (1.0 - dot(normal, lightDir)), 0.005);
    float bias = 0.05 * tan(acos(dot(normal, lightDir)));
	bias = clamp(bias, 0.0, 0.009);
    // check whether current frag pos is in shadow
    // float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;
    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    
    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;
        
    return shadow;
}

vec3 calcSpotLight(SpotLight light, vec3 N, vec3 V, vec3 F0, vec3 albedo, vec4 fragPosLightSpace, sampler2D shadowMap)
{
    // calculate per-light radiance
    vec3 L = normalize(light.position - FragPos);
    vec3 H = normalize(V + L);
    float distance = length(light.position - FragPos);
    //float attenuation = 1.0 / (distance * distance);
    float attenuation = 1.0 / (light.lightRange.x + light.lightRange.y * distance + light.lightRange.z * (distance * distance));
    float theta = dot(L, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    vec3 radiance = light.color.rgb * attenuation * intensity;

    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);   
    float G   = GeometrySmith(N, V, L, roughness);      
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        
    vec3 nominator    = NDF * G * F; 
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
    
    // kS is equal to Fresnel
    vec3 kS = F;
    // for energy conservation, the diffuse and specular light can't
    // be above 1.0 (unless the surface emits light); to preserve this
    // relationship the diffuse component (kD) should equal 1.0 - kS.
    vec3 kD = vec3(1.0) - kS;
    // multiply kD by the inverse metalness such that only non-metals 
    // have diffuse lighting, or a linear blend if partly metal (pure metals
    // have no diffuse light).
    kD *= 1.0 - metallic;	  

    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);  

    float shadow = ShadowCalculation(fragPosLightSpace, light.position, shadowMap);      

    // add to outgoing radiance Lo
    return (1.0 - shadow) * (kD * albedo / PI + specular) * radiance * NdotL;
}

vec3 calcDirLight(DirLight light, vec3 N, vec3 V, vec3 F0, vec3 albedo)
{
    // calculate per-light radiance
    vec3 L = normalize(light.position - FragPos);
    vec3 H = normalize(V + L);

    
    vec3 radiance = light.color.rgb * light.color.a;

    // Cook-Torrance BRDF
    float NDF = DistributionGGX(N, H, roughness);   
    float G   = GeometrySmith(N, V, L, roughness);      
    vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);
        
    vec3 nominator    = NDF * G * F; 
    float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
    vec3 specular = nominator / denominator;
    
    // kS is equal to Fresnel
    vec3 kS = F;
    // for energy conservation, the diffuse and specular light can't
    // be above 1.0 (unless the surface emits light); to preserve this
    // relationship the diffuse component (kD) should equal 1.0 - kS.
    vec3 kD = vec3(1.0) - kS;
    // multiply kD by the inverse metalness such that only non-metals 
    // have diffuse lighting, or a linear blend if partly metal (pure metals
    // have no diffuse light).
    kD *= 1.0 - metallic;	  

    // scale light by NdotL
    float NdotL = max(dot(N, L), 0.0);
    float shadow = ShadowCalculation(oFragPosDirLightSpace, light.position, shadowMaps0);         

    // add to outgoing radiance Lo
    return (1.0 - shadow) * (kD * albedo / PI + specular) * radiance * NdotL;
}

vec3 boxProjection(vec3 direction, vec3 position, vec3 cubemapPosition) 
{
    vec3 boxMin = vec3(-24, 0, -30);
    vec3 boxMax = vec3(24, 30, 30);

    vec3 factors = vec3(0.0);
    factors.x = direction.x > 0 ? boxMax.x : boxMin.x;
    factors.y = direction.y > 0 ? boxMax.y : boxMin.y;
    factors.z = direction.z > 0 ? boxMax.z : boxMin.z;
    factors -= position;
    factors /= direction;
    float scalar = min(min(factors.x, factors.y), factors.z);
    return direction * scalar + (position - cubemapPosition);
}

void main()
{		
    vec3 albedo = texture(texAlbedo, oTexCoords).rgb;

    vec3 N = getNormalFromMap();
    vec3 V = normalize(viewPos - FragPos);
    vec3 R = reflect(-V, N);
    R = boxProjection(R, FragPos, vec3(0, 15, 0));

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metallic);

    // reflectance equation
    vec3 Lo = calcDirLight(dirLight, N, V, F0, albedo);
    Lo += calcSpotLight(spotLights0, N, V, F0, albedo, oFragPosSpotLightSpace[0], shadowMaps1);
    Lo += calcSpotLight(spotLights1, N, V, F0, albedo, oFragPosSpotLightSpace[1], shadowMaps2);
    Lo += calcSpotLight(spotLights2, N, V, F0, albedo, oFragPosSpotLightSpace[2], shadowMaps3);
    Lo += calcSpotLight(spotLights3, N, V, F0, albedo, oFragPosSpotLightSpace[3], shadowMaps4);
    Lo += calcSpotLight(spotLights4, N, V, F0, albedo, oFragPosSpotLightSpace[4], shadowMaps5);
    
    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - metallic;
    vec3 irradiance = texture(irradianceMap, N).rgb;
    vec3 diffuse = irradiance * albedo;

    const float MAX_REFLECTION_LOD = 4.0;
    vec3 prefilteredColor = textureLod(prefilterMap, R,  roughness * MAX_REFLECTION_LOD).rgb;    
    vec2 brdf  = texture(brdfLUT, vec2(max(dot(N, V), 0.0), roughness)).rg;
    vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);
    // ambient lighting (note that the next IBL tutorial will replace 
    // this ambient lighting with environment lighting).
    vec3 ambient = (kD * diffuse + specular) * ao;
    
    vec3 color = ambient + Lo;

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2)); 

    FragColor = vec4(color, 1.0);
}