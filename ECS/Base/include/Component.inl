#pragma once

class Entity;

class Component
{
public:
	Component() = default;
	virtual ~Component() = default;

	Entity* entityPtr = nullptr;
};