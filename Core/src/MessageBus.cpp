#include "../include/MessageBus.h"
#include "../include/Message.inl"
#include "../include/Module.inl"
#include "../../Core/include/globalDefs.h"

MessageBus::MessageBus(int bufferSize)
{
	buffer0.reserve(bufferSize);
	buffer1.reserve(bufferSize);
}

void MessageBus::addReceiver(Module* modulePtr)
{
	receivers.push_back(modulePtr);
}

void MessageBus::sendMessage(Message msg)
{
	if (activeBuffer->size() < activeBuffer->capacity())
	{
		activeBuffer->push_back(msg);
	}
	else
	{
		LOGMSG("Message buffer overflow");
		notify();
	}
}

void MessageBus::notify()
{
	auto tmp = activeBuffer;
	activeBuffer = inactiveBuffer;
	inactiveBuffer = tmp;

	for (auto ptr : receivers)
	{
		for (auto msg : *inactiveBuffer)
		{
			ptr->receiveMessage(msg);
		}
	}
	inactiveBuffer->clear();
}