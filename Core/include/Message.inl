#pragma once
#include "Event.inl"
#include "globalDefs.h"

class Message
{
public:
	static constexpr int BUFFERSIZE = 32 - sizeof(Event);

	/// <summary>
	/// Message object constructor
	/// </summary>
	/// <param name="event">type of message</param>
	inline Message(Event event)
	{
		this->event = event;
	}

	/// <summary>
	/// Message object constructor
	/// </summary>
	/// <typeparam name="T">type of additional data</typeparam>
	/// <param name="event">type of message</param>
	/// <param name="value">additional data to send</param>
	template<typename T>
	Message(Event event, T value)
	{
		this->event = event;
		if (sizeof(value) > BUFFERSIZE)
		{
			LOGMSG("Message is too big");
		}

		T* ptr = reinterpret_cast<T*>(buffer);
		*ptr = value;
	}

	/// <summary>
	/// Get the message event
	/// </summary>
	/// <returns>type of message</returns>
	inline Event getEvent()
	{
		return event;
	}

	/// <summary>
	/// get the value of message
	/// </summary>
	/// <typeparam name="T">type of data to extract</typeparam>
	/// <returns>stored additional data</returns>
	template<typename T>
	T getValue()
	{
		T* ptr = reinterpret_cast<T*>(buffer);
		return *ptr;
	}

private:
	Event event = Event::UNKNOWN;
	char buffer[BUFFERSIZE] = {};
};