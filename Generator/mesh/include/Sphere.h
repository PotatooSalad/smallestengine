#pragma once
#include "../../../Core/include/globalDefs.h"
#include "../../../Renderer/include/MeshDataStructures.h"
#include <vector>

class Sphere
{
public:
	Sphere(float radius, int sectors, int stacks) : radius(radius), sectors(sectors), stacks(stacks) {}

	void getSphere(std::vector<Vertex>& vertices, std::vector<uint>& indices);
private:
	float radius;
	int sectors;
	int stacks;
};