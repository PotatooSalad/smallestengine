#include "../../include/packets/InstancedPacket.h"

#include "../../include/Material.h"
#include "../../include/Mesh.h"
#include "../../include/Shader.h"
#include "../../include/Texture.h"
#include "../../include/RendererModule.h"

InstancedPacket::InstancedPacket(Mesh* mesh, Material* material) : RenderPacket(mesh, material)
{
    instanceMatrices.reserve(INSTANCE_ALLOCATION);
    instanceMatricesUnculled.reserve(INSTANCE_ALLOCATION);
    instanceOccluded.reserve(INSTANCE_ALLOCATION);
}

void InstancedPacket::render(glm::mat4& VP, RenderMode mode, Texture* directionalShadowMap, Texture* spotShadowMaps[], Texture* irradianceMap, Texture* prefilteredMap, Texture* brdf)
{
    if (RendererModule::firstRender)
    {
        if (mode == RenderMode::CUBEMAP_MAPPING && material->hasCubemapShader())
        {
            material->setShadowMaps(directionalShadowMap, spotShadowMaps);
        }
        if (mode == RenderMode::NORMAL && material->hasCubemapShader())
        {
            material->setTexture("brdfLUT", brdf);
            material->setTexture("irradianceMap", irradianceMap);
            material->setTexture("prefilterMap", prefilteredMap);
        }
    }
    material->use(mode);
    material->setVP(VP);

    for (size_t i = 0; i < instanceMatrices.size(); i++)
    {
        if (!instanceOccluded[i])
        {
            instanceMatricesUnculled.push_back(*(instanceMatrices[i]));
        }
    }
    if (mesh->hasBackFaceCulling())
    {
        glDisable(GL_CULL_FACE);
        mesh->renderInstanced(instanceMatricesUnculled.size(), instanceMatricesUnculled.data());
        glEnable(GL_CULL_FACE);
    }
    else
    {
        mesh->renderInstanced(instanceMatricesUnculled.size(), instanceMatricesUnculled.data());
    }

    instanceMatricesUnculled.clear();
}

void InstancedPacket::renderWithShader(Shader* shader, glm::mat4& VP)
{
    shader->use();
    for (size_t i = 0; i < instanceMatrices.size(); i++)
    {
        if (!instanceOccluded[i])
        {
            instanceMatricesUnculled.push_back(*(instanceMatrices[i]));
        }
    }

    mesh->renderInstanced(instanceMatrices.size(), instanceMatricesUnculled.data());

    instanceMatricesUnculled.clear();
}