#include "../include/Procedural.h"
//#include <string>
//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#include "stb_image_write.h"
//int Procedural::id = 0;
Procedural::Procedural(glm::ivec2 size, glm::vec3 baseColor, ProceduralEngineSetup setup)
{
	this->size = size;
	this->baseColor = baseColor;
	this->texData = new byte[size.x * size.y * 3];
	this->setup = setup;
	//id++;
	engine = std::mt19937(setup.seed);
	distr = std::uniform_real_distribution<float>(setup.range.x, setup.range.y);
	if (setup.generateNoise)
	{
		this->noiseArr = new float[size.x * size.y];
		generateNoise();
	}
	else
	{
		this->noiseArr = nullptr;
	}
}

//void Procedural::saveData()
//{
//	std::string name = "tex";
//	name += std::to_string(id);
//	name += ".tga";
//	stbi_write_tga(name.c_str(), size.x, size.y, 3, texData);
//}

Procedural::~Procedural()
{
	memset(texData, 0, size.x * size.y * 3);
	delete[] texData;
	if (setup.generateNoise)
	{
		memset(noiseArr, 0, size.x * size.y);
	}
	delete[] noiseArr;
}

float Procedural::randomValue()
{
	return distr(engine);
}

void Procedural::generateNoise()
{
	
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			noiseArr[y * size.x + x] = randomValue();
		}
	}
}

float Procedural::perlinSample(float x)
{
	float fract = glm::fract(x);
	float rand1 = randomValue();
	float rand2 = -randomValue();
	float loPos = rand1 * fract;
	float hiPos = rand2 * (1.0f - fract);
	float u = fract * fract * (3.0f - 2.0f * fract);
	float ft = u * PI;
	float f = (1 - glm::cos(ft)) * 0.5f;
	return loPos * (1 - f) + hiPos * f;
}

float Procedural::perlinNoise1D(float x)
{
	float result = 0.0f;
	float amplitude = setup.perlinAmplitude;
	for (int i = 0; i < setup.perlinOctaves; ++i)
	{
		result += amplitude * perlinSample(x);
		x *= 2.0f;
		amplitude *= 0.5f;
	}
	return result;
}

float Procedural::perlinNoise2D(glm::vec2 uv)
{
	// Initial values
	float value = 0.0;
	float amplitud = setup.perlinAmplitude;
	//
	// Loop of octaves
	for (int i = 0; i < setup.perlinOctaves; i++) 
	{
		value += amplitud * smoothNoise(uv.x, uv.y);
		uv *= 2.;
		amplitud *= .5;
	}
	return value;
}

byte Procedural::floatToByte(float f)
{
	int temp = (int)(255 * f);
	return (byte)glm::clamp(temp, 0, 255);
}

byte Procedural::doubleToByte(double f)
{
	int temp = (int)(255 * f);
	return (byte)glm::clamp(temp, 0, 255);
}

double Procedural::turbulence(double x, double y, double size)
{
	double value = 0.0;
	double initialSize = size;

	while (size >= 1)
	{
		value += smoothNoise(x / size, y / size) * size;
		size /= 2.0;
	}
	return value / initialSize;
}

double Procedural::smoothNoise(double x, double y)
{
	//get fractional part of x and y
	double fractX = x - int(x);
	double fractY = y - int(y);

	//wrap around
	int x1 = (int(x) + size.x) % size.x;
	int y1 = (int(y) + size.y) % size.y;

	//neighbor values
	int x2 = (x1 + size.x - 1) % size.x;
	int y2 = (y1 + size.y - 1) % size.y;

	//smooth the noise with bilinear interpolation
	double value = 0.0;
	value += fractX * fractY * noiseArr[y1 * size.x + x1];
	value += (1 - fractX) * fractY * noiseArr[y1 * size.x + x2];
	value += fractX * (1 - fractY) * noiseArr[y2 * size.x + x1];
	value += (1 - fractX) * (1 - fractY) * noiseArr[y2 * size.x + x2];

	return value;
}