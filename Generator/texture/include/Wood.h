#pragma once
#include "Procedural.h"
#include "../../../Renderer/include/Texture.h"

class Wood : public Procedural
{
public:
	Wood(glm::ivec2 size, glm::vec3 baseColor, glm::vec3 additionalColor1, glm::vec3 additionalColor2, ProceduralEngineSetup setup);
	
	Texture* getNormalTex(TextureCreateInfo texSetup);

private:
	int normMultX = 5;
	int normMultY = 2;
	static float shift;
	glm::vec3 additColor1;
	glm::vec3 additColor2;
	
	byte* normalTex;

	virtual void countTexture();
	/*void saveNormal();*/
	void countNormal();
	float noise(glm::vec2 uv);
};