#pragma once
#include "Procedural.h"

class Marble : public Procedural
{
public:
	Marble(glm::ivec2 size, glm::vec3 baseColor, glm::vec3 additColor, ProceduralEngineSetup setup);

private:
	glm::vec3 additColor;

	const double turbulencePower = 12.0;
	const double turbulenceSize = 32.0;

	void countTexture();
};