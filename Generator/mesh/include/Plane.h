#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "../../../Renderer/include/MeshDataStructures.h"
#include "../../../Core/include/globalDefs.h"

class Plane
{
public:
	Plane(glm::vec3 planeCenter, glm::vec2 segments, glm::vec2 segmentSize = glm::vec2(1, 1), glm::vec3 normals = glm::vec3(0, 1, 0)) 
		: planeCenter(planeCenter), segments(segments), segmentSize(segmentSize), normals(normals) {}

	void getPlanePoints(std::vector<Vertex>& vertices, std::vector<uint>& indices);
private:
	glm::vec3 planeCenter;
	glm::vec2 segments = glm::vec2(1, 1);
	glm::vec2 segmentSize = glm::vec2(1, 1);
	glm::vec3 normals = glm::vec3(0, 1, 0);
};