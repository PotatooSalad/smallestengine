#version 430 core

layout (std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

const float AIR = 1.0;
const float GLASS = 1.51714;

const float ETA = AIR / GLASS;
// http://en.wikipedia.org/wiki/Refractive_index ->Reflectivity
const float R0 = ((AIR - GLASS) * (AIR - GLASS)) / ((AIR + GLASS) * (AIR + GLASS));

in vec4 FragPos;
in vec3 oNormal;

uniform samplerCube enviromentalMap;
uniform vec4 glassColor;

out vec4 FragColor;

void main()
{
    vec3 viewDir = normalize(vec3(FragPos - viewPos));

    vec3 refraction = refract(viewDir, oNormal, ETA);
    vec3 reflection = reflect(viewDir, oNormal);
    //http://en.wikipedia.org/wiki/Schlick%27s_approximation
    float fresnel = R0 + (1.0 - R0) * pow((1.0 - dot(-viewDir, oNormal)), 5.0);

    vec4 refractionColor = texture(enviromentalMap, normalize(refraction));
    vec4 reflectionColor = texture(enviromentalMap, normalize(reflection));

    FragColor = glassColor * mix(refractionColor, reflectionColor, fresnel);
}