#pragma once
#include <glm/glm.hpp>
#include "../../../Core/include/globalDefs.h"
#include <random>

struct ProceduralEngineSetup
{
	int seed = 2137;
	glm::vec2 range = { 0, 1 };
	int perlinOctaves = 3;
	float perlinAmplitude = 0.5f;
	bool generateNoise = true;
};

class Procedural
{
public:
	Procedural(glm::ivec2 size, glm::vec3 baseColor, ProceduralEngineSetup setup);
	virtual ~Procedural();
	byte* getTextureData() { return texData; }
	float randomValue();

protected:
	glm::ivec2 size;
	glm::vec3 baseColor;

	byte* texData;
	float* noiseArr;

	virtual void countTexture() = 0;
	//void saveData();
	float perlinNoise1D(float x);
	float perlinNoise2D(glm::vec2 uv);
	const float TWO_PI = (2.0f * PI);

	float perlinSample(float x);

	byte floatToByte(float f);
	byte doubleToByte(double f);

	double turbulence(double x, double y, double size);
	double smoothNoise(double x, double y);
	ProceduralEngineSetup setup;

	//static int id;
private:
	void generateNoise();
	std::mt19937 engine;
	std::uniform_real_distribution<float> distr;
};