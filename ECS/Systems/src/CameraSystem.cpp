#include "../include/CameraSystem.h"

#include "../../Components/Render/Camera.h"
#include "../../Components/Transform.inl"
#include "../../Base/include/Entity.h"
#include "../../../Core/include/Message.inl"
#include "../../../Core/include/Core.h"
#include <GLFW/glfw3.h>

Camera* CameraSystem::mainCamera = nullptr;

bool CameraSystem::setAsMain(Entity* entity)
{
	Transform* tranTmp = entity->getComponentPtr<Transform>();
	Camera* cameraTmp = entity->getComponentPtr<Camera>();

	if (tranTmp != nullptr && cameraTmp != nullptr)
	{
		if (mainCamera != nullptr)
		{
			mainCamera->isMain = false;
		}
        LOGMSG("Set main Camera");
		cameraTmp->isMain = true;
		mainCamera = cameraTmp;
        GetCore().getMessageBus().sendMessage(Message(Event::SET_CAMERA, mainCamera));
		return true;
	}

	return false;
}


bool CameraSystem::assertEntity(Entity* entity)
{
	transform = entity->getComponentPtr<Transform>();
	camera = entity->getComponentPtr<Camera>();
	return (transform != nullptr && camera != nullptr);
}

void CameraSystem::frameUpdate()
{
    if (camera->isMain)
    {
        // * ===== Calculate projection matrix =====
        if (camera->projectionChanged)
        {
            ViewFrustum& frustum = camera->getFrustumModifiable();

            switch (camera->getProjectionMode())
            {
            case CameraProjection::PERSPECTIVE:
                camera->projectionMatrix = glm::perspective(
                    glm::radians(frustum.fov),
                    frustum.aspectRatio,
                    frustum.nearPlane,
                    frustum.farPlane
                );
                break;
                // ! ----- Warning! Orthographic projection is untested and may produce undefined behaviour ----- !
            case CameraProjection::ORTHOGRAPHIC:
                camera->projectionMatrix = glm::ortho(
                    -frustum.fov * frustum.aspectRatio,
                    frustum.fov * frustum.aspectRatio,
                    -frustum.fov,
                    frustum.fov,
                    frustum.nearPlane,
                    frustum.farPlane
                );
                break;
            }

            // * ===== Calculate frustum properties used for frustum culling =====

            float tang = glm::tan(glm::radians(frustum.fov / 2.0f));
            frustum.Hnear = 2.0f * tang * frustum.nearPlane;
            frustum.Wnear = frustum.Hnear * frustum.aspectRatio;

            frustum.Hfar = 2.0f * tang * frustum.farPlane;
            frustum.Wfar = frustum.Hfar * frustum.aspectRatio;
        }

        // * ===== Calculate view matrix =====
        camera->viewMatrix = transform->getToModelMatrix();
        // * ===== Update camera frustum =====
        ViewFrustum& frustum = camera->getFrustumModifiable();
        frustum.position = transform->getWorldPosition();
        frustum.front = -glm::normalize(glm::vec3(transform->getModelMatrix()[2]));
        frustum.up = glm::normalize(glm::vec3(transform->getModelMatrix()[1]));
        frustum.right = glm::normalize(glm::vec3(transform->getModelMatrix()[0]));

        camera->projectionChanged = false;
    }
}

void CameraSystem::receiveMessage(Message msg)
{
    switch (msg.getEvent())
    {
    case Event::KEY_PRESSED:

        switch (msg.getValue<int>())
        {
        case GLFW_KEY_W:
            movementVector.z += -1.0f;
            break;
        case GLFW_KEY_S:
            movementVector.z += 1.0f;
            break;
        case GLFW_KEY_A:
            movementVector.x += -1.0f;
            break;
        case GLFW_KEY_D:
            movementVector.x += 1.0f;
            break;
        case GLFW_KEY_Q:
            if (freeCamera)
            {
                movementVector.y += -1.0f;
            }
            break;
        case GLFW_KEY_E:
            if (freeCamera)
            {
                movementVector.y += 1.0f;
            }
            break;
        case GLFW_KEY_C:
            usingMouse = !usingMouse;
            if (usingMouse)
            {
                glfwSetInputMode(GetCore().getWindowPtr(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            }
            else
            {
                glfwSetInputMode(GetCore().getWindowPtr(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            }
            break;
        case GLFW_KEY_M:
            freeCamera = !freeCamera;
            break;
        }

        break;
    case Event::KEY_RELEASED:

        switch (msg.getValue<int>())
        {
        case GLFW_KEY_W:
            movementVector.z -= -1.0f;
            break;
        case GLFW_KEY_S:
            movementVector.z -= 1.0f;
            break;
        case GLFW_KEY_A:
            movementVector.x -= -1.0f;
            break;
        case GLFW_KEY_D:
            movementVector.x -= 1.0f;
            break;
        case GLFW_KEY_Q:
            if (freeCamera)
            {
                movementVector.y -= -1.0f;
            }
            break;
        case GLFW_KEY_E:
            if (freeCamera)
            {
                movementVector.y -= 1.0f;
            }
            break;
        }
        break;

    case Event::MOUSE_CURSOR_MOVED:
    {
        if (usingMouse)
        {
            CursorData cursorData = msg.getValue<CursorData>();
            yaw += cursorData.xDelta * sensitivity;
            pitch -= cursorData.yDelta * sensitivity;
            pitch = freeCamera ? glm::clamp(pitch, -89.0f, 89.0f) : glm::clamp(pitch, -30.0f, 15.0f);
        }
    }
    break;
    }
}

void CameraSystem::fixedUpdate()
{
    glm::vec3 direction = glm::vec3(0.0f);
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction = glm::normalize(direction);
    glm::quat rotation = glm::quatLookAt(direction, up);
    transform->getLocalRotationModifiable() = rotation;

    glm::vec3 move =
        glm::normalize(glm::cross({direction.x, 0, direction.z}, up)) * movementVector.x +
        up * movementVector.y +
        glm::vec3(direction.x, 0, direction.z) * -movementVector.z;

    transform->getLocalPositionModifiable() += move * speed;
}