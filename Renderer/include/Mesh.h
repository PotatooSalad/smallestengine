#pragma once

#include <vector>
#include <glm/mat4x4.hpp>
#include "../../Core/include/globalDefs.h"
#include "MeshDataStructures.h"

struct Vertex;

class Mesh
{
public:
	Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, bool backFaceCulling = false);
	~Mesh();

	void render();
	void renderInstanced(int count, glm::mat4* instanceMatrices);

	unsigned int getMeshID() { return meshID; }
	std::vector<Vertex>* getVertices() { return &vertices; }
	std::vector<uint>* getIndices() { return &indices; }
	bool hasBackFaceCulling() { return backFaceCulling; }
	glm::vec3 getMeshCenter() { return meshCenter; }
	Bounds bounds;
private:
	unsigned int meshID;
	std::vector<Vertex> vertices;
	std::vector<uint> indices;
	glm::vec3 meshCenter;

	bool backFaceCulling;

	void setup();
	//void saveMeshToObj();

	uint vao, vbo, ebo, instanceVbo;
	static uint idCounter;
};

