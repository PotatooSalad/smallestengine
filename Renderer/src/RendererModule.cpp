#include "../include/RendererModule.h"
#include "../../Core/include/Message.inl"
#include "../../ECS/Components/Render/MeshRenderer.h"
#include "../include/Material.h"
#include "../include/Mesh.h"
#include "../include/Shader.h"
#include "../../Core/include/Core.h"
#include "../../ECS/Components/Render/Camera.h"
#include "../../ECS/Components/Transform.inl"
#include "../../ECS/Components/Render/Light.h"
#include "../../ECS/Components/Render/Window.h"
#include "../../ECS/Base/include/Entity.h"
#include "../include/Texture.h"
#include "../../Core/include/RendererShaders.inl"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <algorithm>
#include <random>
#include <typeinfo>

bool RendererModule::firstRender = true;
bool RendererModule::day = true;

void RendererModule::receiveMessage(Message msg)
{
    switch (msg.getEvent())
    {
    case Event::ADD_MESH:
    {
        MeshRenderer* mr = msg.getValue<MeshRenderer*>();
        if (mr->material->isInstancingEnabled())
        {
            auto packet = instancedPackets.insert({ key(mr->mesh->getMeshID(), mr->material->getID()), InstancedPacket(mr->mesh, mr->material)});
            packet.first->second.instanceMatrices.push_back(&mr->modelMatrix);
            packet.first->second.instanceOccluded.push_back(true);
        }
        else
        {
            if (mr->entityPtr->getComponentPtr<Window>() != nullptr)
            {
                normalPackets.push_back(NormalPacket(mr->mesh, mr->material, mr->modelMatrix, true));
                windowPosition = mr->modelMatrix[3];
                windowPosition.y = 15;
            }
            else
            {
                normalPackets.push_back(NormalPacket(mr->mesh, mr->material, mr->modelMatrix));
            }
        }
        break;
    }
    case Event::SET_CAMERA:
        mainCamera = msg.getValue<Camera*>();
        break;
    case Event::ADD_LIGHT:
        if (Light* light = msg.getValue<Light*>())
        {
            switch (light->lightType)
            {
            case LightType::Directional:
                directionalLight = light;
                break;
            case LightType::Spot:
                spotLights.push_back(light);
                break;
            default:
                break;
            }
        }
        break;
    case Event::KEY_PRESSED:
        switch (msg.getValue<int>())
        {
        /*case GLFW_KEY_F:
            frustumCullingEnabled = !frustumCullingEnabled;
            break;*/
        case GLFW_KEY_G:
            day = !day;
            firstRender = true;
            break;
        default:
            break;
        }
        break;

    case Event::WINDOW_RESIZED:
        glm::ivec2 size = msg.getValue<glm::ivec2>();
        /*glBindRenderbuffer(GL_RENDERBUFFER, gbufferRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x / 2, size.y / 2);

        glBindTexture(GL_TEXTURE_2D, gPosition->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x / 2, size.y / 2, 0, GL_RGBA, GL_FLOAT, 0);

        glBindTexture(GL_TEXTURE_2D, gNormal->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x / 2, size.y / 2, 0, GL_RGBA, GL_FLOAT, 0);*/

        glBindRenderbuffer(GL_RENDERBUFFER, gbufferRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, size.x, size.y);

        glBindTexture(GL_TEXTURE_2D, gPosition->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x, size.y, 0, GL_RGBA, GL_FLOAT, 0);

        glBindTexture(GL_TEXTURE_2D, gNormal->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x, size.y, 0, GL_RGBA, GL_FLOAT, 0);

        glBindRenderbuffer(GL_RENDERBUFFER, sceneRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size.x, size.y);

        glBindTexture(GL_TEXTURE_2D, scene->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x, size.y, 0, GL_RGBA, GL_FLOAT, 0);

        glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, size.x, size.y, 0, GL_RED, GL_FLOAT, 0);

        glBindTexture(GL_TEXTURE_2D, ssaoBlurBuffer->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x, size.y, 0, GL_RGBA, GL_FLOAT, 0);

        /*glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, size.x / 2, size.y / 2, 0, GL_RED, GL_FLOAT, 0);

        glBindTexture(GL_TEXTURE_2D, ssaoBlurBuffer->getID());
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, size.x / 2, size.y / 2, 0, GL_RGBA, GL_FLOAT, 0);*/

        glBindTexture(GL_TEXTURE_2D, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        break;
    }

}

void RendererModule::initialize(GLFWwindow* window, RendererModuleCreateInfo createInfo)
{
	this->window = window;
	this->createInfo = createInfo;

	if (createInfo.cullFace)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(createInfo.cullFaceMode);
		glFrontFace(createInfo.cullFrontFace);
	}

    if (createInfo.depthTest)
    {
        glEnable(GL_DEPTH_TEST);
    }

    if (createInfo.wireframeMode)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    if (createInfo.gammaCorrection)
    {
        glEnable(GL_FRAMEBUFFER_SRGB);
    }
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

    /// ------------- UBOs ---------------------
    glGenBuffers(1, &cameraBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, cameraBuffer);
    glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4) + sizeof(glm::vec4), nullptr, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, 0, cameraBuffer, 0, 2 * sizeof(glm::mat4) + sizeof(glm::vec4));

    uint lightBufferSize = 4 * sizeof(glm::vec4) + MAX_SPOTLIGHTS * (5 * sizeof(glm::vec4) + 4 * sizeof(float));
    glGenBuffers(1, &lightBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, lightBuffer);
    glBufferData(GL_UNIFORM_BUFFER, lightBufferSize, nullptr, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, 1, lightBuffer, 0, lightBufferSize);

    glGenBuffers(1, &shadowBuffer);
    glBindBuffer(GL_UNIFORM_BUFFER, shadowBuffer);
    glBufferData(GL_UNIFORM_BUFFER, (MAX_SPOTLIGHTS + 1) * sizeof(glm::mat4), nullptr, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferRange(GL_UNIFORM_BUFFER, 2, shadowBuffer, 0, (MAX_SPOTLIGHTS + 1) * sizeof(glm::mat4));


    //------------------------ LIGHTING DEPTH MAP FBO & TEXTURE ---------------------

    TextureCreateInfo texCreateInfo;
    texCreateInfo.format = GL_DEPTH_COMPONENT;
    texCreateInfo.internalFormat = GL_DEPTH_COMPONENT;
    texCreateInfo.generateMipmaps = false;
    texCreateInfo.type = GL_FLOAT;
    texCreateInfo.width = SHADOW_WIDTH;
    texCreateInfo.height = SHADOW_HEIGHT;
    texCreateInfo.minFilter = GL_NEAREST;
    texCreateInfo.magFilter = GL_NEAREST;
    texCreateInfo.wrapMode = GL_CLAMP_TO_BORDER;

    directionalDepthMap = new Texture((byte*)nullptr, texCreateInfo);
    // attach depth texture as FBO's depth buffer
    glGenFramebuffers(1, &directionalDepthMapFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, directionalDepthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, directionalDepthMap->getID(), 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(MAX_SPOTLIGHTS, spotDepthMapFBO);
    
    for (int i = 0; i < MAX_SPOTLIGHTS; ++i)
    {
        spotDepthMaps.push_back(new Texture((byte*)nullptr, texCreateInfo));
        glBindFramebuffer(GL_FRAMEBUFFER, spotDepthMapFBO[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, spotDepthMaps[i]->getID(), 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        lightsVP.push_back(glm::mat4(1.0));
    }

    lightsVP.push_back(glm::mat4(1.0));

    //--------------------------ENVIROMENTAL CUBEMAP-------------------------------------------
    texCreateInfo.cubemap = true;
    texCreateInfo.format = GL_RGB;
    texCreateInfo.internalFormat = GL_RGB16F;
    texCreateInfo.width = CUBEMAP_SIZE;
    texCreateInfo.height = CUBEMAP_SIZE;
    //texCreateInfo.type = GL_UNSIGNED_BYTE;
    texCreateInfo.minFilter = GL_LINEAR;
    texCreateInfo.magFilter = GL_LINEAR;
    texCreateInfo.wrapMode = GL_CLAMP_TO_EDGE;
    enviromentalMap = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    skyboxMap = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    windowReflectionMap = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);

    glGenFramebuffers(1, &cubeMapFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, cubeMapFBO);
    glGenRenderbuffers(1, &cubeMapRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, cubeMapRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, CUBEMAP_SIZE, CUBEMAP_SIZE);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, cubeMapRBO);

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    // ------------------------ IRRADIANCE ----------------------------------------------

    texCreateInfo.width = IRRADIANCE_SIZE;
    texCreateInfo.height = IRRADIANCE_SIZE;
    irradianceMap = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);

    glGenFramebuffers(1, &captureFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glGenRenderbuffers(1, &captureRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, IRRADIANCE_SIZE, IRRADIANCE_SIZE);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    irradianceShader = GetCore().objectModule.newShader(Shaders::irradianceFrag, Shaders::irradianceVert);

    // ------------------------------------- PREFILTER -------------------------------------------------
    texCreateInfo.width = PREFILTER_SIZE;
    texCreateInfo.height = PREFILTER_SIZE;
    texCreateInfo.generateMipmaps = true;
    texCreateInfo.minFilter = GL_LINEAR_MIPMAP_LINEAR;
    prefilterMap = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    prefilterShader = GetCore().objectModule.newShader(Shaders::prefilteredFrag, Shaders::irradianceVert);

    //-------------------------------------- BRDF --------------------------------------------------------
    texCreateInfo.cubemap = false;
    texCreateInfo.width = CUBEMAP_SIZE;
    texCreateInfo.height = CUBEMAP_SIZE;
    texCreateInfo.internalFormat = GL_RG;
    texCreateInfo.format = GL_RG;
    texCreateInfo.generateMipmaps = false;
    texCreateInfo.minFilter = GL_LINEAR;
    BRDFTexture = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    brdfShader = GetCore().objectModule.newShader(Shaders::brdfFrag, Shaders::brdfVert);

    // ----------------------------------------- SKYBOX --------------------------------------------------------
    float skyboxVertices[] = {
        // positions          
        -1.0f,  1.0f, -1.0f,    -1.0f, -1.0f, -1.0f,    1.0f, -1.0f, -1.0f,     1.0f, -1.0f, -1.0f,     1.0f,  1.0f, -1.0f,     -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,    -1.0f, -1.0f, -1.0f,    -1.0f,  1.0f, -1.0f,    -1.0f,  1.0f, -1.0f,    -1.0f,  1.0f,  1.0f,    -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,    1.0f, -1.0f,  1.0f,     1.0f,  1.0f,  1.0f,     1.0f,  1.0f,  1.0f,     1.0f,  1.0f, -1.0f,     1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,    -1.0f,  1.0f,  1.0f,    1.0f,  1.0f,  1.0f,     1.0f,  1.0f,  1.0f,     1.0f, -1.0f,  1.0f,     -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,    1.0f,  1.0f, -1.0f,     1.0f,  1.0f,  1.0f,     1.0f,  1.0f,  1.0f,     -1.0f,  1.0f,  1.0f,    -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,    -1.0f, -1.0f,  1.0f,    1.0f, -1.0f, -1.0f,     1.0f, -1.0f, -1.0f,     -1.0f, -1.0f,  1.0f,    1.0f, -1.0f,  1.0f
    };

    glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    skyboxShader = GetCore().objectModule.newShader(Shaders::cubeFragment, Shaders::cubeVertex);
    skyboxCountShader = GetCore().objectModule.newShader(Shaders::skyboxFragment, Shaders::cubeVertex);

    //-------------------------------------------------- QUAD ------------------------------------------------------
    float quadVertices[] = {
        // positions        // texture Coords
        -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
         1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
    };
    // setup plane VAO
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);
    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    // ---------------------------------------------- GBUFFER ------------------------------------------------------
    glGenFramebuffers(1, &gbufferFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, gbufferFBO);
    texCreateInfo.internalFormat = GL_RGBA16F;
    texCreateInfo.format = GL_RGBA;
    texCreateInfo.minFilter = GL_LINEAR;
    texCreateInfo.magFilter = GL_LINEAR;
    texCreateInfo.wrapMode = GL_CLAMP_TO_EDGE;
    /*texCreateInfo.width = GetCore().windowWidth / 2;
    texCreateInfo.height = GetCore().windowHeight / 2;*/
    texCreateInfo.width = GetCore().windowWidth;
    texCreateInfo.height = GetCore().windowHeight;
    gPosition = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    gNormal = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPosition->getID(), 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal->getID(), 0);
    uint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, attachments);

    glGenRenderbuffers(1, &gbufferRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, gbufferRBO);
    //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, GetCore().windowWidth / 2, GetCore().windowHeight / 2);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, GetCore().windowWidth, GetCore().windowHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, gbufferRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    gbufferShader = GetCore().objectModule.newShader(Shaders::gBufferFrag, Shaders::gBufferVert);
    gbufferShaderInstanced = GetCore().objectModule.newShader(Shaders::gBufferFrag, Shaders::gBufferInstancedVert);

    // -------------------------------------------- SCENE BUFFERS ------------------------------------------------
    glGenFramebuffers(1, &sceneFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
    texCreateInfo.internalFormat = GL_RGBA16F;
    texCreateInfo.width = GetCore().windowWidth;
    texCreateInfo.height = GetCore().windowHeight;
    scene = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, scene->getID(), 0);

    glGenRenderbuffers(1, &sceneRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, sceneRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, GetCore().windowWidth, GetCore().windowHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, sceneRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    combineShader = GetCore().objectModule.newShader(Shaders::sceneFrag, Shaders::ssaoVert);

    // -------------------------------------------- SSAO ----------------------------------------------------------
    glGenFramebuffers(1, &ssaoFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
    texCreateInfo.internalFormat = GL_RED;
    texCreateInfo.format = GL_RED;
    /*texCreateInfo.width = GetCore().windowWidth / 2;
    texCreateInfo.height = GetCore().windowHeight / 2;*/
    texCreateInfo.width = GetCore().windowWidth;
    texCreateInfo.height = GetCore().windowHeight;
    ssaoColorBuffer = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer->getID(), 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(1, &ssaoBlurFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
    ssaoBlurBuffer = GetCore().objectModule.newTexture((byte*)nullptr, texCreateInfo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoBlurBuffer->getID(), 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    std::uniform_real_distribution<float> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
    std::default_random_engine gen;
    for (unsigned int i = 0; i < 64; ++i)
    {
        glm::vec3 sample(randomFloats(gen) * 2.0 - 1.0, randomFloats(gen) * 2.0 - 1.0, randomFloats(gen));
        sample = glm::normalize(sample);
        sample *= randomFloats(gen);
        float scale = float(i) / 64.0f;

        // scale samples s.t. they're more aligned to center of kernel
        scale = glm::mix(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.push_back(sample);
    }
    texCreateInfo.internalFormat = GL_RGBA32F;
    texCreateInfo.format = GL_RGB;
    texCreateInfo.wrapMode = GL_REPEAT;
    texCreateInfo.width = 4;
    texCreateInfo.height = 4;
    float noise[16 * 3];
    for (unsigned int i = 0; i < 16; i++)
    {
        noise[i * 3 + 0] = randomFloats(gen) * 2.0f - 1.0f;
        noise[i * 3 + 1] = randomFloats(gen) * 2.0f - 1.0f;
        noise[i * 3 + 2] = 0.0f; // rotate around z-axis (in tangent space)

        float length = noise[i * 3 + 0] * noise[i * 3 + 0] + noise[i * 3 + 1] * noise[i * 3 + 1] + noise[i * 3 + 2] * noise[i * 3 + 2];
        length = glm::sqrt(length);
        noise[i * 3 + 0] = glm::abs(noise[i * 3 + 0]) / length;
        noise[i * 3 + 1] = glm::abs(noise[i * 3 + 1]) / length;
        noise[i * 3 + 2] = glm::abs(noise[i * 3 + 2]) / length;
    }
    ssaoNoise = GetCore().objectModule.newTexture(noise, texCreateInfo);

    ssaoShader = GetCore().objectModule.newShader(Shaders::ssaoFrag, Shaders::ssaoVert);
    ssaoBlurShader = GetCore().objectModule.newShader(Shaders::ssaoBlurFrag, Shaders::ssaoVert);
}

void RendererModule::render()
{
    if (mainCamera != nullptr)
    {
        // ======================================= FIRST FRAME RENDER ===============================================
        if (firstRender)
        {
            if (day)
            {
                for (auto l : spotLights)
                {
                    l->color = glm::vec4(0.0f);
                }
                directionalLight->color = DIRECTIONAL_COLOR;
            }
            else
            {
                for (auto l : spotLights)
                {
                    l->color = SPOTLIGHT_COLOR;
                }
                directionalLight->color = glm::vec4(0.0f);
            }
            shadowMapping();

            // ---------------------- SHADOW UBO ---------------------
            glBindBuffer(GL_UNIFORM_BUFFER, shadowBuffer);
            for (int i = 0; i <= MAX_SPOTLIGHTS; ++i)
            {
                glBufferSubData(GL_UNIFORM_BUFFER, i * sizeof(glm::mat4), sizeof(glm::mat4), &lightsVP[i]);
            }
            glBindBuffer(GL_UNIFORM_BUFFER, 0);

            /// --------------------- Light UBO -------------------------
            int spotlightIndex = 0;
            uint offset;
            glBindBuffer(GL_UNIFORM_BUFFER, lightBuffer);
            glBufferSubData(GL_UNIFORM_BUFFER, 0,                       sizeof(glm::vec3), &(directionalLight->positionInWorld));
            glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::vec4),       sizeof(glm::vec3), &(directionalLight->direction));
            glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::vec4),   sizeof(glm::vec4), &(directionalLight->ambient));
            glBufferSubData(GL_UNIFORM_BUFFER, 3 * sizeof(glm::vec4),   sizeof(glm::vec4), &(directionalLight->color));
            for (auto l : spotLights)
            {
                offset = 4 * sizeof(glm::vec4) + (spotlightIndex * (5 * sizeof(glm::vec4) + 4 * sizeof(float)));
                glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(float), &(l->cutOff));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + sizeof(float), sizeof(float), &(l->outerCutoff));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + 4 * sizeof(float), sizeof(glm::vec3), &(l->positionInWorld));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + 4 * sizeof(float) + sizeof(glm::vec4), sizeof(glm::vec3), &(l->direction));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + 4 * sizeof(float) + 2 * sizeof(glm::vec4), sizeof(glm::vec4), &(l->ambient));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + 4 * sizeof(float) + 3 * sizeof(glm::vec4), sizeof(glm::vec4), &(l->color));
                glBufferSubData(GL_UNIFORM_BUFFER, offset + 4 * sizeof(float) + 4 * sizeof(glm::vec4), sizeof(glm::vec3), &(l->lightRange));
                ++spotlightIndex;
            }
            glBindBuffer(GL_UNIFORM_BUFFER, 0);

            renderToCubemap();
        }

        /// --------------------- Camera UBO -------------------------
        glBindBuffer(GL_UNIFORM_BUFFER, cameraBuffer);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), &mainCamera->projectionMatrix);
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), &mainCamera->viewMatrix);
        glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), sizeof(glm::vec4), &mainCamera->getFrustum().position);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        glm::mat4 VP = mainCamera->projectionMatrix * mainCamera->viewMatrix;
        //glViewport(0, 0, Core::windowWidth / 2, Core::windowHeight / 2);
        glViewport(0, 0, Core::windowWidth, Core::windowHeight);
        if (!firstRender)
        {
            frustumCulling(mainCamera->getFrustum(), RenderMode::NORMAL);
        }
        else
        {
            allPackets();
        }
        ssao(VP);
        glViewport(0, 0, Core::windowWidth, Core::windowHeight);
        glClearColor(createInfo.clearColor.r, createInfo.clearColor.g, createInfo.clearColor.b, 1.0f);
        
        glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderSceneGraph(VP, RenderMode::NORMAL);

        glDepthFunc(GL_LEQUAL);
        glm::mat4 view = glm::mat4(glm::mat3(mainCamera->viewMatrix));
        VP = mainCamera->projectionMatrix * view;
        
        renderSkybox(skyboxShader, VP);
        glDepthFunc(GL_LESS);

        VP = mainCamera->projectionMatrix * mainCamera->viewMatrix;
        renderTransparent(VP);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        combineShader->use();
        combineShader->setInt("sceneAmbient", 0);
        combineShader->setInt("ssao", 1);
        scene->bind(0);
        ssaoBlurBuffer->bind(1);
        renderQuad();
        if (firstRender)
        {
            firstRender = false;
        }
    }
}

void RendererModule::renderSceneGraph(glm::mat4 VP, RenderMode mode)
{
    while (!opaqueQueue.empty())
    {
        opaqueQueue.front()->render(VP, mode, directionalDepthMap, spotDepthMaps.data(), irradianceMap, prefilterMap, BRDFTexture);
        opaqueQueue.pop_front();
    }
}

void RendererModule::renderTransparent(glm::mat4 VP)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask(GL_FALSE);
    while (!transparentQueue.empty())
    {
        if (transparentQueue.front()->window)
        {
            transparentQueue.front()->material->setVec3("centerPosition", windowPosition);
            transparentQueue.front()->renderTransparent(VP, windowReflectionMap);

        }
        else
        {
            transparentQueue.front()->renderTransparent(VP, enviromentalMap);
        }
        transparentQueue.pop_front();
    }
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
}

void RendererModule::renderSkybox(Shader* shader, glm::mat4 VP)
{
    shader->use();
    shader->setMat4("VP", VP);
    if (shader == skyboxCountShader)
    {
        if (day)
        {
            shader->setVec4("groundColor", glm::vec4(0.313, 0.266, 0.227, 1.0));
            shader->setVec4("skyColor", glm::vec4(0.392, 0.580, 0.725, 1.0));
            shader->setVec4("starsColor", glm::vec4(0.392, 0.580, 0.725, 1.0));
            shader->setVec4("zenithColor", glm::vec4(0.803, 0.890, 0.898, 1.0));
        }
        else
        {
            shader->setVec4("groundColor", glm::vec4(0.078, 0.070, 0.062, 1.0));
            shader->setVec4("skyColor", glm::vec4(0.0, 0.0, 0.0, 1.0));
            shader->setVec4("starsColor", glm::vec4(1.0, 1.0, 1.0, 1.0));
            shader->setVec4("zenithColor", glm::vec4(0.019, 0.066, 0.098, 1.0));
            
        }
        glBindVertexArray(skyboxVAO);
    }
    else
    {
        glBindVertexArray(skyboxVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxMap->getID());
    }
    // skybox cube
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
}

void RendererModule::shadowMapping()
{

    glm::mat4 directionalProjMat = directionalLight->lightProjectionMatrix;
    glm::mat4 directionalViewMat = directionalLight->lightViewMatrix;

    lightsVP[0] = directionalProjMat * directionalViewMat;

    //------------------------- CAMERA UBO -------------------------
    glBindBuffer(GL_UNIFORM_BUFFER, cameraBuffer);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), &directionalProjMat);
    glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), &directionalViewMat);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    //------------------------- RENDER ----------------------------
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, directionalDepthMapFBO);

    glClear(GL_DEPTH_BUFFER_BIT);

    frustumCulling(directionalLight->lightFrustum, RenderMode::SHADOW_MAPPING);
    renderSceneGraph(lightsVP[0], RenderMode::SHADOW_MAPPING);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    for (int i = 0; i < MAX_SPOTLIGHTS; ++i)
    {
        glm::mat4 spotProjMat = spotLights[i]->lightProjectionMatrix;
        glm::mat4 spotViewMat = spotLights[i]->lightViewMatrix;
        lightsVP[1 + i] = spotProjMat * spotViewMat;

        //------------------------- CAMERA UBO -------------------------
        glBindBuffer(GL_UNIFORM_BUFFER, cameraBuffer);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), &spotProjMat);
        glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), &spotViewMat);
        
        //---------------------------- RENDER ------------------------------
        glBindFramebuffer(GL_FRAMEBUFFER, spotDepthMapFBO[i]);
        glClear(GL_DEPTH_BUFFER_BIT);
        frustumCulling(spotLights[i]->lightFrustum, RenderMode::SHADOW_MAPPING);
        renderSceneGraph(lightsVP[1 + i], RenderMode::SHADOW_MAPPING);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void RendererModule::frustumCulling(const ViewFrustum frustum, RenderMode mode)
{
    if (frustumCullingEnabled)
    {
        calculateFrustumPlanes(frustum);
        calculateFrustumPoints(frustum);
    }
    bool shadowMapping = (mode == RenderMode::SHADOW_MAPPING);

    //Frustum Culling
    for (auto& packet : normalPackets)
    {
        if (objectInFrustum(packet.mesh->bounds, packet.modelMatrix))
        {
            if (shadowMapping && packet.material->hasShadowShader() || !shadowMapping)
            {
                switch (packet.material->getRenderType())
                {
                case RenderType::Opaque:
                    opaqueQueue.push_back(&packet);
                    break;
                case RenderType::Transparent:
                    transparentQueue.push_back(&packet);
                    break;
                default:
                    break;
                }
            }
        }
    }

    for (auto& packet : instancedPackets)
    {
        int i = 0;
        bool wholePacketOccluded = true;
        for (auto matrix : packet.second.instanceMatrices)
        {
            if (objectInFrustum(packet.second.mesh->bounds, *matrix))
            {
                packet.second.instanceOccluded[i] = false;
                wholePacketOccluded = false;
            }
            i++;
        }

        if (!wholePacketOccluded)
        {
            opaqueQueue.push_back(&(packet.second));
        }
    }
    std::sort(opaqueQueue.begin(), opaqueQueue.end(), [](RenderPacket* a, RenderPacket* b) {return a->material->getID() > b->material->getID(); });
}

void RendererModule::allPackets()
{
    for (auto& packet : normalPackets)
    {
        if (packet.material->hasShadowShader())
        {
            switch (packet.material->getRenderType())
            {
            case RenderType::Opaque:
                opaqueQueue.push_back(&packet);
                break;
            case RenderType::Transparent:
                transparentQueue.push_back(&packet);
                break;
            default:
                break;
            }
        }
    }

    for (auto& packet : instancedPackets)
    {
        int i = 0;
        for (auto matrix : packet.second.instanceMatrices)
        {
            packet.second.instanceOccluded[i] = false;
            i++;
        }
        opaqueQueue.push_back(&(packet.second));
    }
    std::sort(opaqueQueue.begin(), opaqueQueue.end(), [](RenderPacket* a, RenderPacket* b) {return a->material->getID() > b->material->getID(); });
}

void RendererModule::renderToCubemap()
{
    glm::vec3 targets[6] = {
        glm::vec3(+1.0f, 0.0f, 0.0f),
        glm::vec3(-1.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, +1.0f, 0.0f),
        glm::vec3(0.0f, -1.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, +1.0f),
        glm::vec3(0.0f, 0.0f, -1.0f)
    };
    glm::vec3 ups[6] = {
        glm::vec3(0.0f, -1.0f, 0.0f),
        glm::vec3(0.0f, -1.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, -1.0f),
        glm::vec3(0.0f, -1.0f, 0.0f),
        glm::vec3(0.0f, -1.0f, 0.0f)
    };

    glm::vec3 cameraPos(0, 15, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, CUBEMAP_SIZE, CUBEMAP_SIZE);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    glm::mat4 proj = glm::perspective(glm::radians(90.0f), 1.0f, 0.01f, 1.0f);
    glm::mat4 view, VP;
    glViewport(0, 0, CUBEMAP_SIZE, CUBEMAP_SIZE);
    glDepthFunc(GL_LEQUAL);
    for (int i = 0; i < 6; ++i)
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, skyboxMap->getID(), 0);
        view = glm::lookAt(glm::vec3(0), targets[i], ups[i]);
        VP = proj * view;
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        renderSkybox(skyboxCountShader, VP);
    }
    glDepthFunc(GL_LESS);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);



    proj = glm::perspective(glm::radians(90.0f), 1.0f, 0.01f, 100.0f);
    ViewFrustum frus;
    frus.aspectRatio = 1.0f;
    frus.farPlane = 100.0f;
    frus.nearPlane = 0.01f;
    frus.fov = 90.0f;
    frus.position = cameraPos;

    float tang = glm::tan(glm::radians(frus.fov / 2.0f));
    frus.Hnear = 2.0f * tang * frus.nearPlane;
    frus.Wnear = frus.Hnear * frus.aspectRatio;

    frus.Hfar = 2.0f * tang * frus.farPlane;
    frus.Wfar = frus.Hfar * frus.aspectRatio;

    // ========================= WINDOW REFLECTION MAP RENDER ================================

    glBindFramebuffer(GL_FRAMEBUFFER, cubeMapFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, cubeMapRBO);
    for (int i = 0; i < 6; ++i)
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, windowReflectionMap->getID(), 0);
        view = glm::lookAt(windowPosition, windowPosition + targets[i], ups[i]);
        VP = proj * view;

        frus.front = targets[i];
        frus.up = ups[i];
        frus.right = glm::cross(frus.front, frus.up);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //frustumCulling(frus, RenderMode::CUBEMAP_MAPPING);
        allPackets();
        renderSceneGraph(VP, RenderMode::CUBEMAP_MAPPING);
        glDepthFunc(GL_LEQUAL);
        view = glm::mat4(glm::mat3(view));
        VP = proj * view;

        renderSkybox(skyboxShader, VP);
        glDepthFunc(GL_LESS);
    }

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // ============================ TRUE ENVIRONMENTAL MAP RENDER ====================
    glBindFramebuffer(GL_FRAMEBUFFER, cubeMapFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, cubeMapRBO);
    frus.farPlane = 1000.0f;
    for (int i = 0; i < 6; ++i)
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, enviromentalMap->getID(), 0);
        view = glm::lookAt(cameraPos, cameraPos + targets[i], ups[i]);
        VP = proj * view;

        frus.front = targets[i];
        frus.up = ups[i];
        frus.right = glm::cross(frus.front, frus.up);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        frustumCulling(frus, RenderMode::CUBEMAP_MAPPING);
        renderSceneGraph(VP, RenderMode::CUBEMAP_MAPPING);
        glDepthFunc(GL_LEQUAL);
        view = glm::mat4(glm::mat3(view));
        VP = proj * view;

        renderSkybox(skyboxShader, VP);
        glDepthFunc(GL_LESS);
    }

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //-----------------irradiance map-----------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, IRRADIANCE_SIZE, IRRADIANCE_SIZE);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);

    irradianceShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxMap->getID());
    glViewport(0, 0, IRRADIANCE_SIZE, IRRADIANCE_SIZE);
    for (int i = 0; i < 6; ++i)
    {
        view = glm::lookAt(glm::vec3(0), targets[i], ups[i]);
        VP = proj * view;
        irradianceShader->setMat4("VP", VP);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irradianceMap->getID(), 0);
        // skybox cube
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindVertexArray(skyboxVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
    }
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //----------------------------prefiltered map-------------------------------
    prefilterShader->use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, enviromentalMap->getID());
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    uint maxMipLevels = 5;
    for (uint mip = 0; mip < maxMipLevels; ++mip)
    {
        uint mipSize = (uint)((double)PREFILTER_SIZE * glm::pow(0.5f, mip));
        glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipSize, mipSize);
        glViewport(0, 0, mipSize, mipSize);
        float roughness = (float)mip / (float)(maxMipLevels - 1);
        prefilterShader->setFloat("roughness", roughness);
        for (int i = 0; i < 6; ++i)
        {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, prefilterMap->getID(), mip);
            view = glm::lookAt(glm::vec3(0), targets[i], ups[i]);
            VP = proj * view;
            prefilterShader->setMat4("VP", VP);
            // skybox cube
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glBindVertexArray(skyboxVAO);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);
        }
    }
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //-----------------------BRDF-------------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
    glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, CUBEMAP_SIZE, CUBEMAP_SIZE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, BRDFTexture->getID(), 0);

    glViewport(0, 0, 512, 512);
    brdfShader->use();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderQuad();
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RendererModule::renderQuad()
{
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

void RendererModule::ssao(glm::mat4 VP)
{
    glBindFramebuffer(GL_FRAMEBUFFER, gbufferFBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    for (auto packet : opaqueQueue)
    {
        if (dynamic_cast<InstancedPacket*>(packet) != nullptr)
        {
            packet->renderWithShader(gbufferShaderInstanced, VP);
        }
        else
        {
            packet->renderWithShader(gbufferShader, VP);
        }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //------------------SSAO RENDERING-----------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ssaoShader->use();
    ssaoShader->setInt("gPosition", 0);
    ssaoShader->setInt("gNormal", 1);
    ssaoShader->setInt("texNoise", 2);
    glm::vec2 scale = glm::vec2(GetCore().windowWidth / 4.0f, GetCore().windowHeight / 4.0f);
    //glm::vec2 scale = glm::vec2(GetCore().windowWidth / 8.0f, GetCore().windowHeight / 8.0f);
    ssaoShader->setVec2("noiseScale", scale);
    for (int i = 0; i < 64; ++i)
    {
        ssaoShader->setVec3("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
    }
    gPosition->bind(0);
    gNormal->bind(1);
    ssaoNoise->bind(2);
    renderQuad();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //--------------------------- SSAO BLUR ---------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ssaoBlurShader->use();
    ssaoColorBuffer->bind(0);
    renderQuad();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RendererModule::clean()
{

}

void RendererModule::calculateFrustumPlanes(const ViewFrustum frustum)
{
    glm::vec3 nearCenter = frustum.position + frustum.front * frustum.nearPlane;
    glm::vec3 farCenter = frustum.position + frustum.front * frustum.farPlane;

    glm::vec3 aux, point, normal;

    frustumPlanes[NEARP][NORMAL] = frustum.front;
    frustumPlanes[NEARP][POINT] = nearCenter;

    frustumPlanes[FARP][NORMAL] = -frustum.front;
    frustumPlanes[FARP][POINT] = farCenter;

    point = nearCenter + frustum.up * frustum.Hnear / 2.0f;
    aux = point - frustum.position;
    aux = glm::normalize(aux);
    normal = glm::normalize(glm::cross(aux, frustum.right));
    frustumPlanes[TOP][NORMAL] = normal;
    frustumPlanes[TOP][POINT] = point;

    point = nearCenter - frustum.up * frustum.Hnear / 2.0f;
    aux = point - frustum.position;
    aux = glm::normalize(aux);
    normal = glm::normalize(glm::cross(frustum.right, aux));
    frustumPlanes[BOTTOM][NORMAL] = normal;
    frustumPlanes[BOTTOM][POINT] = point;

    point = nearCenter - frustum.right * frustum.Wnear / 2.0f;
    aux = point - frustum.position;
    aux = glm::normalize(aux);
    normal = glm::normalize(glm::cross(aux, frustum.up));
    frustumPlanes[LEFT][NORMAL] = normal;
    frustumPlanes[LEFT][POINT] = point;

    point = nearCenter + frustum.right * frustum.Wnear / 2.0f;
    aux = point - frustum.position;
    aux = glm::normalize(aux);
    normal = glm::normalize(glm::cross(frustum.up, aux));
    frustumPlanes[RIGHT][NORMAL] = normal;
    frustumPlanes[RIGHT][POINT] = point;
}

void RendererModule::calculateFrustumPoints(const ViewFrustum frustum)
{
    glm::vec3 nearCenter = frustum.position + frustum.front * frustum.nearPlane;
    glm::vec3 farCenter = frustum.position + frustum.front * frustum.farPlane;

    // =============== Z axis =========== Y axis ============================= X axis =========================
    frustumPoints[0] = nearCenter - (frustum.up * frustum.Hnear / 2.0f) - (frustum.right * frustum.Wnear / 2.0f); // -1, -1, -1
    frustumPoints[1] = farCenter - (frustum.up * frustum.Hfar / 2.0f) - (frustum.right * frustum.Wfar / 2.0f); // -1, -1, 1
    frustumPoints[2] = nearCenter + (frustum.up * frustum.Hnear / 2.0f) - (frustum.right * frustum.Wnear / 2.0f); // -1, 1, -1
    frustumPoints[3] = nearCenter - (frustum.up * frustum.Hnear / 2.0f) + (frustum.right * frustum.Wnear / 2.0f); // 1, -1, -1
    frustumPoints[4] = farCenter + (frustum.up * frustum.Hfar / 2.0f) + (frustum.right * frustum.Wfar / 2.0f); // 1, 1, 1
    frustumPoints[5] = nearCenter + (frustum.up * frustum.Hnear / 2.0f) + (frustum.right * frustum.Wnear / 2.0f); // 1, 1, -1
    frustumPoints[6] = farCenter - (frustum.up * frustum.Hfar / 2.0f) + (frustum.right * frustum.Wfar / 2.0f); // 1, -1, 1
    frustumPoints[7] = farCenter + (frustum.up * frustum.Hfar / 2.0f) - (frustum.right * frustum.Wfar / 2.0f); // -1, 1, 1
}

bool RendererModule::objectInFrustum(Bounds& meshBounds, glm::mat4& modelMatrix)
{
    bool result = false;

    int out, in;
    // ? +++++ Check for each frustum plane +++++
    for (size_t i = 0; i < 6; i++)
    {
        // Reset counters
        out = 0;
        in = 0;
        for (size_t j = 0; j < 8 && (in == 0 || out == 0); j++)
        {
            // ? +++++ Check if corner is outside or inside +++++
            if (pointToPlaneDistance(frustumPlanes[i][POINT], frustumPlanes[i][NORMAL], modelMatrix * glm::vec4(meshBounds.getPoint(j), 1.0f)) < 0.0f)
            {
                out++;
            }
            else
            {
                in++;
            }
        }

        // ? +++++ If all corners are out +++++
        if (!in)
        {
            return false;
        }
        // ? +++++ If some corners are in +++++
        else
        {
            result = true;
        }
    }
    return result;
}

float RendererModule::pointToPlaneDistance(glm::vec3& pointOnPlane, glm::vec3& planeNormal, glm::vec3 point)
{
    return glm::dot(point - pointOnPlane, planeNormal);
}