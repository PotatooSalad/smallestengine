#pragma once
#include <vector>
#include "../../../Renderer/include/MeshDataStructures.h"
#include "../../../Core/include/globalDefs.h"

class Cylinder
{
public:
	Cylinder(float height, float radius, int baseSegments, int heightSegments, glm::vec3 basePos = glm::vec3(0)) 
		: height(height), baseRadius(radius), baseSegments(baseSegments), heightSegments(heightSegments), basePos(basePos) 
	{
		unitCircleVertices = new glm::vec3[baseSegments];
		float sectorAngle = 0;
		float angle = 2.0f * (float)PI / (float)baseSegments;
		for (int i = 0; i < baseSegments; ++i)
		{
			sectorAngle = angle * i;
			unitCircleVertices[i] = glm::vec3(cos(sectorAngle), 0.0f, sin(sectorAngle));
		}
	}

	Cylinder(float height, float bottomRadius, float topRadius, int baseSegments, int heightSegments, float thickness, glm::vec3 basePos = glm::vec3(0))
		:height(height), baseRadius(bottomRadius), topRadius(topRadius), baseSegments(baseSegments),
		heightSegments(heightSegments), thickness(thickness), basePos(basePos) 
	{
		unitCircleVertices = new glm::vec3[baseSegments];
		float sectorAngle = 0;
		float angle = 2.0f * (float)PI / (float)baseSegments;
		for (int i = 0; i < baseSegments; ++i)
		{
			sectorAngle = angle * i;
			unitCircleVertices[i] = glm::vec3(cos(sectorAngle), 0.0f, sin(sectorAngle));
		}
	}
	~Cylinder()
	{
		delete[] unitCircleVertices;
	}

	void getCylinderMesh(std::vector<Vertex>& vertices, std::vector<uint>& indices);
	void getCylinderMeshBowl(std::vector<Vertex>& vertices, std::vector<uint>& indices);
private:
	float height = 0.0f;
	float baseRadius = 0.0f;
	float topRadius = 0.0f;
	float thickness = 0.0f;
	int baseSegments = 3;
	int heightSegments = 1;
	glm::vec3 basePos;
	glm::vec3* unitCircleVertices;
};