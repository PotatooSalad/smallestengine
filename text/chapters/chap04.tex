\chapter{Siatki geometryczne}
Bez siatek geometrycznych nie można w~ogóle mówić o~scenie. W~końcu, żeby przekazać jakiekolwiek dane do karty graficznej trzeba wysłać (w przypadku OpenGL) polecenie \verb|glBindVertexArray()|, a~zaraz po nim \verb|glDrawElements()| czy \verb|glDrawArrays()|, które według dokumentacji OpenGL\cite{openGL} w~dużym uproszczeniu podpinają dane wierzchołków do kontekstu i~rysują prymitywy wyznaczone przez te wierzchołki. Oczywiście, bez shaderów wyrenderowanych brył nie da się zobaczyć, ponieważ będą po prostu w~kolorze tła, ale nawet mając ustawione wszystkie uniformy shadera, dopóki nie zostanie wywołane \verb|draw()| te dane nie mają wartości.\par
Planując scenę starano się skupić na prostych bryłach geometrycznych, których algorytmy generowania są łatwe do napisania i~mało skomplikowane obliczeniowo. Wszystkie siatki są tworzone podczas inicjalizacji programu, jednak dobrze, żeby proces ich obliczania był krótki, żeby aplikacja uruchamiała się jak najszybciej. Ostatecznie zdecydowano się na 5~brył: prostopadłościan, sferę, płaszczyznę, cylinder i~stożek (i przy okazji stożek ścięty), przy czym dwa ostatnie generowane są jednym algorytmem, gdyż stożek można najprościej określić jako cylinder z~promieniem górnej podstawy równym zero. Poza podstawowymi siatkami stworzono również ich zmodyfikowane wersje, takie jak prostopadłościan z~otworem oraz wydrążony stożek (na kształt miski). Dalsza część tego rozdziału przedstawia działanie algorytmów wykorzystanych przy tworzeniu wyżej wymienionych siatek geometrycznych.
\section{Klasa Box}
Klasa \verb|Box| zajmuje się generowaniem siatki prostopadłościanu. Za parametry konstruktora (listing \ref{lst:boxConstructor}) klasy zostały wybrane pozycja punktu środka siatki oraz wielkość bryły we wszystkich trzech wymiarach. Punkt odniesienia (pivot) jest zawsze w~punkcie \verb|S = (0, 0, 0)| w~przestrzeni siatki. Większość prostopadłościanów ma ustawiony punkt środka obiektu na środku dolnej podstawy. Nie ma potrzeby obracania brył, a~punkt odniesienia umieszczony w~takim miejscu ułatwia ustawienie obiektu na scenie.\\\\
\begin{lstlisting}[caption={Konstruktor klasy Box}, label={lst:boxConstructor}]
Box::Box(glm::vec3 center, glm::vec3 size)
{
	this->center = center;
	this->size = size;
	leftDownFrontCorner = center - size / 2.0f;
	// Add size.z to make front corner
	leftDownFrontCorner.z += size.z;
}
\end{lstlisting}
Najważniejszym punktem w~procesie generowania siatki prostopadłościanu jest lewy dolny przedni wierzchołek bryły. Na podstawie jego pozycji obliczane są pozostałe 23 wierzchołki. Kolejność ich generowania została przestawiona na rysunku \ref{fig:boxIndices}. \\ 
Razem z~tablicą wierzchołków jest generowana tablica indeksów, których kolejne trójki wartości można określić dwoma wzorami:
\begin{eqnarray}
	triangle_{n1}(4 * n; 4 * n + 2; 4 * n + 3) \\
	triangle_{n2}(4 * n; 4 * n + 3; 4 * n + 1)
\end{eqnarray}
Gdzie n to numer ściany liczony od 0 w~kolejności: przód, tył, prawo, lewo, dół, góra.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{../figures/boxIndices.png}
	\caption{Rozkład wierzchołków w~prostopadłościanie (opracowanie własne)}
	\label{fig:boxIndices}
\end{figure}\par
Użycie takiej liczby wierzchołków pozwala na dokładniejsze określenie koordynatów tekstury. Zostało to wykorzystane do obliczenia pozycji każdej ze ścian w~przestrzeni tekstury, aby sprawiało wrażenie ,,zaginania się'' obrazu na rogach. Ta koncepcja nazwana na potrzeby tej pracy zawijaniem tekstury została przedstawiona na rysunku \ref{fig:boxTexture}. Obliczanie pozycji wierzchołka na teksturze sprowadza się do dwóch kroków:
\begin{enumerate}
	\item Ustalenie pozycji wierzchołka względem rozłożenia siatki, np. dla wierzchołka 0 jest to \verb|(u,v) = (size.y, size.y)|.
	\item Podzielenie pozycji przez długość tekstury; dla \verb|u| jest to \verb|2*size.y + size.x|, a~dla \\\verb|v| \dywiz{} \verb|2*size.y + size.z|.
\end{enumerate}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{../figures/boxTexture.png}
	\caption{Rozkład koordynatów tekstury na ściankach prostopadłościanu (opracowanie własne)}
	\label{fig:boxTexture}
\end{figure}\par
Zostało zauważone, że jeśli blaty ustawione na scenie mają być realistyczne, to główną ścianą musi być górna. Odchodzące ściany boczne otaczają w~przestrzeni tekstury ścianę główną w~taki sposób, że środek ściany głównej ma zawsze współrzędne tekstury równe \verb|(u,v) = (0.5, 0.5)|. Ponadto zostało założone, że górna i~dolna ściana będą na tych samych współrzędnych, ponieważ spód jest w~większości przypadków zasłonięty. Można tutaj równie dobrze wpisać losowe wartości, jednak lepiej by tekstura była w~miarę spójna, gdyż istnieją obiekty w~których dolna część bryły jest widoczna (np. górne szafki).\par
Zalety wykorzystania zawijania tekstury widać na pierwszy rzut oka. Powierzchnie drewniane sprawiają wrażenie jakby były zrobione z~jednego kawałka drewna czy deski. Podobnie jest z~pseudomarmurowymi blatami. Patrząc na nie można pomyśleć, że zostały obklejone ciągłym kawałkiem okleiny. Są to efekty jak najbardziej pożądane, przemawiające za realistycznością sceny.\\
\subsection{Prostopadłościan z~otworem}
Klasa \verb|Box| posiada również możliwość tworzenia wariacji bryły, do której najzwyczajniej pasuje nazwa ,,prostopadłościan z~otworem''. Przygotowany został na potrzeby tworzenia obiektów, które nie są w~stanie mieć ciągłej siatki, ale nie jest pożądane tworzenie osobnych encji z~osobnymi fragmentami bryły. Na scenie taka modyfikacja prostopadłościanu została użyta do stworzenia blatu pod oknem. Wymagany w tym miejscu był obiekt z jednolicie rozłożoną teksturą, ale równocześnie z otworem w którym mieści się zlew. W przypadku czterech osobnych brył składających się na jeden blat trudne jest spełnienie warunku ciągłej tekstury na całej złączonej powierzchni. Jedynym rozwiązaniem mogłoby być zaimplementowanie mapowania tekstury w~przestrzeni świata. Dodaje to jednak obliczeń do fragment shadera co w~pewnym stopniu przekłada się na wydajność. Nie znaleziono jednak potrzeby implementacji tego rozwiązania, gdyż zdecydowano się na rozszerzenie algorytmu zawijania tekstury i dostosowania go dla zmodyfikowanych prostopadłościanów.\par
Aby stworzyć prostopadłościan z~otworem siatka dzielona jest na 4 bryły. Jeśli otwór jest na tyle duży, że jedna z~brył w~którymś z~wymiarów ma wielkość 0, siatka dla tej części nie jest generowana. Taka sytuacja pojawia się tylko w~przypadku ściany z~drzwiami, jednak przy każdym generowaniu prostopadłościanu z~otworem sprawdzane jest czy jakaś część przypadkiem powinna nie istnieć. Rozkład figur razem z~ich oznaczeniami został przedstawiony na rysunku \ref{fig:boxHoled}.\\
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{../figures/holedBox.png}
	\caption{Podział prostopadłościanu na mniejsze siatki (opracowanie własne)}
	\label{fig:boxHoled}
\end{figure}\\\par
Podczas wyznaczania pozycji nowych prostopadłościanów trzeba wziąć pod uwagę, w~której płaszczyźnie przechodzi otwór. Zostało założone, że ściana, która jest najkrótsza wyznacza oś przechodzenia otworu. Pozostałe dwa wymiary stają się płaszczyzną, na której dokonywany jest podział na mniejsze bryły (według rysunku \ref{fig:boxHoled}). Dla każdego wydzielonego prostopadłościanu liczony jest rozmiar w~płaszczyźnie podziału (trzeci wymiar brany jest z~bazowej bryły). Pomocniczo jest również wyznaczany lewy dolny przedni wierzchołek, który służy jedynie do obliczenia pozycji środka. Na podstawie parametrów tworzone są obiekty typu \verb|Box|. Dane wierzchołków i~indeksów wszystkich podbrył są łączone w~ciągłą sekwencję i~na ich podstawie generowana jest siatka. Cały proces obliczania i~tworzenia podobiektów dla przypadku otworu w~osi Y przedstawiono na listingu \ref{lst:getHoledBox}.
\begin{lstlisting}[caption={Funkcja obliczająca pozycje i~rozmiary podbrył dla prostopadłościanu z otworem}, label={lst:getHoledBox}]
void Box::getHoledBox(glm::vec3 holePosition, glm::vec3 holeSize,
 std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	float min = glm::min(glm::min(size.x, size.y), size.z);
	//[...]
	if((min - size.y) < 0.001f) //float comparison
	{
		boxSize1 = { glm::distance(leftDownFrontCorner.x, holePosition.x), size.y, size.z };
		boxCenter1 = leftDownFrontCorner + (boxSize1 / 2.0f);
		offset1 = { size.y , 0.0f, size.y };
		
		boxSize2 = {holeSize.x, size.y, holePosition.z - leftDownFrontCorner.z};
		glm::vec3 ldfc2 = leftDownFrontCorner + {boxSize1.x, 0, 0};
		boxCenter2 = ldfc2 + (boxSize2 / 2.0f);
		offset2 = { boxSize1.x + size.y, 0.0f, size.y };
		
		boxSize3 = { holeSize.x, size.y, size.z - boxSize2.z - holeSize.z };
		glm::vec3 ldfc3 = { ldfc2.x, leftDownFrontCorner.y, holePosition.z + holeSize.z };
		boxCenter3 = ldfc3 + (boxSize3 / 2.0f);
		offset3 = { boxSize1.x + size.y, 0.0f, boxSize2.z + holeSize.z + size.y };
		
		boxSize4 = { size.x - holeSize.x - boxSize1.x, size.y, size.z };
		glm::vec3 ldfc4 = leftDownFrontCorner + {boxSize1.x + holeSize1.x, 0, 0};
		boxCenter4 = ldfc4 + (boxSize4 / 2.0f);
		offset4 = { boxSize1.x + boxSize2.x + size.y, 0.0f, size.y };
		
		texSize.x = boxSize1.x + boxSize2.x + boxSize4.x + 2 * size.y;
		texSize.y = 0.0f;
		texSize.z = boxSize1.z + 2 * size.y;
	}

	if (boxSize1.x > 0.01f && boxSize1.y > 0.01f && boxSize1.z > 0.01f)
	{
		Box child(boxCenter1, boxSize1, offset1, texSize);
		child.getBox(vertices, indices);
	}
//etc.
}
\end{lstlisting}
Do konstrukcji podobiektów siatki wykorzystany został drugi konstruktor klasy \verb|Box| przyjmujący dwa dodatkowe parametry -- offset tekstury oraz sumę długości ścian bezpośrednio wchodzących w~rozkład tekstury. W~tym przypadku rozkład współrzędnych pozwala na zawijanie się obrazu, jak również na spójne połączenia między podbryłami. Innymi słowy, prostopadłościan z~otworem wygląda jakby był obklejony jednym kawałkiem okleiny. \par
Rozkład tekstury zależny jest też od osi płaszczyzny podziału, to znaczy jeśli płaszczyzna podziału jest w~osiach XZ, bazową ścianą na której jest skupione zawijanie tekstury jest ściana górna. W~przypadku płaszczyzny XY główną ścianą jest prawa, a~dla ZY -- frontowa. Ułożenie tekstury dla płaszczyzny XZ zostało przedstawione na rysunku \ref{fig:holedTexture}.\par
Obliczanie współrzędnych tekstury dla prostopadłościanu z otworem sprowadza się do dodania offsetu do pozycji wierzchołka (w przestrzeni tekstury) oraz podzielenie tej wartości przez sumę długości odpowiednich ścian składających się na ,,długość tekstury'' (\verb|texSize|). Zmiana dzielnika w~formule jest tutaj istotna, gdyż powoduje to znormalizowanie współrzędnych tekstury i~wyeliminowanie powtarzania się obrazu na obiekcie. Wykorzystanie offsetu pozwala na kontynuowanie tekstury na kolejnych podobiektach, by nałożona na bryłę wydawała się być spójna i~jednolita.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{../figures/holedBoxTexture.png}
	\caption{Rozkład współrzędnych tekstury dla prostopadłościanu z otworem (opracowanie własne)}
	\label{fig:holedTexture}
\end{figure}
\section{Klasa Cylinder}
Klasa \verb|Cylinder| generuje siatki graniastosłupów oraz ostrosłupów prawidłowych. Wygenerowanie prawdziwego cylindra lub stożka w~powierzchniowej reprezentacji wielokątowej jest praktycznie niemożliwe, zawsze te bryły są przybliżane wielokątami o~dużej liczbie wierzchołków w~podstawie zamiast kół. Poza liczbą ścian parametrami do wygenerowania podstawowej siatki są: wysokość, promienie podstaw, liczba segmentów wysokości oraz pozycja środka bryły. Jeśli promienie podstaw są równe, wynikową siatką jest cylinder, jeśli są różne -- stożek, przy czym dla jednej z~podstaw równej 0 jest to zwykły stożek, a~dla obu większych od zera -- stożek ścięty. Poza tym, w~konstruktorze (listing \ref{lst:cylinderConstructor}) można przekazać parametr \verb|thickness|, który powoduje, że zamiast prostej bryły generuje się jej ,,wydrążona'' wersja: dla cylindra -- rura, dla stożka -- lejek i~dla stożka ściętego -- miska.
\begin{lstlisting}[caption={Konstruktor klasy Cylinder}, label={lst:cylinderConstructor}]
Cylinder(float height, float bottomRadius, float topRadius, 
int baseSegments, int heightSegments, float thickness = 0.0f, 
glm::vec3 basePos = glm::vec3(0))
{
	//assign values to their respective class variables
}
\end{lstlisting}
\subsection{Okrąg jednostkowy}
Istotną strukturą na której bazuje cały algorytm generowania siatek brył obrotowych jest okrąg (a raczej wielokąt) jednostkowy. Określa on pozycje wierzchołków ,,okręgu'' o~promieniu 1. Na podstawie tej struktury wyznaczane są wszystkie pozostałe wierzchołki siatki. Odbywa się to przez pomnożenie wartości okręgu jednostkowego w~danym punkcie, a~wartość we współrzędnej y określa odległość punktu od podstawy. Dodatkowo, wartości współrzędnych wierzchołków okręgu jednostkowego określają wektory normalne ścian między tymi wierzchołkami. Współrzędne wierzchołków ,,okręgu'' jednostkowego są obliczane na podstawie wzoru zaczerpniętego z~artykułu\cite{cyl}:
\begin{equation}
	vert_{i} = (\cos(angle * i), 0, \sin(angle * i)),
\end{equation}
gdzie
\begin{equation}
	angle = \frac{2 * \pi}{baseSegments}
\end{equation}
\subsection{Cylinder i~Stożek}
Algorytm generowania cylindra zaczyna się od wyznaczenia środków dolnej i~górnej podstawy oraz zapisania ich do tablicy wierzchołków. Dzięki temu przy tworzeniu tablicy indeksów wierzchołków trójkątów podczas zapisywania podstawy następuje odwołanie się do punktu środka przez indeks 0 lub 1, w~zależności która podstawa jest generowana.\\
Po zapisaniu dwóch głównych wierzchołków obliczane są po kolei:
\begin{enumerate}
	\item Trójkąt dolnej podstawy.
	\item Wszystkie segmenty składające się na ścianę -- od dołu do góry; ich liczbę wyznacza \verb|heightSegments|.
	\item Trójkąt górnej podstawy.
\end{enumerate}
I te trzy kroki wykonują się w~pętli, której warunkiem końca jest osiągnięcie przez licznik wartości \verb|baseSegments|.\par
Współrzędne tekstury dla podstaw prostych wersji siatek są rozłożone tak, że podstawy są rozciągnięte między 0 a~1 w~przestrzeni tekstury. Wygląda to tak jakby wycięto koło w~obrazie i~naklejono je na podstawę. Dla ścian bocznych tekstura rozkłada się w~równych kawałkach, jakby była owinięta wokół wertykalnej części bryły. Przykładową siatkę cylindra wyznaczoną wyżej przedstawionym algorytmem zaprezentowano na rysunku \ref{fig:cylinder}.\par
W przypadku stożka algorytm generacji wygląda praktycznie tak samo. Różni się tylko tym, że przy każdym segmencie ścian bocznych trzeba obliczyć promień dla danej wysokości. Skoro ściany boczne są dzielone na segmenty o~równej wysokości, długości kolejnych promieni także różnią się stałą wartością. Jest to dokładnie różnica między promieniem górnej i~dolnej podstawy podzielona przez liczbę segmentów ścian bocznych. Ta wielkość pomnożona przez indeks segmentu i~dodana do promienia podstawy daje wartość promienia w~tym segmencie.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{../figures/cylinder.png}
	\caption{Siatka ,,cylindra'' o~20 ścianach bocznych (opracowanie własne)}
	\label{fig:cylinder}
\end{figure}
\subsection{Wydrążone wariacje}
Jeśli parametr \verb|thickness| jest większy od 0, tworzona jest wariacja siatki -- dla cylindra rura, dla stożka lejek, a~dla stożka ściętego -- miska. W~scenie używana jest tylko ta ostatnia bryła, jednakże algorytm jest tak skonstruowany, że ma możliwość stworzenia takich siatek. \verb|Thickness| tutaj wyznacza różnicę między wewnętrznym a~zewnętrznym promieniem modyfikacji, innymi słowy -- grubość bryły. Dla miski grubość przekłada się też na odległość między zewnętrzną (dolną) a~wewnętrzną podstawą.\par
Podczas tworzenia siatki wariacji warto zwrócić uwagę na to, że górna (i dolna w~przypadku rury) podstawa składa się teraz z~dwóch połączonych okręgów, więc trzeba generować jej wierzchołki według schematu tworzenia segmentu ściany. Dodatkowo, trzeba uwzględnić generowanie ściany od wewnętrznej strony, więc po przejściu podstawowych trzech kroków algorytmu, powtarzany jest krok drugi, ale dla wewnętrznych ścian (wykonywany od góry do dołu). W przypadku generowania siatki miski, podczas obliczania wierzchołków ścian wewnętrznych, sprawdzane jest czy przypadkiem następny segment nie przejdzie poniżej wewnętrznej podstawy. Wówczas ostatni segment jest na wysokości \verb|basePos.y + thickness| i~obliczane są trójkąty wewnętrznej podstawy. Wynikowa siatka została przedstawiona na rysunku \ref{fig:bowl}. Uproszczona wersja opisanego wyżej algorytmu przedstawiona została na listingu \ref{lst:cylAlgotithm}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.35\textwidth]{../figures/bowl1.png}
	\includegraphics[width=0.35\textwidth]{../figures/bowl2.png}
	\caption{Siatka miski o~grubości 1 (opracowanie własne)}
	\label{fig:bowl}
\end{figure}
\begin{lstlisting}[caption={Uproszczona implementacja generowania cylindra i~jego modyfikacji}, label={lst:cylAlgotithm}]
//bottom center vertex
Vertex v(basePos);
v.normal = glm::vec3(0.0f, -1.0f, 0.0f);
v.texcoord = { 0.5, 0.5 };
vertices.push_back(v);
bool isThickness = thickness > 0.001f;
//top center vertex if thickness is 0
if (!isThickness)
{
	v.position = basePos + glm::vec3(0.0f, height, 0.0f);
	v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	v.texcoord = { 0.5, 0.5 };
	vertices.push_back(v);
}
for (int i = 0; i < baseSegments; ++i)
{
	indices.push_back(0);
	//bottom triangle vertex
	v.position = unitCircleVertices[i] * baseRadius + basePos;
	//"rotated" vertex
	v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + basePos;
	//add indices of both vertices
	for(int j = 1; j < heightSegments; ++j)
	{
		lastIndex = vertices.size();
		//lower vertices
		heightRadius = baseRadius + (float)(j - 1) / heightSegments * (topRadius - baseRadius);
		v.position = unitCircleVertices[i] * heightRadius + 
		glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
		//rotated vertex
		v.position = unitCircleVertices[(i + 1) % baseSegments] * heightRadius + 
		glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
		//do the same for j / heightSegments
		//add indices in order lastIndex + {0-2-3} {0-3-1}
	}
	lastIndex = vertices.size();
	//add top triangle vertex (bottom triangle vertex + (0, height, 0))
	if(thickness)
	{
		//add vertex from inner circle
	}
	//add "rotated" top vertex
	if(thickness)
	{
		//add "rotated" vertex from inner circle
		//add indices in order lastIndex + {0-2-3} {0-3-1}
	}
	else
	{
		indices.push_back(1);
		//add top vertices: lastIndex, lastIndex + 1
	}
	
	if(isThickness)
	{
		for(int j = heightSegments; j >= 1; --j)
		{
			//add inner wall vertices same way as outer but from higher to lower
			if ((segHeight * (j - 1)) < thickness)
			{
				//add last wall vertices on height basePos.y + thickness 
				//and inner base triangle vertices
			}
		}
	}
}
\end{lstlisting}
\section{Klasa Sphere}
Klasa \verb|Sphere| generuje sferę, czyli powierzchnię, na której punkty są równoodległe od środka. Parametrami konstruktora są promień, liczba sektorów i~stosów (z ang. stacks). Sektory i~stosy określają odpowiednio podział horyzontalny i~wertykalny sfery (przedstawiony na rysunku \ref{fig:sphered}).\par
Taki rozkład bryły pozwala na wyznaczenie kąta stosowego oraz sektorowego, z~których już prosta droga do policzenia wierzchołków sfery. Kąt sektorowy (${\beta}$) to kąt między osią X a~projekcją tego punktu na płaszczyznę XY. Kąt stosowy (${\alpha}$) jest określany jako kąt między projekcją punktu na płaszczyznę XY a~promieniem poprowadzonym od środka do tego punktu. Dokładny rozkład kątów został przedstawiony na rysunku \ref{fig:spherePoint}. Wykorzystując wyznaczone wartości współrzędne wierzchołka są obliczane ze wzoru:\\\\
\begin{eqnarray}
	x = (r * \cos\alpha) * \cos\beta \\
	y = (r * \cos\alpha) * \sin\beta \\
	z = r * \sin\alpha
\end{eqnarray}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/sphere.png}
		\caption{Definicja sektorów i~stosów}
		\label{fig:sphered}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/spherePoint.png}
		\caption{Pozycja kąta stosowego (${\alpha}$) i~sektorowego (${\beta}$)}
		\label{fig:spherePoint}
	\end{subfigure}
	\caption{Składowe sfery (opracowanie własne)}
\end{figure}\par
Wektory normalne ścian oblicza się dzieląc koordynaty pozycji wierzchołka przez promień sfery. Współrzędne tekstury to odpowiednio indeks sektora/stosu podzielony przez liczbę sektorów/stosów. W~algorytmie generowania sfery wierzchołki obliczane są od dołu do góry, sektor po sektorze. Dla wyznaczania trójkątów siatki są stworzone pomocnicze zmienne, które przechowują indeks wierzchołka z~aktualnego sektora i~sektora wyżej (jeśli oczywiście istnieje następny). Zaimplementowany algorytm tworzenia siatki sfery został przedstawiony na listingu \ref{lst:getSphere}.
\begin{lstlisting}[caption={Implementacja algorytmu generowania siatki sfery}, label={lst:getSphere}]
void Sphere::getSphere(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	Vertex vert;
	float xy;
	float sectorStep = 2 * PI / sectors;
	float stackStep = PI / stacks;
	float sectorAngle, stackAngle;
	//stack indices
	int lower = 0;
	int higher = 0;
	for (int i = 0; i <= stacks; ++i)
	{
		stackAngle = PI / 2 - i * stackStep;
		xy = radius * glm::cos(stackAngle);
		vert.position.z = radius * glm::sin(stackAngle);
		if (i < stacks)
		{
			lower = i * (sectors + 1);
			higher = lower + sectors + 1;
		}
		for (int j = 0; j <= sectors; ++j, ++lower, ++higher)
		{
			sectorAngle = j * sectorStep;
			vert.position.x = xy * glm::cos(sectorAngle);
			vert.position.y = xy * glm::sin(sectorAngle);

			vert.normal.x = vert.position.x / radius;
			vert.normal.y = vert.position.y / radius; 
			vert.normal.z = vert.position.z / radius;
			vert.texcoord.x = (float)j / sectors;
			vert.texcoord.y = (float)i / stacks;
			vertices.push_back(vert);
			if (i < stacks && j < sectors)
			{
				if (i != 0)
				{
					indices.push_back(lower);
					indices.push_back(higher);
					indices.push_back(lower + 1);
				}
				if (i != stacks - 1)
				{
					indices.push_back(lower + 1);
					indices.push_back(higher);
					indices.push_back(higher + 1);
				}
			}
		}
	}
}
\end{lstlisting}
% ================================================================================================================
\section{Klasa Plane}
Klasa \verb|Plane| generuje płaszczyznę. Mimo prostoty siatki wybrane zostały aż 4 parametry konstruktora: 
\begin{itemize}
	\item pozycja środka płaszczyzny,
	\item liczba segmentów (w obu osiach),
	\item wielkość segmentu (w obu osiach, domyślnie \verb|(1, 1)|),
	\item kierunek normalnych (domyślnie \verb|(0, 1, 0)|).
\end{itemize}
Algorytm obliczania danych siatki płaszczyzny zaczyna się od obliczenia pozycji lewego dolnego rogu, a~później, w~dwóch pętlach przechodząc po kolejnych segmentach generowane są kolejne wierzchołki i~współrzędne tekstury. Cały proces został przedstawiony na listingu \ref{lst:plane}.
\begin{lstlisting}[caption={Implementacja algorytmu generującego siatkę płaszczyzny}, label={lst:plane}]
for (int x = 0; x <= segments.x; ++x)
{
	for (int z = 0; z <= segments.y; ++z)
	{
		v.position.x = leftDown.x + x * segmentSize.x;
		v.position.y = leftDown.y;
		v.position.z = leftDown.z + z * segmentSize.y;
		v.normal = normals;
		
		v.texcoord.x = x / segments.x;
		v.texcoord.y = z / segments.y;
		vertices.push_back(v);
		//ySize = segments.y + 1
		if (x < segments.x && z < segments.y )
		{
			indices.push_back(x * ySize + z);
			indices.push_back(x * ySize + z + 1);
			indices.push_back((x + 1) * ySize + z + 1);
			indices.push_back(x * ySize + z);
			indices.push_back((x + 1) * ySize + z + 1);
			indices.push_back((x + 1) * ySize + z);
		}
	}
}
\end{lstlisting}