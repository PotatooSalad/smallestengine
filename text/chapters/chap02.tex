\chapter{Silnik graficzny}
\section{Architektura aplikacji}
Silnik graficzny aplikacji został zaimplementowany w~języku C++ i~OpenGL, w~większości na podstawie poradnika przetłumaczonego przez mgra inż. Tomasza Gałaja \cite{learnOpenGL}. Dodatkowo, do tworzenia obiektów aplikacji została użyta architektura Entity\dywiz{}Component\dywiz{}System, która może wydawać się nieco na wyrost, jednakże została zastosowana głównie by mieć możliwość tworzenia obiektów w~uporządkowany sposób oraz żeby praca związana z~przekazywaniem danych do renderera odbywała się sama. Poza tym, architektura ECS jest na tyle uniwersalna, że jeśli pojawiłyby się chęci stworzenia w~przyszłości bardziej złożonej aplikacji na tym silniku, np. gry to nie byłoby z~tym problemu. Wystarczyłoby dołączyć komponenty związane z~silnikiem gry, np. fizykę, kolizje czy przeciwników.\par 
Innym istotnym elementem aplikacji jest menedżer obiektów, który zajmuje się głównie zarządzaniem pamięcią w~trakcie działania programu. Jest zaprojektowany w~taki sposób, by zapobiegał wszelkim wyciekom i~niepoprawnym odwołaniom do pamięci. Po zakończeniu aplikacji zaalokowane miejsce jest czyszczone i~zwalniane, więc ma mowy o~zostawianiu tzw. śmieci w~pamięci. Diagram klas przedstawiający architekturę Entity\dywiz{}Component\dywiz{}System został przedstawiony na rysunku \ref{fig:engArch}.\par
W aplikacji można wyróżnić 4 główne komponenty: \verb|Transform|, \verb|Camera|, \verb|MeshRenderer| oraz \verb|Light|. Dla klasy \verb|Transform| utworzony jest moduł zarządzania sceną, który iterując się od węzła głównego po kolejnych dzieciach w~grafie, oblicza pozycje i~rotacje wszystkich elementów w~przestrzeni świata. Do optymalizacji grafu został użyty wzorzec projektowy dirty flag, wzięty z~,,Game Design Patterns'' \cite{patterns}. Trzy ostatnie komponenty mają swoje odpowiednie systemy, które albo ustawiają konkretne wartości zmiennych, albo przesyłają dane z~komponentów do modułu renderera. Każdy z~systemów ma możliwość implementacji trzech metod: 
\begin{itemize}
	\item \verb|start()|, wywołujący się przed pierwszą klatką działania aplikacji,
	\item \verb|frameUpdate()|, wywołujący się co klatkę, zaraz przed renderem,
	\item \verb|fixedUpdate()|, wywołujący się w~stałym kroku czasowym (co ${\sim}$16ms).
\end{itemize}
Ostatnia wymieniona wyżej funkcja wykorzystywana jest tylko przy systemie kamery, aby poruszanie i~obracanie się na scenie było płynne (niezależne od liczby klatek na sekundę).
\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{figures/engineArchitecture.png}
	\caption{Diagram UML architektury ECS wraz z~menadżerem obiektów (opracowanie własne)}
	\label{fig:engArch}
\end{figure}\par
Na moduł renderera składa się wiele klas. Jako podstawowe zostały określone klasy \verb|Texture|, \verb|Shader| i~\verb|Mesh|, ponieważ, używając poleceń OpenGL, komunikują się bezpośrednio z~kartą graficzną, tworzą odpowiednie bufory w~pamięci oraz przekazują do nich dane. Ponad nimi jest klasa \verb|Material|, która przechowuje wartości uniformów dla przypisanych do niej shaderów i~ustawia je automatycznie gdy shader jest używany. Takie rozwiązanie jest wygodne, ponieważ trzeba pamiętać o~ustawieniu wartości uniformów tylko raz, podczas tworzenia materiału.\par 
Wskaźniki do meshy i~materiałów (oraz macierz modelu) przechowywane są w~tak zwanych ,,paczkach'' (\verb|RenderPacket|), które są przekazywane do renderera. Te pakiety później są przydzielane do kolejek renderingu ze względu na obsługę przeźroczystości przez materiał. Potem następuje sortowanie kolejek. Elementy przeźroczyste ustawia się w~zależności od odległości od kamery (od najdalszego do najbliższego), by mógł poprawnie wykonać się blending. Dla nieprzeźroczystych obiektów następuje sortowanie po identyfikatorze materiału, co jest zabiegiem czysto optymalizacyjnym. Materiały o~takim samym id mają dokładnie taki sam zestaw shaderów. Gdy w~kolejce będzie kilka obiektów z~tym samym materiałem, nie trzeba przełączać shadera, czyli nie ma potrzeby od nowa ustawiać uniformów. Dokładna struktura renderera została przedstawiona na rysunku \ref{fig:rendereruml}.
\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{figures/rednererArchitecture.png}
	\caption{Diagram UML architektury renderera (opracowanie własne)}
	\label{fig:rendereruml}
\end{figure}
\section{Renderowanie sceny}
Przed zobaczeniem każdej kolejnej klatki aplikacji wykonywane jest dużo obliczeń tzw. pozaekranowych (z ang. off-screen). Jest to związane z~generowaniem tekstur i~map, które mają wspomóc realizm w~odbiorze sceny. Proces renderowania został podzielony na kilka części, które zostaną objaśnione w~dalszej części tej sekcji.
	\subsection{Mapowanie cieni}
Pierwszym krokiem w~tworzeniu sceny jest mapowanie cieni (z ang. shadow mapping). Podczas inicjalizacji aplikacji dla każdego światła tworzony jest bufor ramki (z ang. Framebuffer) przechwytujący głębokość oraz tekstura o~rozmiarze 1024 na 1024 pikseli, do której są zapisywane wartości z~bufora. W~trakcie generowania map najpierw wykonują się obliczenia dla światła kierunkowego, a~potem dla reflektorów. Dla każdego źródła oświetlenia odbywa się to w~taki sam sposób:
\begin{enumerate}
	\item Ustawianie bufora ramki.
	\item Czyszczenie bufora głębokości.
	\item Obliczanie macierzy view\dywiz{}projection (z przestrzeni świata do przestrzeni ekranu).
	\item Wypełnianie kolejki obiektami nieprzeźroczystymi znajdującymi się we frustum światła i~sortowanie jej.
	\item Przekazanie kolejki do renderowania.
	\item Przepięcie bufora ramki na bufor 0 (bufor ekranu).
\end{enumerate}
Po tym procesie stworzone zostają mapy głębokości, które pozwalają na obliczenie położeń cieni rzucanych przez obiekty które znalazły się bliżej źródła światła. Przykładowa mapa głębokości dla światła kierunkowego została przedstawiona poniżej.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\textwidth]{../figures/directionalShadow.png}
	\caption{Mapa głębokości dla światła kierunkowego (opracowanie własne)}
\end{figure}
	\subsection{Mapa irradiancji}
W drugiej części procedury renderowania generowana jest cubemapa środowiska i~irradiancji oraz mapy odbić dla powierzchni metalicznych. w~tym segmencie wykonuje się najwięcej obliczeń, jednak przeprowadzane one są tylko podczas działania pierwszej klatki aplikacji oraz w~pierwszej klatce po przełączeniu trybu oświetlenia. Scena jest całkowicie statyczna, więc nie ma potrzeby obliczania wszystkiego za każdym razem. Podobny zabieg został zastosowany we wspominanym wcześniej mapowaniu cieni. Ponowny render map jest konieczny jedynie przy zmianie oświetlenia, ponieważ zmienia się cała kolorystyka sceny.\par
Cubemapa środowiska rysowana jest przy użyciu prostego shadera. Tworzy on gradient między kolorami ziemi, zenitu oraz nieba na podstawie wartości współrzędnej 'y' tekstury. Dodatkowo, na warstwie nieba generowane są gwiazdy przy użyciu algorytmu białego szumu w~wersji dla shaderów przedstawionym w~\cite{shaderRand}. Wynikowe cubemapy środowiska zostały przedstawione na rysunku \ref{fig:skybox}. Implementacja algorytmu dla fragment shadera została przedstawiona na listingu \ref{lst:enviro}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/skybox.png}
		\caption{Dzień}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/skybox2_lighter.png}
		\caption{Noc (rozjaśniona o~30\%)}
	\end{subfigure}
	\caption{Mapy środowiska dla pór dnia (opracowanie własne)}
	\label{fig:skybox}
\end{figure}
\begin{lstlisting}[language={GLSL}, caption={Fragment shader generujący cubemapę środowiska}, label={lst:enviro}]
#version 430 core
in vec3 TexCoord;
out vec4 FragColor;

uniform vec4 groundColor;
uniform vec4 zenithColor;
uniform vec4 skyColor;
uniform vec4 starsColor;

float noise(vec3 value)
{
	vec3 smallValue = sin(value);
	float random = dot(smallValue, vec3(12.9898, 78.233, 37.719));
	random = fract(sin(random) * 143758.5453);
	return random;
}

void main()
{
	float middle = 0.1;
	float middleStep = 0.05;
	vec3 normCoord = normalize(TexCoord);
	float p = normCoord.y;
	vec4 color = mix(groundColor, zenithColor, smoothstep(0, middle, p));
	color = mix(color, zenithColor, smoothstep(middle, middleStep + middle, p));

	vec4 skyColour = noise(TexCoord) > 0.995 ? starsColor : skyColor;

	color = mix(color, skyColour, smoothstep(middleStep + middle, 1, p));
	color.a = 1.0;
	FragColor = pow(color, vec4(2.2));
}
\end{lstlisting}\par
Mapa środowiska jest później zapisywana do tekstury by można było ją użyć do generowania mapy irradiancji. Mapa irradiancji przedstawia przybliżony kolor i~jasność otoczenia. Wykorzystuje się ją do stworzenia wrażenia bardziej realistycznie oświetlonej sceny. Wpływ mapy irradiancji na oświetlenie sceny zostało przedstawione na rysunku \ref{fig:irradiance}. Dokładne wyjaśnienie działania i~implementacja algorytmu generującego mapę irradiancji został przedstawiony w~rozdziale ,,Mapa irradiancji diffuse'' \cite{learnOpenGL}.
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/without_irradiance.png}
		\caption{Bez irradiancji}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/with_irradiance.png}
		\caption{Z mapą irradiancji}
	\end{subfigure}
	\caption{Porównanie oświetlenia sceny ze względu na dołączanie mapy irradiancji (opracowanie własne)}
	\label{fig:irradiance}
\end{figure}\par
Zanim algorytm przejdzie do następnego kroku obliczana jest jeszcze cubemapa odbić. Żeby odbijany obraz zgadzał się z~otoczeniem, mapę odbić trzeba stworzyć na podstawie cubemapy otoczenia, generowanej najlepiej w~początkowej pozycji kamery, czyli współrzędnych \verb|(0, 16, 0)|. Nie można użyć mapy środowiska wykorzystanej przy irradiancji, ponieważ obiekty będą odbijać tylko niebo (i zaimprowizowany grunt), a~nie wnętrze pomieszczenia. Generowane jest więc sześć obrazów wnętrza, po jednym w~każdym kierunku, które zostają złożone w~cubemapę środowiska. Na jej podstawie obliczana jest mapa prefiltracji według~\cite{learnOpenGL} (rozdział ,,Specular IBL''). Odbicia na metalicznym obiekcie zostały przedstawione na rysunku \ref{fig:specular}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{../figures/specularibl.png}
	\caption{Użycie mapy specular na metalicznym obiekcie (opracowanie własne)}
	\label{fig:specular}
\end{figure}
	\subsection{SSAO}
Okluzja otoczenia w~przestrzeni ekranu\cite{SSAO} to ostatni krok przed renderowaniem rzeczywistej sceny. Został zaimplementowany bufor próbkowania, który przechowuje dane pozycji i~wektorów normalnych dla każdego fragmentu w~przestrzeni kamery. Pominięto dane koloru, ponieważ przy samym SSAO są one niepotrzebne, a~silnik nie posiłkuje się renderingiem odroczonym. Pojawiła się również potrzeba dopisania shadera obsługującego rendering instancjonowany. Tak jak w~przypadku obliczania cieni czy mapy odbić brak instancjonowanych obiektów sceny był wręcz niezauważalny, tak przy SSAO rzuca się to mocno w~oczy. Jedynym obiektem renderowanym przy użyciu instancjonowania jest szczypiorek w~doniczce. Można było obejść się bez tej optymalizacji, jednak wówczas kolejka renderingu powiększyłaby się o~ponad 1000 elementów, co mocno odbiłoby się na wydajności aplikacji. Na rysunkach \ref{fig:withoutSSAO} i~\ref{fig:withSSAO} zostało przedstawione porównanie wyglądu rośliny przed i~po zaimplementowaniu zapisywania instancjonowanych fragmentów do bufora próbkowania.\par
Dzięki SSAO roślina nabrała objętości i~przede wszystkim realizmu. Bez okluzji otoczenia ten szczypiorek wydaje się płaską poszarpaną zieloną plamą. Poza tym nałożone jest SSAO z~doniczki co jeszcze bardziej udziwnia wygląd rośliny.
Nie zagłębiając się w~szczegóły, pozostała część algorytmu obliczania okluzji otoczenia przestrzeni ekranu została zaimplementowana dokładnie tak jak w~\cite{learnOpenGL}. Wynik działania algorytmu został przedstawiony na rysunku \ref{fig:ssao}
\begin{figure}
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth]{../figures/flower_without_ssao.png}
		\caption{Bez SSAO}
		\label{fig:withoutSSAO}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.65\textwidth]{../figures/flower_with_ssao.png}
		\caption{Z SSAO}
		\label{fig:withSSAO}
	\end{subfigure}
	\caption{Porównanie wyglądu instancjonowanych obiektów ze względu na generowanie mapy okluzji otoczenia (opracowanie własne)}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{../figures/ssao.png}
	\caption{Tekstura przedstawiająca końcowy wynik obliczeń SSAO (opracowanie własne)}
	\label{fig:ssao}
\end{figure}\par
	\subsection{PBR}
PBR czyli model renderingu bazującego na fizyce\cite{PBR} to najważniejszy element sceny, ponieważ od niego głównie zależy realizm obiektów znajdujących się na niej. W aplikacji wykorzystano wspominane wcześniej mapy odbić, oświetlenia środowiskowego, cieni, ale też metaliczność, oraz -- opisana w następnym podrozdziale tej pracy -- przeźroczystość i załamanie światła. Większość wymienionych składowych oblicza się osobno i jedynie przekazuje się wyniki w postaci tekstur do głównego shadera. W nim obliczane jest jedynie oświetlenie oraz pozycja cieni, które są później łączone z przekazanymi wynikami wcześniejszych kalkulacji. Rozdzielenie składowych PBR na kolejne kroki pozwala uporządkować cały proces obliczeniowy i pozwala na zastosowanie optymalizacji, takich jak renderowanie map irradiancji i odbić w pierwszej klatce (które już zostało wspomniane wcześniej).\par 
Na scenie znajduje się pięć świateł reflektorowych i~jedno światło kierunkowe. Trzy reflektory są umieszczone pod górnymi szafkami, a~dwie -- na suficie. Każde ze świateł reflektorowych ma swój graficzny odpowiednik w~postaci żarówki, której kolor przedstawia stan światła (włączone/wyłączone). Implementacja shaderów obliczających oświetlenie i~cieniowanie bazujące na fizyce zostało zaczerpnięte z~,,Physically Based Rendering: From Theory To Implementation'' \cite{PBR}.\par 
Scena, już z~obliczonym światłem, cieniami i~odbiciami renderowana jest do bufora podobnego do tego używanego przy SSAO. Użyto takiego zabiegu, by móc później połączyć go z~efektem okluzji otoczenia. Łączenie odbywa się w~shaderze, którego kod umieszczono na listingu \ref{lst:windowShader}.
\begin{lstlisting}[language={GLSL}, caption={Shader łączący SSAO z~renderem sceny}, label={lst:windowShader}]
#version 430 core
out vec4 FragColor;
in vec2 oTexCoord;
uniform sampler2D sceneAmbient;
uniform sampler2D ssao;
void main()
{
	vec3 color = texture(sceneAmbient, oTexCoord).rgb;
	// HDR tonemapping
	color = color / (color + vec3(1.0));
	// gamma correct
	color = pow(color, vec3(1.0/2.2)); 

	color *= texture(ssao, oTexCoord).r;

	FragColor = vec4(color, 1.0);
}
\end{lstlisting}
	\subsection{Szkło}
Elementy szklane w~aplikacji można podzielić na szybę w~oknie oraz obiekty znajdujące się w~pomieszczeniu. Taki podział został zastosowany, ponieważ do obliczeń związanych z~szybą trzeba zastosować nieco inną metodę. Przede wszystkim, efekt refrakcji na takiej szybie praktycznie nie występuje przez cienkość materiału i to, że krawędzie tafli szkła nie są widoczne. Dodatkowo, żeby osiągnąć realistyczne odbicie w~szybie potrzebna jest mapa odbić wygenerowana z~pozycji szyby. Zastosowanie mapy środowiska wygenerowanej z~początkowej pozycji kamery nie jest w stanie wygenerować poprawnego obrazu odbić na powierzchni szyby. Stworzona z poprawnej pozycji mapa później jest przekształcana do projekcji prostopadłościennej (Box projection) i~na jej podstawie obliczane są odbicia. Przykładowy fragment odbicia w~szybie okiennej został zaprezentowany na rysunku \ref{fig:window}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{../figures/window.png}
	\caption{Przedstawienie odbić w~szybie dla nocnego oświetlenia (opracowanie własne)}
	\label{fig:window}
\end{figure}\par
Pozostałe półprzeźroczyste powierzchnie mają swój własny schemat obliczeń -- obliczane są współczynniki załamania oraz odbicia, które później są interpolowane. Współczynnikiem wagowym interpolacji została wybrana aproksymacja Schlicka, której wzór prezentuje się w~taki sposób \cite{schlick}:
\begin{eqnarray}
	R(\theta) = R_{0} + (1 - R_{0})(1 - \cos\theta)^5,\\
	gdzie\ R_{0} = \bigg(\frac{n_1 - n_2}{n_1 + n_2}\bigg)^2
\end{eqnarray}
gdzie ${n_1}$ i~${n_2}$ to współczynniki załamania dla -- odpowiednio -- powietrza i~szkła, a~${\theta}$ to kąt między odwróconym kierunkiem patrzenia a~wektorem normalnym powierzchni. Odbicie i~załamanie światła dla powierzchni szklanych na przykładzie miski z~owocami zostało przedstawione na rysunku \ref{fig:glassBowl}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{../figures/bowlie.png}
	\caption{Załamanie obrazu na powierzchni szklanej miski (opracowanie własne)}
	\label{fig:glassBowl}
\end{figure}
\section{Optymalizacja}
Sposoby optymalizacji aplikacji były już przytaczane w~tej pracy, jednak warto jest je uporządkować w~jednym miejscu. z~poziomu obliczeń w~procesorze można przytoczyć jedynie optymalizację grafu sceny przez tzw. dirty flag. Nie jest to dużo, jednak podczas tworzenia kodu programu problematyczne były obliczenia na karcie graficznej, więc na nich skupiła się chęć optymalizacji. Najważniejszym zabiegiem poprawiającym wydajność aplikacji jest instancjonowanie obiektów występujących w~dużych ilościach, a~mających tą samą siatkę i~materiał. Wykorzystane jest przy renderowaniu źdźbeł roślin doniczkowych. Dla realizmu generowana jest bardzo duża liczba łodyg (ponad 600), więc bez instancjonowania mogłyby być spore kłopoty w~osiągnięciu pożądanej liczby klatek na sekundę.\par
Drugim najważniejszym zabiegiem optymalizacyjnym jest sortowanie kolejki renderowania według identyfikatora materiału. Dzięki temu można pomijać ustawianie uniformów shadera i~od razu renderować siatki obiektów. Tak jak dla małych shaderów jest to niezauważalne, tak przy najbardziej istotnym (PBR) użycie tego zabiegu powoduje dwukrotny wzrost wydajności. Może to być spowodowane tym, że shader PBR przechowuje ponad 10 uniformów, z~czego większość to samplery tekstur.\par
Jeśli chodzi o~uniformy shaderów, to zostały również zaimplementowane bufory obiektów uniformów (z ang. Uniform Buffer Object). Są to bufory przechowujące zmienne globalne, do których dostęp może mieć każdy shader jeśli tylko zostanie to zdefiniowane. W~aplikacji używane są trzy bufory:
\begin{itemize}
	\item \verb|CameraUBO|, który przechowuje jej pozycję oraz macierze widoku i~projekcji,
	\item \verb|LightUBO|, zawierający dane związane ze światłami takie jak kolor, kierunek świecenia czy zasięg,
	\item \verb|ShadowUBO|, przechowujący macierze z~przestrzeni świata do przestrzeni ekranu dla każdego ze świateł; bufor używany głównie podczas obliczania map cieni, stąd taka nazwa.
\end{itemize}\par
Ponadto zdecydowano, żeby generować wszystkie niezmienne mapy, czyli środowiska, cieni, irradiancji i~odbić, w~każdej pierwszej klatce po zmianie trybu oświetlenia (w tym w~pierwszej klatce całej aplikacji). Scena jest niezmienna, wiec nie ma potrzeby wykonywania skomplikowanych obliczeń co klatkę. Wystarczy już, że co klatkę wykonuje się zasobożerny shader PBR, który łączy wymienione wyżej mapy z~danymi renderowanych obiektów. Inaczej mówiąc -- obliczenia niezwiązane bezpośrednio z~pozycją kamery są wykonywane tylko raz w~trakcie przełączania trybu oświetlenia sceny.\par
Innym sposobem optymalizacji programu jest frustum culling zaimplementowany dla kamery i~świateł. Frustum culling jest to technika pozwalająca na renderowanie tylko tych obiektów, które znajdują się w~polu widzenia (ang. field of view). Scena w~aplikacji jest na tyle mała, że usuwanie niewidocznych powierzchni dla kamery nie daje dużego wzrostu szybkości renderowania. Jest to spowodowane tym, że -- gdy punkt widzenia jest w~pomieszczeniu -- przez większość czasu widoczna jest praktycznie połowa obiektów składających się na scenę. \par
Inna sytuacja jest w~przypadku świateł. Pomijanie nieistotnych dla światła powierzchni jest widoczne najbardziej dla lampek pod szafkami. Zasięg ich świecenia pozwala na oświetlenie blatu i~elementów leżących na nim, zatem nie ma potrzeby renderowania do mapy głębokości obiektów które znajdują się dalej, bo i~tak się na niej nie znajdą. Dzięki temu na każdą mapę cieni dla świateł pod szafkami składa się mniej niż dziesięć renderowanych obiektów, zamiast ponad stu. Działanie frustum cullingu zostało przedstawione na rysunku \ref{fig:frustum}. Kolorem zielonym oznaczono obiekty, które całkowicie znajdują się w~polu widzenia, czyli pomiędzy płaszczyzną bliższą (z ang. near plane), a~dalszą (z ang, far plane). Dodatkowym parametrem frustum jest kąt widzenia, wyznaczający zasięg rozchodzenia się ,,promieni'' widzenia, które składają się na pole widzenia (ang. field of view). Kolorem żółtym przedstawiono obiekty częściowo będące we frustum, a~czerwonym -- obiekty pomijane. Warto zwrócić uwagę, że jeśli obiekt znajduje się między pozycją kamery a~płaszczyzną bliższą, również zostaje odrzucony przez frustum culling. Dlatego też dobrze dobierać jak najmniejszą płaszczyznę bliższą, odpowiednią do rozmiaru sceny.\par 
Algorytm wykrywania obiektów we frustum został zaimplementowany dla OBB (Oriented Bounding Box) według strony Lighthouse3d.com\cite{frustum}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{../figures/frustumculling.png}
	\caption{Schemat działania frustum cullingu dla kamery (opracowanie własne)}
	\label{fig:frustum}
\end{figure} 