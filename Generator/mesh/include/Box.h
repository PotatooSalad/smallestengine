#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include "../../../Renderer/include/MeshDataStructures.h"
#include "../../../Core/include/globalDefs.h"

class Box
{
public:
	Box(glm::vec3 center, glm::vec3 size, glm::vec3 texOffset = { 0, 0, 0 }, glm::vec3 texDiv = {0, 0, 0});

	void getBox(std::vector<Vertex>& vertices, std::vector<uint>& indices);
	/// <summary>
	/// Get box with hole
	/// </summary>
	/// <param name="holePosition">Left down front corner of hole</param>
	/// <param name="holeSize">size of hole</param>
	/// <param name="vertices"></param>
	/// <param name="indices"></param>
	void getHoledBox(glm::vec3 holePosition, glm::vec3 holeSize, std::vector<Vertex>& vertices, std::vector<uint>& indices);
private:
	glm::vec3 center;
	glm::vec3 size;
	glm::vec3 texOffset;
	glm::vec3 texDiv;
	glm::vec3 leftDownFrontCorner;

	static uint boxIndices[36];
};