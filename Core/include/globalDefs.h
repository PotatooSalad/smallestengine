#pragma once

#define LOGMSG(msg) printf("%s line:%d: %s\n", __FILE__, __LINE__, msg)
#define PI 3.14159265359

using byte = unsigned char;
using uint = unsigned int;

class Core;

Core& GetCore();