#include "../include/Checker.h"

Checker::Checker(glm::ivec2 texSize, int numberOfTiles, glm::vec3 darkerColor, glm::vec3 lighterColor, ProceduralEngineSetup setup) : Procedural(texSize, darkerColor, setup)
{
	this->numberOfTiles = numberOfTiles;
	this->lightColor = lighterColor;
	this->tileSize = texSize.x / numberOfTiles;
	countTexture();
	//saveData();
}

void Checker::countTexture()
{
	bool dark = true;
	for (int y = 0; y < size.y; ++y)
	{
		int yFactor = y / tileSize;
		for (int x = 0; x < size.x; ++x)
		{
			int xFactor = x / tileSize;
			dark = xFactor % 2 == 0;
			if (yFactor % 2 == 1) dark = !dark;
			texData[y * size.x * 3 + x * 3 + 0] = floatToByte( dark ? baseColor.r : lightColor.r );
			texData[y * size.x * 3 + x * 3 + 1] = floatToByte( dark ? baseColor.g : lightColor.g );
			texData[y * size.x * 3 + x * 3 + 2] = floatToByte( dark ? baseColor.b : lightColor.b );
		}
	}
}