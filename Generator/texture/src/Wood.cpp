#include "../include/Wood.h"
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include "../../../Renderer/include/Texture.h"
#include "../../../Core/include/Core.h"
//#include <string>
//#include "stb_image_write.h"

float Wood::shift = 0;

Wood::Wood(glm::ivec2 size, glm::vec3 baseColor, glm::vec3 additionalColor1, glm::vec3 additionalColor2, ProceduralEngineSetup setup) : Procedural(size, baseColor, setup)
{
	this->additColor1 = additionalColor1;
	this->additColor2 = additionalColor2;


	normalTex = new byte[size.x * size.y * 3 * normMultX * normMultY];

	countTexture();
	countNormal();
	//saveData();
	//saveNormal();
	shift += 100;
}

//void Wood::saveNormal()
//{
//	std::string name = "woodNormal";
//	name += std::to_string(id);
//	name += ".tga";
//	stbi_write_tga(name.c_str(), size.x * normMultX, size.y * normMultY, 3, normalTex);
//}

using namespace glm;

void Wood::countTexture()
{
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			vec2 fragCoord = vec2(x, y) + vec2(shift);
			vec2 uv = fragCoord / (vec2)size;
			float ratio = size.x / (float)size.y;
			uv.y *= ratio;

			vec3 value = baseColor;

			value = mix(value, additColor1, vec3(perlinNoise2D(vec2(uv.x) * 10.0f)));
			value = mix(value, additColor2, noiseArr[(int)(uv.x * size.y + uv.x)] * 0.1f);
			value -= noise(uv * vec2(500.0f, 20.0f)) * 0.1f;

			vec3 base = value;
			texData[y * size.x * 3 + (x * 3)] = floatToByte(base.x);
			texData[y * size.x * 3 + (x * 3) + 1] = floatToByte(base.y);
			texData[y * size.x * 3 + (x * 3) + 2] = floatToByte(base.z);

		}
	}
}

void Wood::countNormal()
{
	bool isLine = false;
	for (int x = 0; x < (size.x * normMultX); ++x)
	{
		isLine = randomValue() > 0.5f;
		for (int y = 0; y < (size.y * normMultY); ++y)
		{
			float colorValue = !isLine;
			colorValue += (float)(64.0f * turbulence(x, y, 5.0f) / 256.0f);
			glm::vec3 val(0, 0, colorValue);
			normalTex[y * size.x * 3 * normMultX + x * 3 + 0] = (byte)glm::clamp((int)(127.5 * (val.x + 1.0)), 0, 255);
			normalTex[y * size.x * 3 * normMultX + x * 3 + 1] = (byte)glm::clamp((int)(127.5 * (val.y + 1.0)), 0, 255);
			normalTex[y * size.x * 3 * normMultX + x * 3 + 2] = (byte)glm::clamp((int)(127.5 * (val.z + 1.0)), 0, 255);
		}
	}

}

Texture* Wood::getNormalTex(TextureCreateInfo texSetup)
{
	texSetup.width = size.x * normMultX;
	texSetup.height = size.y * normMultY;
	return GetCore().objectModule.newTexture(normalTex, texSetup);
}



float Wood::noise(vec2 uv) {
	return smoothNoise(uv.x, uv.y);
}



