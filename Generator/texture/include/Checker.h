#pragma once
#include "Procedural.h"

class Checker : public Procedural
{
public:
	Checker(glm::ivec2 texSize, int numberOfTiles, glm::vec3 darkerColor, glm::vec3 lighterColor, ProceduralEngineSetup setup);
private:
	glm::vec3 lightColor;
	int numberOfTiles;
	int tileSize;

	void countTexture();
};