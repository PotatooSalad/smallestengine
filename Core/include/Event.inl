#pragma once

enum class Event : unsigned int
{
	UNKNOWN = 0,
	ADD_MESH,				// Data: Mesh*
	ADD_LIGHT,				// Data: Light*
	SET_CAMERA,				// Data: Camera*
	WINDOW_RESIZED,			// Data: glm::ivec2(width, height)
	KEY_PRESSED,			// Data: key pressed
	KEY_REPEAT,				// Data: key repeated
	KEY_RELEASED,			// Data: key released
	MOUSE_CURSOR_MOVED,		// Data: cursor data

};