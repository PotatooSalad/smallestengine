#pragma once

#include "../../Base/include/Component.inl"
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

class Material;
class Mesh;

struct MeshRenderer : public Component
{
	MeshRenderer() = default;
	virtual ~MeshRenderer() = default;

	bool shouldRender = true;
	bool instanced = false;

	std::vector<glm::vec4> intstancedPositions;

	glm::mat4 modelMatrix = glm::mat4(1);
	Material* material = nullptr;
	Mesh* mesh = nullptr;
};