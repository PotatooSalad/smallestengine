#version 430 core
#define MAX_SPOTLIGHTS 1

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
//layout (location = 5) in vec4 instanceModel; // Assigns up to index 5
layout (std140, binding = 2) uniform ShadowMapping
{
    mat4 directionalLightSpaceMatrix;
    mat4 spotLightSpaceMatrix0;
    //mat4 spotLightSpaceMatrix1;
};

uniform mat4 MVP;
uniform mat4 model;

out vec3 oNormal;
out vec2 oTexCoords;
out vec3 FragPos;
out vec4 oFragPosDirLightSpace;
out vec4 oFragPosSpotLightSpace[MAX_SPOTLIGHTS];

void main() 
{
    oNormal = mat3(transpose(inverse(model))) * normal;
    //FragPos = vec3(model * (instanceModel + vec4(position, 0.0)));
    FragPos = vec3(model * vec4(position, 1.0));
    oTexCoords = texCoords;
    //gl_Position = MVP * (instanceModel + vec4(position, 0.0));
    oFragPosDirLightSpace = directionalLightSpaceMatrix * vec4(FragPos, 1.0);
    oFragPosSpotLightSpace[0] = spotLightSpaceMatrix0 * vec4(FragPos, 1.0);
    //oFragPosSpotLightSpace[1] = spotLightSpaceMatrix1 * vec4(FragPos, 1.0);
    gl_Position = MVP * vec4(position, 1.0);
}