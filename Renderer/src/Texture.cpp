#include "../include/Texture.h"
#include <string.h>

Texture::Texture(byte* data, TextureCreateInfo createInfo)
{
    if (data != nullptr)
    {
        this->data = new byte[createInfo.width * createInfo.height * 3];
        memcpy(this->data, data, createInfo.width * createInfo.height * 3);
    }
    else
    {
        this->data = data;
    }
    this->texInfo = createInfo;
    init();
}

Texture::Texture(float* data, TextureCreateInfo createInfo)
{
    if (data != nullptr)
    {
        this->floatData = new float[createInfo.width * createInfo.height * 3];
        memcpy(this->floatData, data, createInfo.width * createInfo.height * 3);
    }
    else
    {
        this->floatData = data;
    }
    this->texInfo = createInfo;
    init();
}

void Texture::init()
{
    glGenTextures(1, &ID);
    if (texInfo.cubemap)
    {
        glBindTexture(GL_TEXTURE_CUBE_MAP, ID);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, texInfo.magFilter);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, texInfo.minFilter);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, texInfo.wrapMode);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, texInfo.wrapMode);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, texInfo.wrapMode);

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, NULL);

        if (texInfo.generateMipmaps)
        {
            glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
        }

        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, ID);
        if (texInfo.type == GL_UNSIGNED_BYTE)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, data);
        }
        else if(texInfo.type == GL_FLOAT)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, texInfo.internalFormat, texInfo.width, texInfo.height, 0, texInfo.format, texInfo.type, floatData);
        }

        if (texInfo.generateMipmaps)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, texInfo.wrapMode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, texInfo.wrapMode);
        if (texInfo.wrapMode == GL_CLAMP_TO_BORDER)
        {
            float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
            glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, texInfo.minFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, texInfo.magFilter);

    }
    
}
void Texture::bind(int textureUnit) const
{
    glActiveTexture(GL_TEXTURE0 + textureUnit);
    if (texInfo.cubemap)
    {
        glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, ID);
    }
}