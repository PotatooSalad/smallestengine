#pragma once
#include <glm/vec3.hpp>

namespace LightRange
{
	/*const glm::vec3 _7 = glm::vec3(1.0f, 0.7f, 1.8f);
	const glm::vec3 _13 = glm::vec3(1.0f, 0.35f, 0.44f);
	const glm::vec3 _20 = glm::vec3(1.0f, 0.22f, 0.2f);
	const glm::vec3 _32 = glm::vec3(1.0f, 0.14f, 0.07f);*/
	const glm::vec3 _50 = glm::vec3(1.0f, 0.09f, 0.032f);
	const glm::vec3 _65 = glm::vec3(1.0f, 0.07f, 0.017f);
	const glm::vec3 _100 = glm::vec3(1.0f, 0.045f, 0.0075f);
	/*const glm::vec3 _160 = glm::vec3(1.0f, 0.027f, 0.0028f);
	const glm::vec3 _200 = glm::vec3(1.0f, 0.022f, 0.0019f);
	const glm::vec3 _325 = glm::vec3(1.0f, 0.014f, 0.0007f);
	const glm::vec3 _600 = glm::vec3(1.0f, 0.007f, 0.0002f);
	const glm::vec3 _3250 = glm::vec3(1.0f, 0.0014f, 0.000007f);*/
}