#pragma once
#include "Module.inl"
#include "../../Renderer/include/Texture.h"
#include "../../Renderer/include/MeshDataStructures.h"
#include "../../Renderer/include/Material.h"
#include <vector>

class Entity;
class Component;
class Mesh;
class Shader;

/// <summary>
/// controlling all objects in simulation
/// </summary>
class ObjectModule : public Module
{
public:
	ObjectModule();
	~ObjectModule();


	std::vector<Entity*>* getEntitiesVector();
	Entity* getEntityPtryByName(const char* name);
	Entity* newEntity(int bufferSize = 0, const char* name = "");

	template<typename T>
	T* newEmptyComponentForLastEntity();

	Shader* newShader(const char* fragmentShaderData, const char* vertexShaderData, const char* geometryShaderData = nullptr);
	Texture* newTexture(byte* data, TextureCreateInfo info);
	Texture* newTexture(float* data, TextureCreateInfo info);

	Material* getMaterialPtrByName(const char* name);
	Material* newMaterial(Shader* shader, std::string name, RenderType type = RenderType::Opaque, bool instancingEnabled = false);
	
	Mesh* newMesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, bool backFaceCulling = false);
	Mesh* getLastMesh() { return meshes[meshes.size() - 1]; }

	void receiveMessage(Message msg);

private:
	std::vector<Entity*> entities;
	std::vector<Component*> components;
	std::vector<Mesh*> meshes;
	std::vector<Material*> materials;
	std::vector<Shader*> shaders;
	std::vector<Texture*> textures;

	static uint entityID;
};

#include "ObjectModule.ipp"

