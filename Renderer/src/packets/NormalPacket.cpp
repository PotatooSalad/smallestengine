#include "../../include/packets/NormalPacket.h"

#include "../../include/Material.h"
#include "../../include/RendererModule.h"
#include "../../include/Mesh.h"
#include "../../include/Shader.h"
#include "../../include/Texture.h"
#include <string>

void NormalPacket::render(glm::mat4& VP, RenderMode mode, Texture* directionalShadowMap, Texture* spotShadowMaps[], Texture* irradianceMap, Texture* prefilteredMap, Texture* brdf)
{
    bool shadowMapping = (mode == RenderMode::SHADOW_MAPPING);
    
    if (RendererModule::firstRender)
    {
        if (mode == RenderMode::CUBEMAP_MAPPING && material->hasCubemapShader())
        {
            material->setShadowMaps(directionalShadowMap, spotShadowMaps);
        }
        if (mode == RenderMode::NORMAL && material->hasCubemapShader())
        {
            material->setTexture("brdfLUT", brdf);
            material->setTexture("irradianceMap", irradianceMap);
            material->setTexture("prefilterMap", prefilteredMap);
            //material->setShadowMaps(directionalShadowMap, spotShadowMaps);
        }
    }
    material->use(mode);
    material->setTransformMatrices(modelMatrix, VP, shadowMapping);

    if (mesh->hasBackFaceCulling())
    {
        glDisable(GL_CULL_FACE);
        mesh->render();
        glEnable(GL_CULL_FACE);
    }
    else
    {
        mesh->render();
    }
}

void NormalPacket::renderTransparent(glm::mat4& VP, Texture* prefilteredMap)
{
    if (RendererModule::firstRender)
    {
        material->setTexture("enviromentalMap", prefilteredMap);
    }
    material->use(RenderMode::NORMAL);
    material->setTransformMatrices(modelMatrix, VP, false);
    if (mesh->hasBackFaceCulling())
    {
        glDisable(GL_CULL_FACE);
        mesh->render();
        glEnable(GL_CULL_FACE);
    }
    else
    {
        mesh->render();
    }
}

void NormalPacket::renderWithShader(Shader* shader, glm::mat4& VP)
{
    if (material->isMaterialGlass())
    {
        return;
    }
    shader->use();
    glm::mat4 MVP = VP * modelMatrix;
    shader->setMat4("model", modelMatrix);
    shader->setMat4("MVP", MVP);
    if (mesh->hasBackFaceCulling())
    {
        glDisable(GL_CULL_FACE);
        mesh->render();
        glEnable(GL_CULL_FACE);
    }
    else
    {
        mesh->render();
    }
}