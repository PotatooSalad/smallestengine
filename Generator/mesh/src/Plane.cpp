#include "../include/Plane.h"

void Plane::getPlanePoints(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	glm::vec3 leftDown = planeCenter - glm::vec3(segments.x / 2.0f * segmentSize.x, 0.0f, segments.y / 2.0f * segmentSize.y);
	Vertex v;
	int ySize = segments.y + 1;
	for (int x = 0; x <= segments.x; ++x)
	{
		for (int z = 0; z <= segments.y; ++z)
		{
			v.position.x = leftDown.x + x * segmentSize.x;
			v.position.y = leftDown.y;
			v.position.z = leftDown.z + z * segmentSize.y;

			v.normal = normals;
			
			v.texcoord.x = x / segments.x;
			v.texcoord.y = z / segments.y;
			vertices.push_back(v);

			if (x < segments.x && z < segments.y )
			{
				indices.push_back(x * ySize + z);
				indices.push_back(x * ySize + z + 1);
				indices.push_back((x + 1) * ySize + z + 1);
				indices.push_back(x * ySize + z);
				indices.push_back((x + 1) * ySize + z + 1);
				indices.push_back((x + 1) * ySize + z);
			}
		}
	}
}