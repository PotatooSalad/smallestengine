#include "../include/Entity.h"

#include "../include/Component.inl"

Entity::Entity(int id, int bufferSize, const char* name)
{
	this->id = id;
	this->components.reserve(bufferSize);
	this->name = name;
}

void Entity::addComponent(Component* ptr)
{
	components.push_back(ptr);
	ptr->entityPtr = this;
}