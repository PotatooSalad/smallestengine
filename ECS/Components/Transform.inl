#pragma once

#include "../Base/include/Component.inl"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <unordered_set>

struct Transform : public Component
{
    friend class SceneGraph;
public:
	Transform() = default;
	virtual ~Transform() = default;

    /// <summary>
    /// Get modifiable reference to LocalPosition.
    /// Automatically set dirty flag, so shouldn't be used to read value
    /// </summary>
    /// <returns> modifiable local position of node</returns>
    inline glm::vec3& getLocalPositionModifiable() { dirty = true; return localPosition; }

    /// <summary>
    /// Get read-only reference to Local Rotation.
    /// </summary>
    /// <returns> local rotation of node</returns>
    inline const glm::quat& getLocalRotation() { return localRotation; }

    /// <summary>
    /// Get modifiable reference to Local Rotation.
    /// Automatically set dirty flag, so shouldn't be used to read value;
    /// </summary>
    /// <returns>modifiable local rotation of node </returns>
    inline glm::quat& getLocalRotationModifiable() { dirty = true; return localRotation; }

    /// <summary>
    /// Transformation from model space to world space.
    /// </summary>
    /// <returns>parentMatrix * localMatrix</returns>
    const inline glm::mat4& getModelMatrix() const { return modelMatrix; }

    /// <summary>
    /// Transformation from parent space to world space.
    /// </summary>
    /// <returns>parent->modelMatrix</returns>
    const inline glm::mat4& getParentMatrix() const { return parent->modelMatrix; }

    /// <summary>
    /// Transformation from world space to model space.
    /// </summary>
    /// <returns>modelMatrix^(-1)</returns>
    const inline glm::mat4& getToModelMatrix() const { return toModelMatrix; }

    const inline glm::vec3 getWorldPosition() const { return glm::vec3(modelMatrix[3]); }

	/// <summary>
	/// set transform parent, two-way connection
	/// </summary>
	/// <param name="newParent">pointer to new parent of transform</param>
	void setParent(Transform* newParent)
	{
		if (parent != nullptr)
		{
			parent->children.erase(this);
		}
		newParent->children.insert(this);
		parent = newParent;
		dirty = true;
	}

	/// <summary>
	/// get pointer to parent
	/// </summary>
	/// <returns> parent of this node</returns>
	inline Transform* getParent() { return parent; }

	/// <summary>
	/// dirty flag
	/// </summary>
	bool dirty = true;

	/// <summary>
	/// child nodes
	/// </summary>
	std::unordered_set<Transform*> children;

private:

	Transform* parent = nullptr;

	glm::vec3 localPosition = glm::vec3(0.0f);
	glm::quat localRotation = { 1.0f, 0.0f, 0.0f, 0.0f };

	glm::mat4 modelMatrix = glm::mat4(1);
	glm::mat4 toModelMatrix = glm::mat4(1);
};