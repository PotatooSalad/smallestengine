#pragma once
#include "../../../Core/include/globalDefs.h"
#include <glm/glm.hpp>
#include "../include/Procedural.h"

class NormalMap : public Procedural
{

public:
	NormalMap(int width, int height, ProceduralEngineSetup setup) : Procedural({ width, height }, { 0,0,0 }, setup) { countTexture(); }

private:
	void countTexture();
};