#pragma once

#include <vector>

class Module;
class Message;

class MessageBus
{
public:
	MessageBus(int bufferSize = 0);

	void addReceiver(Module* modulePtr);

	void sendMessage(Message msg);

	void notify();
private:
	std::vector<Message> buffer0;
	std::vector<Message> buffer1;

	std::vector<Module*> receivers;

	std::vector<Message>* activeBuffer = &buffer0;
	std::vector<Message>* inactiveBuffer = &buffer1;
};