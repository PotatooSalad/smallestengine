#pragma once
#include "Procedural.h"

class Wallpaper : public Procedural
{
public:
	Wallpaper(glm::ivec2 size, glm::vec3 color, ProceduralEngineSetup setup) : Procedural(size, color, setup) { countTexture(); }

	Wallpaper(glm::ivec2 size, glm::vec3 color, glm::vec3 secondColor, ProceduralEngineSetup setup) 
		: Procedural(size, color, setup), secondColor(secondColor) { countDoubleColoredTexture(); }
private:
	glm::vec3 secondColor;
	void countTexture();
	void countDoubleColoredTexture();
};