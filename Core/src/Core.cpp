#include "../include/Core.h"
#include "../include/Message.inl"
#include "../../Renderer/include/Shader.h"
#include "../../Renderer/include/Mesh.h"
#include "../../ECS/Base/include/Entity.h"
#include "../../ECS/Components/Render/MeshRenderer.h"
#include "../../ECS/Components/Render/Camera.h"
#include "../../Renderer/include/Material.h"
#include "../../Renderer/include/Texture.h"
#include "../../ECS/Components/Transform.inl"
#include "../../Generator/mesh/include/Box.h"
#include "../../Generator/texture/include/Wallpaper.h"
#include "../../ECS/Components/Render/Light.h"
#include "../include/SceneMaker.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#include "stb_image_write.h"

Core* Core::instance = nullptr;
int Core::windowWidth = Core::INIT_WINDOW_WIDTH;
int Core::windowHeight = Core::INIT_WINDOW_HEIGHT;

int Core::init()
{
    if (instance != nullptr)
    {
        LOGMSG("Core already exists!");
        return -1;
    }
    instance = this;

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // glfw window creation
    // --------------------
    window = glfwCreateWindow(INIT_WINDOW_WIDTH, INIT_WINDOW_HEIGHT, "Inzynierka 2020", NULL, NULL);
    if (window == NULL)
    {
        LOGMSG("Failed to create GLFW window");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        LOGMSG("Failed to initialize GLAD");
        return -1;
    }

    glViewport(0, 0, windowWidth, windowHeight);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    glfwSetKeyCallback(window, keyboardCallback);
    glfwSetCursorPosCallback(window, cursorCallback);

#pragma region Renderer
    // ! ----- Renderer initialization block -----
    RendererModuleCreateInfo rendererCreateInfo = {};
    rendererCreateInfo.clearColor = glm::vec3(0.0f, 0.0f, 0.0f);
    rendererCreateInfo.clearFlags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
    rendererCreateInfo.cullFace = true;
    rendererCreateInfo.cullFaceMode = GL_BACK;
    rendererCreateInfo.cullFrontFace = GL_CCW;
    rendererCreateInfo.depthTest = true;
    rendererCreateInfo.wireframeMode = false;
    rendererCreateInfo.gammaCorrection = false;
    rendererModule.initialize(window, rendererCreateInfo);
#pragma endregion

#pragma region messageBus
    messageBus.addReceiver(&rendererModule);
    messageBus.addReceiver(&systemsModule);
    messageBus.addReceiver(&objectModule);
    messageBus.addReceiver(&cameraSystem);
#pragma endregion

#pragma region systemsModule
    systemsModule.entities = objectModule.getEntitiesVector();
    systemsModule.addSystem(&cameraSystem);
    systemsModule.addSystem(&meshRendererSystem);
    systemsModule.addSystem(&lightSystem);
#pragma endregion

    return initScene();
}


int Core::initScene()
{
    SceneMaker sm;
    objectModule.newEntity(2, "camera");
    {
        Camera* c = objectModule.newEmptyComponentForLastEntity<Camera>();
        ViewFrustum& frustum = c->getFrustumModifiable();
        frustum.farPlane = 2500.0f;
        frustum.nearPlane = 0.1f;
        frustum.fov = 90.0f;
        frustum.aspectRatio = (float)windowWidth / (float)windowHeight;
        c->getProjectionModeModifiable() = CameraProjection::PERSPECTIVE;
        auto camTrans = objectModule.newEmptyComponentForLastEntity<Transform>();
        camTrans->setParent(&sceneGraph.rootNode);
        camTrans->getLocalPositionModifiable() = { 1, 16, 0 };
    }
    
    sm.initScene(&objectModule);

    return 0;
}

int Core::mainLoop()
{
    cameraSystem.setAsMain(objectModule.getEntityPtryByName("camera"));
    previousFrameStart = glfwGetTime();
    double lag = 0;
    sceneGraph.updateTransforms();
    messageBus.notify();
    systemsModule.run(UpdateType::START);
    while (!glfwWindowShouldClose(window))
    {
        sceneGraph.updateTransforms();
        currentFrameStart = glfwGetTime();
        double lastFrameTime = currentFrameStart - previousFrameStart;
        lag += lastFrameTime;

        previousFrameStart = currentFrameStart;

        glfwPollEvents();

        while (lag >= FIXED_TIME_STEP)
        {
            messageBus.notify();
            sceneGraph.updateTransforms();
            systemsModule.run(UpdateType::FIXED);
            lag -= FIXED_TIME_STEP;
        }

        sceneGraph.updateTransforms();
        systemsModule.run(UpdateType::FRAME);
        messageBus.notify();
        rendererModule.render();

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    return 0;
}

void Core::cleanup()
{

}

void Core::close()
{
    glfwTerminate();
}

void Core::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    windowWidth = width;
    windowHeight = height;
    GetCore().getMessageBus().sendMessage(Message(Event::WINDOW_RESIZED, glm::ivec2(width, height)));
    GetCore().getMessageBus().notify();
}

void Core::keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // ! ----- Quick fix for function buttons crashing the application -----
    if (action == GLFW_KEY_UNKNOWN || key == GLFW_KEY_UNKNOWN)
    {
        return;
    }

    Event event = Event::UNKNOWN;
    switch (action)
    {
    case GLFW_PRESS:
        event = Event::KEY_PRESSED;
        break;
    case GLFW_REPEAT:
        event = Event::KEY_REPEAT;
        break;
    case GLFW_RELEASE:
        event = Event::KEY_RELEASED;
        break;
    }
    if (event != Event::UNKNOWN)
    {
        GetCore().getMessageBus().sendMessage(Message(event, key));
    }
}

void Core::cursorCallback(GLFWwindow* window, double xPos, double yPos)
{
    static double xOld = 0;
    static double yOld = 0;

    auto data = CursorData
    {
        //conversion to float limits structure size
        static_cast<float>(xPos),
        static_cast<float>(yPos),
        static_cast<float>(xPos - xOld),
        static_cast<float>(yPos - yOld)
    };

    GetCore().getMessageBus().sendMessage(Message(Event::MOUSE_CURSOR_MOVED, data));

    xOld = xPos;
    yOld = yPos;
}