#include "../include/LightSystem.h"
#include "../../Components/Transform.inl"
#include "../../Components/Render/Light.h"
#include "../../Components/Render/MeshRenderer.h"
#include "../../Base/include/Entity.h"
#include "../../../Core/include/Message.inl"
#include "../../../Core/include/Core.h"

bool LightSystem::assertEntity(Entity* entity)
{
	transformPtr = entity->getComponentPtr<Transform>();
	lightPtr = entity->getComponentPtr<Light>();
	return (transformPtr != nullptr) && (lightPtr != nullptr);
}
void LightSystem::start()
{
	lightPtr->positionInWorld = transformPtr->getWorldPosition();
	lightPtr->direction = glm::vec3(glm::toMat4(transformPtr->getLocalRotation()) * glm::vec4(0, -1, 0, 1));
	lightPtr->lightViewMatrix = glm::lookAt(transformPtr->getWorldPosition(), transformPtr->getWorldPosition() + lightPtr->direction, glm::vec3(0, 1, 0));
	calcFrustum();
	GetCore().getMessageBus().sendMessage(Message(Event::ADD_LIGHT, lightPtr));
}

void LightSystem::frameUpdate()
{
	if (lightPtr->lightType == LightType::Spot)
	{
		auto mr = lightPtr->entityPtr->getComponentPtr<MeshRenderer>();
		if (mr != nullptr)
		{
			if (lightPtr->color.a < 0.1f)
			{
				mr->material->setVec4("glassColor", {1, 1, 1, 0.2});
			}
			else
			{
				mr->material->setVec4("glassColor", { 1, 1, 1, 2.0 });
			}
		}
	}
}

void LightSystem::calcFrustum() 
{
	ViewFrustum& frustum = lightPtr->lightFrustum;
	switch (lightPtr->lightType)
	{
	case LightType::Directional:
		lightPtr->lightProjectionMatrix = glm::ortho(
			-frustum.fov * frustum.aspectRatio,
			frustum.fov * frustum.aspectRatio,
			-frustum.fov,
			frustum.fov,
			frustum.nearPlane,
			frustum.farPlane
		);
		break;

	case LightType::Spot:
		lightPtr->lightProjectionMatrix = glm::perspective(
			glm::radians(frustum.fov),
			frustum.aspectRatio,
			frustum.nearPlane,
			frustum.farPlane
		);
		break;
	}

	float tang = glm::tan(glm::radians(frustum.fov / 2.0f));
	frustum.Hnear = 2.0f * tang * frustum.nearPlane;
	frustum.Wnear = frustum.Hnear * frustum.aspectRatio;

	frustum.Hfar = 2.0f * tang * frustum.farPlane;
	frustum.Wfar = frustum.Hfar * frustum.aspectRatio;

	frustum.position = transformPtr->getWorldPosition();
	frustum.front = lightPtr->direction;
	frustum.right = glm::normalize(glm::cross({ 0, 1, 0 }, frustum.front));
	frustum.up = glm::cross(lightPtr->direction, frustum.right);
}
