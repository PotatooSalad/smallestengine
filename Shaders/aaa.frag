#version 430 core

layout (std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

const float AIR = 1.0;
const float GLASS = 1.51714;

const float ETA = AIR / GLASS;
// http://en.wikipedia.org/wiki/Refractive_index ->Reflectivity
const float R0 = ((AIR - GLASS) * (AIR - GLASS)) / ((AIR + GLASS) * (AIR + GLASS));

in vec4 FragPos;
in vec3 oNormal;

uniform samplerCube enviromentalMap;
uniform vec3 centerPosition;

out vec4 FragColor;

vec3 boxProjection(vec3 direction, vec3 position, vec3 cubemapPosition) 
{
    vec3 boxMin = vec3(-24, 0, -30);
    vec3 boxMax = vec3(24, 30, 30);

    vec3 factors = vec3(0.0);
    factors.x = direction.x > 0 ? boxMax.x : boxMin.x;
    factors.y = direction.y > 0 ? boxMax.y : boxMin.y;
    factors.z = direction.z > 0 ? boxMax.z : boxMin.z;
    factors -= position;
    factors /= direction;
    float scalar = min(min(factors.x, factors.y), factors.z);
    return direction * scalar + (position - cubemapPosition);
}

void main()
{
    vec3 viewDir = normalize(vec3(FragPos - viewPos));

    vec3 refraction = refract(viewDir, oNormal, ETA);
    vec3 reflection = reflect(viewDir, oNormal);

	reflection = boxProjection(reflection, FragPos.xyz, centerPosition);

    //http://en.wikipedia.org/wiki/Schlick%27s_approximation
    float fresnel = R0 + (1.0 - R0) * pow((1.0 - dot(-viewDir, oNormal)), 5.0);

    vec4 refractionColor = texture(enviromentalMap, normalize(refraction));
    vec4 reflectionColor = texture(enviromentalMap, normalize(reflection));

    FragColor = vec4(1, 1, 1, 0.5) * reflectionColor;//mix(refractionColor, reflectionColor, fresnel);
}
)shader";