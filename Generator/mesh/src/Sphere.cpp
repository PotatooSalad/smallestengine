#include "../include/Sphere.h"

void Sphere::getSphere(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	Vertex vert;
	float xy;
	float sectorStep = 2 * PI / sectors;
	float stackStep = PI / stacks;
	float lengthInv = 1.0f / radius;
	float sectorAngle, stackAngle;
	
	//stack indices
	int lower = 0;
	int higher = 0;

	for (int i = 0; i <= stacks; ++i)
	{
		stackAngle = PI / 2 - i * stackStep;
		xy = radius * cosf(stackAngle);
		vert.position.z = radius * sinf(stackAngle);

		if (i < stacks)
		{
			lower = i * (sectors + 1);
			higher = lower + sectors + 1;
		}

		for (int j = 0; j <= sectors; ++j, ++lower, ++higher)
		{
			sectorAngle = j * sectorStep;

			vert.position.x = xy * cosf(sectorAngle);
			vert.position.y = xy * sinf(sectorAngle);

			vert.normal.x = vert.position.x * lengthInv;
			vert.normal.y = vert.position.y * lengthInv; 
			vert.normal.z = vert.position.z * lengthInv;

			vert.texcoord.x = (float)j / sectors;
			vert.texcoord.y = (float)i / stacks;

			vertices.push_back(vert);
			if (i < stacks && j < sectors)
			{
				if (i != 0)
				{
					indices.push_back(lower);
					indices.push_back(higher);
					indices.push_back(lower + 1);
				}

				if (i != stacks - 1)
				{
					indices.push_back(lower + 1);
					indices.push_back(higher);
					indices.push_back(higher + 1);
				}
			}
		}
	}
}