#pragma once
#include <glm/glm.hpp>
#include "../../Renderer/include/Texture.h"
#include "../../Generator/texture/include/Procedural.h"
#include "../../Renderer/include/MeshDataStructures.h"

class ObjectModule;
class Material;
class Mesh;
class Entity;
class Shader;

enum class KnobType { LeftDoor, RightDoor, Drawer, DownCenter, UpCenter };


class SceneMaker
{
public:
	SceneMaker() = default;
	void initScene(ObjectModule* objectModule);
	glm::quat eulerToQuaternion(glm::vec3 angle);
private:
	void room();
	void doubleDooredShelf(glm::vec3 pos, Entity* parent = nullptr, glm::vec3 rot = {0, 0, 0});
	Entity* singleDooredShelf(glm::vec3 pos, Mesh* shelfMesh, glm::vec3 meshSize, Mesh* doorMesh, glm::vec3 doorSize, KnobType knobType, int knobMode = 1, Entity* parent = nullptr, glm::vec3 rot = { 0,0,0 });


	void wall(glm::vec3 pos, glm::vec3 size, Material* wallMaterial, const char* name,  glm::vec3 holePos = { 0,0,0 }, glm::vec3 holeSize = {0,0,0});
	Entity* woodenBox(glm::vec3 pos, Mesh* mesh, const char* name, Entity* parent = nullptr, bool dark = false, glm::vec3 rot = { 0,0,0 });
	void makeWoodMaterials(int amount, Material** mats, const char* name, glm::ivec2 texSize, glm::mat3 color);
	void workTop(glm::vec3 pos, glm::vec3 size, const char* name, glm::mat3 color, Material* mat = nullptr, glm::vec3 rot = { 0,0,0 }, glm::vec3 holePos = { 0, 0, 0 }, glm::vec3 holeSize = { 0, 0, 0 });
	void knob(Entity* shelfEnt, KnobType type, glm::vec3 shelfSize, const char* name, bool big = false);
	void cylinder(glm::vec3 pos, glm::vec3 size, Entity* parent, Material* mat, const char* name, glm::vec3 rot = {0, 0, 0});
	void stool(glm::vec3 pos, glm::vec2 sitSize, float height, const char* name);
	void sink(glm::vec3 pos, glm::vec3 size, Entity* parent);
	void tap(glm::vec3 pos, Entity* parent);
	void sphereTextured(glm::vec3 pos, float radius, Material* mat, const char* name, glm::vec3 rot = {0, 90, 0});
	void grass(glm::vec3 pos, Entity* parent, int grassSize, float radius);

	Mesh* boxMesh(glm::vec3 center, glm::vec3 size, glm::vec3 holePos = { 0,0,0 }, glm::vec3 holeSize = {0,0,0});
	Mesh* sphereMesh(float radius, int sectors, int stacks);

	Material** darkWoodMaterials;
	Material** lightWoodMaterials;
	Material* knobMaterial;
	Material* workTop3Mat;
	Material* lampMat;

	ObjectModule* objectModule = nullptr;

	Shader* standardPBR;
	Shader* standardPBRInstanced;
	Shader* cubemapPBR;
	Shader* cubemapPBRInstanced;
	Shader* shadowShader;
	Shader* glassShader;

	Mesh** knobSmallMeshes = nullptr;
	Mesh** knobBigMeshes = nullptr;

	Mesh* doorMesh;
	Mesh* shelfHalfMesh;

	TextureCreateInfo texInfo;
	ProceduralEngineSetup setup;
	std::vector<Vertex> vertices;
	std::vector<uint> indices;

	static int materialIndex;
	static int shelfIndex;

	const int MAX_WOOD = 3;

	const glm::mat3 woodColors = { { 0.670, 0.517, 0.309 },{0.435, 0.305, 0.184},{0.286, 0.188, 0.094} };
	const glm::mat3 darkWoodColors = { {0.482, 0.396, 0.278 },{0.760, 0.596, 0.439},{0.447, 0.298, 0.152} };
	const glm::mat3 marbleColors = { {0.850, 0.815, 0.588}, {0.866, 0.858, 0.811}, {0,0,0} };
	const glm::ivec2 floorSize = glm::ivec2(48, 60);
	const int wallHeight = 30;
	const glm::vec3 lc = { floorSize.x / 2, 0, floorSize.y / 2 };
	const glm::vec3 unitShelf = { 12, 8, 12 };
	const glm::vec3 doorSize = { 5.8, 6, 0.5 };
	const glm::vec3 shelfDoubleSize = { 12, 8, 1 };
	const float spaceSize = 0.1f;
	const float bigSpaceSize = 0.2f;
};
