#pragma once

#include <glad/glad.h>

#include "../../Core/include/globalDefs.h"

struct TextureCreateInfo
{
	bool generateMipmaps = true;
	int width = 1;
	int height = 1;
	uint format = 1;
	uint internalFormat = 1;
	uint wrapMode = 1;
	uint minFilter = 1;
	uint magFilter = 1;
	uint type = GL_UNSIGNED_BYTE;
	bool cubemap = false;
};

class Texture
{
public:
	Texture(byte* data, TextureCreateInfo createInfo);
	Texture(float* data, TextureCreateInfo createInfo);
	~Texture() = default;

	void bind(int textureUnit = 0) const;

	const uint getID() { return ID; }
private:

	void init();

	uint ID;
	TextureCreateInfo texInfo;
	byte* data;
	float* floatData;
};

