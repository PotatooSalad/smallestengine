#include "../include/Mesh.h"
#include "../include/MeshDataStructures.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

//#include <fstream>
//#include <string>

uint Mesh::idCounter = 1;

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<uint> indices, bool backFaceCulling)
{
    glm::vec3 min = { INFINITY, INFINITY, INFINITY };
    glm::vec3 max = { -INFINITY, -INFINITY, -INFINITY };
    for (auto &v : vertices)
    {
        this->vertices.push_back(v);
        min.x = v.position.x < min.x ? v.position.x : min.x;
        min.y = v.position.y < min.y ? v.position.y : min.y;
        min.z = v.position.z < min.z ? v.position.z : min.z;

        max.x = v.position.x > max.x ? v.position.x : max.x;
        max.y = v.position.y > max.y ? v.position.y : max.y;
        max.z = v.position.z > max.z ? v.position.z : max.z;
    }
    meshID = idCounter++;

    bounds.minBound = min;
    bounds.maxBound = max;

    meshCenter = (max + min) / 2.0f;
    this->backFaceCulling = backFaceCulling;

    for (auto i : indices)
    {
        this->indices.push_back(i);
    }
    //saveMeshToObj();
	setup();
}

Mesh::~Mesh()
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ebo);
}

void Mesh::setup()
{
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &instanceVbo);
    glGenBuffers(1, &ebo);

    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint), indices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texcoord));

    // ===== Set instance attrib pointers =====
    glBindBuffer(GL_ARRAY_BUFFER, instanceVbo);
    std::size_t vec4Size = sizeof(glm::vec4);
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);

    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(1 * vec4Size));

    glEnableVertexAttribArray(7);
    glVertexAttribPointer(7, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));

    glEnableVertexAttribArray(8);
    glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);
    glVertexAttribDivisor(7, 1);
    glVertexAttribDivisor(8, 1);

    // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
    // VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
    glBindVertexArray(0);
}

void Mesh::render()
{
    glBindVertexArray(vao);
    if (vertices.size() == 1)
    {
        glDrawArrays(GL_POINTS, 0, 1);
    }
    else if (indices.size() == 0)
    {
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    }
    else
    {
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
}

void Mesh::renderInstanced(int count, glm::mat4* instanceMatrices)
{
    glBindBuffer(GL_ARRAY_BUFFER, instanceVbo);
    glBufferData(GL_ARRAY_BUFFER, count * sizeof(glm::mat4), instanceMatrices, GL_STATIC_DRAW);
    glBindVertexArray(vao);
    glDrawElementsInstanced(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, count);
    glBindVertexArray(0);
}

//void Mesh::saveMeshToObj()
//{
//    std::ofstream file("mesh " + std::to_string(idCounter) + ".obj");
//    if (file.good())
//    {
//        for (int i = 0; i < vertices.size(); ++i)
//        {
//            file << "v " << std::to_string(vertices[i].position.x) << " " << std::to_string(vertices[i].position.y) << " " << std::to_string(vertices[i].position.z) << std::endl;
//            file << "vt " << std::to_string(vertices[i].texcoord.x) << " " << std::to_string(vertices[i].texcoord.y) << std::endl;
//            file << "vn " << std::to_string(vertices[i].normal.x) << " " << std::to_string(vertices[i].normal.y) << " " << std::to_string(vertices[i].normal.z) << std::endl;
//        }
//        for (int i = 0; i < indices.size(); i += 3)
//        {
//            file << "f " << std::to_string(indices[i] + 1) << " " << std::to_string(indices[i + 1] + 1) << " " << std::to_string(indices[i + 2] + 1) << std::endl;
//        }
//    }
//    file.close();
//}
