#include "../include/Material.h"
#include "../include/Shader.h"
#include "../include/Texture.h"
#include "../include/RendererModule.h"

#include <glad/glad.h>

uint Material::idCount = 1;

Material::Material(Shader* shader, bool enableInstancing, std::string name, RenderType type)
{
	ID = idCount;
	this->name = name;
	this->sceneShader = shader;
	this->type = type;
	this->enableInstancing = enableInstancing;
	idCount++;

	printf("%d: %s\n", ID, name.c_str());

	std::unordered_map<std::string, uint>& uniforms = shader->getUniforms();
	for (auto uni : uniforms)
	{
		switch (uni.second)
		{
		case GL_SAMPLER_2D:
		case GL_SAMPLER_CUBE:
			textures.insert(std::pair<std::string, Texture*>(uni.first, nullptr));
			break;
		case GL_FLOAT:
			floats[uni.first] = 0.0f;
			break;
		case GL_FLOAT_VEC2:
			vec2s[uni.first] = glm::vec2(0);
			break;
		case GL_FLOAT_VEC3:
			vec3s[uni.first] = glm::vec3(0);
			break;
		case GL_FLOAT_VEC4:
			vec4s[uni.first] = glm::vec4(0);
			break;
		case GL_FLOAT_MAT4:
			mat4s[uni.first] = glm::mat4(1);
			break;
		default:
			LOGMSG("Material encountered an unsupported uniform type");
		}
	}

	bool instancingSupported = false;
	std::unordered_map<std::string, GLenum>& attributes = shader->getAttributes();

	for (auto var : attributes)
	{
		if (var.first == "instanceModel")
		{
			instancingSupported = true;
			break;
		}
	}

	if (!instancingSupported && this->enableInstancing)
	{
		LOGMSG("Instancing for material is not supported! Falling back to normal rendering.");
	}
}

void Material::use(RenderMode mode)
{
	//LOGMSG(name.c_str());
	Shader* shader = sceneShader;
	//printf("SETUP MATERIAL\n");
	if (mode == RenderMode::SHADOW_MAPPING && shadowMappingShader != nullptr)
	{
		shadowMappingShader->use();
		//printf("SHADOW MAPPING\n");
		return;
	}
	else if (mode == RenderMode::CUBEMAP_MAPPING && cubemapShader != nullptr)
	{
		shader = cubemapShader;
		//printf("CUBEMAP\n");
	}

	if (shader != nullptr)
	{
		bool materialChanged = true; // TODO: Optimization
		
		shader->use();
		if (materialChanged || texturesChanged)
		{
			int i = 0;
			// ===== textures =====
			for (auto texture : textures)
			{
				if (texture.second != nullptr)
				{
					//printf("%d : %s\n", i, texture.first.c_str());
					texture.second->bind(i);
					shader->setInt(texture.first, i);
					++i;
				}
			}
			texturesChanged = false;
		}

		if (materialChanged || floatsChanged)
		{
			// ===== floats =====
			for (auto var : floats)
			{
				shader->setFloat(var.first, var.second);
			}
		}

		if (materialChanged || vec2sChanged)
		{
			// ===== Vec2s =====
			for (auto var : vec2s)
			{
				shader->setVec2(var.first, var.second);
			}
			vec2sChanged = false;
		}

		if (materialChanged || vec3sChanged)
		{
			// ===== Vec3s =====
			for (auto var : vec3s)
			{
				shader->setVec3(var.first, var.second);
			}
			vec3sChanged = false;
		}

		if (materialChanged || vec4sChanged)
		{
			// ===== Vec4s =====
			for (auto var : vec4s)
			{
				shader->setVec4(var.first, var.second);
			}
			vec4sChanged = false;
		}

		if (materialChanged || mat4sChanged)
		{
			// ===== Mat4s =====
			for (auto var : mat4s)
			{
				shader->setMat4(var.first, var.second);
			}
			mat4sChanged = false;
		}
	}
	else
	{
		LOGMSG("Material has no shader!\n");
	}
}

void Material::setShadowMappingShader(Shader* shader)
{
	shadowMappingShader = shader;
}

void Material::setCubemapShader(Shader* shader)
{
	cubemapShader = shader;
}

void Material::setMVP(const glm::mat4& M, const glm::mat4& VP, std::string name)
{
	glm::mat4 model = VP * M;
	sceneShader->setMat4(name, model);
}
void Material::setVP(const glm::mat4& VP, std::string name)
{
	sceneShader->setMat4(name, VP);
}

void Material::setTransformMatrices(const glm::mat4& M, const glm::mat4& VP, bool shadowMapping)
{
	glm::mat4 MVP = VP * M;
	if (shadowMappingShader != nullptr && shadowMapping)
	{
		shadowMappingShader->setMat4("model", M);
		shadowMappingShader->setMat4("lightSpaceMatrix", VP);
		return;
	}
	sceneShader->setMat4("model", M);
	sceneShader->setMat4("MVP", MVP);
}

void Material::setShadowMaps(Texture* directionalShadowMap, Texture* spotShadowMaps[])
{
	if (shadowMappingShader != nullptr)
	{
		setTexture("shadowMaps0", directionalShadowMap);
		for (int i = 1; i <= RendererModule::MAX_SPOTLIGHTS; ++i)
		{
			std::string texName = "shadowMaps" + std::to_string(i);
			setTexture(texName, spotShadowMaps[i - 1]);
		}
		return;
	}
}

void Material::setTexture(std::string name, Texture* value)
{
	std::map<std::string, Texture*>::iterator texturesIter = textures.find(name);
	if (texturesIter != textures.end())
	{
		//printf("Material: %s, texture: %s\n", this->name.c_str(), name.c_str());
		texturesIter->second = value;
		texturesChanged = true;
	}
	else
	{
		std::string msg = "Invalid texture name " + name;
		LOGMSG(msg.c_str());
	}
}

void Material::setFloat(std::string name, float value)
{
	std::unordered_map<std::string, float>::iterator floatsIter = floats.find(name);
	if (floatsIter != floats.end())
	{
		floatsIter->second = value;
		floatsChanged = true;
	}
	else
	{
		std::string msg = "Invalid float uniform name " + name;
		LOGMSG(msg.c_str());
	}
}

void Material::setVec2(std::string name, glm::vec2 value)
{
	std::unordered_map<std::string, glm::vec2>::iterator vec2sIter = vec2s.find(name);
	if (vec2sIter != vec2s.end())
	{
		vec2sIter->second = value;
		vec2sChanged = true;
	}
	else
	{
		std::string msg = "Invalid vec2 uniform name " + name;
		LOGMSG(msg.c_str());
	}
}

void Material::setVec3(std::string name, glm::vec3 value)
{
	std::unordered_map<std::string, glm::vec3>::iterator vec3sIter = vec3s.find(name);
	if (vec3sIter != vec3s.end())
	{
		vec3sIter->second = value;
		vec3sChanged = true;
	}
	else
	{
		std::string msg = "Invalid vec3 uniform name " + name;
		LOGMSG(msg.c_str());
	}
}

void Material::setVec4(std::string name, glm::vec4 value)
{
	std::unordered_map<std::string, glm::vec4>::iterator vec4sIter = vec4s.find(name);
	if (vec4sIter != vec4s.end())
	{
		vec4sIter->second = value;
		vec4sChanged = true;
	}
	else
	{
		std::string msg = "Invalid vec4 uniform name " + name;
		LOGMSG(msg.c_str());
	}
}

void Material::setMat4(std::string name, glm::mat4 value)
{
	std::unordered_map<std::string, glm::mat4>::iterator mat4sIter = mat4s.find(name);
	if (mat4sIter != mat4s.end())
	{
		mat4sIter->second = value;
		mat4sChanged = true;
	}
	else
	{
		std::string msg = "Invalid mat4 uniform name " + name;
		LOGMSG(msg.c_str());
	}
}

const Texture* Material::getTexturePtr(std::string name)
{
	auto iter = textures.find(name);
	if (iter != textures.end())
	{
		return iter->second;
	}
	else
	{
		std::string msg = "Invalid texture uniform name " + name;
		LOGMSG(msg.c_str());
		return nullptr;
	}
}