#include "../include/SceneGraph.h"
#include "../include/Message.inl"

void SceneGraph::receiveMessage(Message msg) {}

SceneGraph::SceneGraph()
{
	rootNode.setParent(&preRootNode);
}

void SceneGraph::updateTransforms()
{
	process(rootNode, false);
}

void SceneGraph::process(Transform& transform, bool dirty)
{
    dirty |= transform.dirty;

    glm::mat4 local(1);

    if (dirty)
    {
        local = glm::translate(local, transform.localPosition);
        local = local * glm::toMat4(transform.localRotation);

        transform.modelMatrix = transform.getParentMatrix() * local;
        //TODO may be computed in more optimal way
        transform.toModelMatrix = glm::inverse(transform.modelMatrix);
        transform.dirty = false;
    }


    for (Transform* t : transform.children)
    {
        process(*t, dirty);
    }
}