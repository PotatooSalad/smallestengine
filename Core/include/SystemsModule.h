#pragma once

#include "Module.inl"
#include "../../ECS/Base/include/System.h"
#include <vector>

/// <summary>
/// ECS systems module
/// </summary>
class SystemsModule : public Module
{
public:
	SystemsModule() = default;

	virtual void receiveMessage(Message msg);

	/// <summary>
	/// Assign system to module
	/// </summary>
	/// <param name="system">pointer to system to assign</param>
	void addSystem(System* system);

	/// <summary>
	/// run all systems
	/// </summary>
	/// <param name="updateType">Start/fixed/frame</param>
	void run(UpdateType updateType);

	std::vector<Entity*>* entities = nullptr;
private:
	std::vector<System*> systems;
};
