#pragma once

#include "CameraStructures.h"
#include "../../Base/include/Component.inl"

#include <glm/mat4x4.hpp>

struct Camera : public Component
{
	Camera() = default;
	virtual ~Camera() = default;

	const ViewFrustum& getFrustum() { return frustum; }
	const CameraProjection& getProjectionMode() { return projectionMode; }

	ViewFrustum& getFrustumModifiable() 
	{ 
		projectionChanged = true; 
		return frustum; 
	}

	CameraProjection& getProjectionModeModifiable()
	{
		projectionChanged = true;
		return projectionMode;
	}

	bool projectionChanged = true;
	bool isMain = false;
	CameraControl control = CameraControl::FREE;

	glm::mat4 viewMatrix = glm::mat4(1);
	glm::mat4 projectionMatrix = glm::mat4(1);

private:
	CameraProjection projectionMode = CameraProjection::PERSPECTIVE;
	ViewFrustum frustum = ViewFrustum();
};