#pragma once

#include <glm/glm.hpp>

/// <summary>
/// Base vertex data structure
/// </summary>
struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texcoord;

	Vertex(glm::vec3 pos) 
	{ 
		position = pos; 
		normal = glm::vec3(0);
		texcoord = glm::vec2(0);
	}

	Vertex(float x, float y, float z)
	{
		position = glm::vec3(x, y, z);
		normal = glm::vec3(0);
		texcoord = glm::vec2(0);
	}

	Vertex()
	{
		position = glm::vec3(0);
		normal = glm::vec3(0);
		texcoord = glm::vec2(0);
	}
};

struct Bounds
{
    glm::vec3 getPoint(int i)
    {
        switch (i)
        {
        case 0:
            return minBound; // -1, -1, -1
        case 1:
            return { minBound.x, minBound.y, maxBound.z }; // -1, -1, 1
        case 2:
            return { minBound.x, maxBound.y, minBound.z }; // -1, 1, -1
        case 3:
            return { maxBound.x, minBound.y, minBound.z }; // 1, -1, -1
        case 4:
            return maxBound; // 1, 1, 1
        case 5:
            return { maxBound.x, maxBound.y, minBound.z }; // 1, 1, -1
        case 6:
            return { maxBound.x, minBound.y, maxBound.z }; // 1, -1, 1
        case 7:
            return { minBound.x, maxBound.y, maxBound.z }; // -1, 1, 1
        default:
            return { 0.0f, 0.0f, 0.0f };
        }
    }

    glm::vec3 minBound;
    glm::vec3 maxBound;
};