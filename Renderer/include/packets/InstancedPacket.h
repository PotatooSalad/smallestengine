#pragma once

#include "../RendererDataStructures.h"
#include <vector>

struct InstancedPacket : RenderPacket
{
    static constexpr uint INSTANCE_ALLOCATION = 32;

    InstancedPacket(Mesh* mesh, Material* material);
    InstancedPacket() = default;
    virtual ~InstancedPacket() = default;

    void render(glm::mat4& VP, RenderMode mode, Texture* directionalShadowMap, Texture* spotShadowMaps[], Texture* irradianceMap, Texture* prefilteredMap, Texture* brdf);
    void renderWithShader(Shader* shader, glm::mat4& VP);

    std::vector<glm::mat4> instanceMatricesUnculled;
    std::vector<glm::mat4*> instanceMatrices;
    std::vector<bool> instanceOccluded;
};