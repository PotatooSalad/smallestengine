#pragma once
#include "../../Base/include/System.h"

struct Transform;
struct Light;

class LightSystem : public System 
{
public:
	LightSystem() = default;
	virtual ~LightSystem() = default;
	bool assertEntity(Entity* entity);
	void start();
	void frameUpdate();
private:
	void calcFrustum();
	Transform* transformPtr;
	Light* lightPtr;
};