#pragma once

#include "MessageBus.h"
#include "SceneGraph.h"
#include "ObjectModule.h"
#include "SystemsModule.h"
#include "../../Renderer/include/RendererModule.h"

#include "../../ECS/Systems/include/CameraSystem.h"
#include "../../ECS/Systems/include/MeshRendererSystem.h"
#include "../../ECS/Systems/include/LightSystem.h"

struct CursorData
{
    // horizontal coordinate of cursor
    float xPos;
    // horizontal coordinate of cursor
    float yPos;
    // change of horizontal coordinate of cursor
    float xDelta;
    // change of horizontal coordinate of cursor
    float yDelta;
};

struct GLFWwindow;

class Core
{
	friend Core& GetCore();
public:

#pragma region Constants
    // defines initial window width.
    static constexpr int INIT_WINDOW_WIDTH = 1280;

    // defines window hight.
    static constexpr int INIT_WINDOW_HEIGHT = 800;

    // frame-independent time between updates in seconds.
    static constexpr double FIXED_TIME_STEP = 1.0 / 60.0;

    // frame-independent time between updates in seconds in lower precision.
    static constexpr float FIXED_TIME_STEP_F = (float)FIXED_TIME_STEP;
#pragma endregion

#pragma region Functions

    Core() = default;
    ~Core() = default;

    int init();
    int mainLoop();
    void cleanup();
    void close();

    MessageBus& getMessageBus() { return messageBus; }
    GLFWwindow* getWindowPtr() { return window; }
    double getCurrentFrameStart() { return currentFrameStart; }
    

#pragma endregion

#pragma region Modules
    MessageBus messageBus = (1024);

    RendererModule rendererModule;
    SceneGraph sceneGraph;
    ObjectModule objectModule;
    SystemsModule systemsModule;
#pragma endregion

#pragma region Systems
    CameraSystem cameraSystem;
    MeshRendererSystem meshRendererSystem;
    LightSystem lightSystem;
#pragma endregion

    static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
    static void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void cursorCallback(GLFWwindow* window, double xPos, double yPos);
    static int windowWidth;
    static int windowHeight;

private:
	static Core* instance;
    GLFWwindow* window;
    double currentFrameStart;
    double previousFrameStart;

    std::vector<Vertex> vertices;
    std::vector<uint> indices;

    int initScene();

};

