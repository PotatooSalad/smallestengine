#pragma once

class Message;

class Module
{
public:
	virtual void receiveMessage(Message msg) = 0;
};