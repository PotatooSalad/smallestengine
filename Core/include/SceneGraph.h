#pragma once

#include "Module.inl"
#include "../../ECS/Components/Transform.inl"

class SceneGraph : public Module
{
public:
	SceneGraph();
	virtual ~SceneGraph() = default;

	virtual void receiveMessage(Message msg);

	void updateTransforms();

	void process(Transform& transform, bool dirty);

	Transform rootNode;
private:
	Transform preRootNode;
};

