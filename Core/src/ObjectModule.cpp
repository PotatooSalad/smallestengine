#include "../include/ObjectModule.h"
#include "../include/Message.inl"
#include "../../Renderer/include/Shader.h"
#include "../../Renderer/include/Mesh.h"

uint ObjectModule::entityID = 0;

ObjectModule::ObjectModule()
{
	entities.reserve(40);
	components.reserve(40);
	meshes.reserve(20);
	materials.reserve(30);
	shaders.reserve(15);
	textures.reserve(40);
}

ObjectModule::~ObjectModule()
{
	entities.clear();
	for (Component* c : components)
	{
		delete c;
	}
	components.clear();

	for (Mesh* m : meshes)
	{
		delete m;
	}
	meshes.clear();

	for (Material* m : materials)
	{
		delete m;
	}
	materials.clear();

	for (Shader* s : shaders)
	{
		delete s;
	}
	shaders.clear();

	for (Texture* t : textures)
	{
		delete t;
	}
	textures.clear();
}

std::vector<Entity*>* ObjectModule::getEntitiesVector()
{
	return &entities;
}

Entity* ObjectModule::getEntityPtryByName(const char* name)
{
	for (Entity* e : entities)
	{
		if (strcmp(e->getName(), "") != 0 && strcmp(e->getName(), name) == 0)
		{
			return e;
		}
	}
	return nullptr;
}

Entity* ObjectModule::newEntity(int bufferSize, const char* name)
{
	entities.push_back(new Entity(entityID++, bufferSize, name));
	return entities[entities.size() - 1];
}

Shader* ObjectModule::newShader(const char* fragmentShaderData, const char* vertexShaderData, const char* geometryShaderData)
{
	shaders.push_back(new Shader(fragmentShaderData, vertexShaderData, geometryShaderData));
	return shaders[shaders.size() - 1];
}

Texture* ObjectModule::newTexture(byte* data, TextureCreateInfo info)
{
	textures.push_back(new Texture(data, info));
	return textures[textures.size() - 1];
}

Texture* ObjectModule::newTexture(float* data, TextureCreateInfo info)
{
	textures.push_back(new Texture(data, info));
	return textures[textures.size() - 1];
}

Material* ObjectModule::getMaterialPtrByName(const char* name)
{
	for (Material* m : materials)
	{
		if (strcmp(m->getName(), "") != 0 && strcmp(m->getName(), name) == 0)
		{
			return m;
		}
	}
	return nullptr;
}

Material* ObjectModule::newMaterial(Shader* shader, std::string name, RenderType type, bool instancingEnabled)
{
	materials.push_back(new Material(shader, instancingEnabled, name, type));
	return materials[materials.size() - 1];
}

Mesh* ObjectModule::newMesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, bool backFaceCulling)
{
	meshes.push_back(new Mesh(vertices, indices, backFaceCulling));
	return meshes[meshes.size() - 1];
}

void ObjectModule::receiveMessage(Message msg)
{

}