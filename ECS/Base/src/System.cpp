#include "../include/System.h"
#include "../include/Entity.h"

void System::process(Entity* entity, UpdateType updateType)
{
    if (assertEntity(entity))
    {
        switch (updateType)
        {
        case UpdateType::START:
            return start();

        case UpdateType::FIXED:
            return fixedUpdate();

        case UpdateType::FRAME:
            return frameUpdate();
        }
    }
}