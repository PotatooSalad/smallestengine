#pragma once

#include <vector>

class Component;

/// <summary>
/// Entity class - container of components
/// </summary>
class Entity
{
public:
	/// <summary>
	/// Entity constructor
	/// </summary>
	/// <param name="id">id of entity</param>
	/// <param name="bufferSize">size of buffer (how many components it may contain</param>
	Entity(int id, int bufferSize = 0, const char* name = "");
	virtual ~Entity() = default;

	/// <summary>
	/// Add component to entity (and make connection between them)
	/// </summary>
	/// <param name="ptr">pointer to component</param>
	void addComponent(Component* ptr);

	/// <summary>
	/// Get name of entity
	/// </summary>
	/// <returns>name of entity (CONST!)</returns>
	const char* getName() const { return name; }

	/// <summary>
	/// Get the n-th component of given type (default is 0)
	/// </summary>
	/// <typeparam name="T">type of component</typeparam>
	/// <param name="n">occurrence of component</param>
	/// <returns>pointer to component (null pointer if wasn't found)</returns>
	template<class T>
	T* getComponentPtr(int n = 0) const;

private:
	int id;
	const char* name;
	std::vector<Component*> components;

};
#include "Entity.ipp"

