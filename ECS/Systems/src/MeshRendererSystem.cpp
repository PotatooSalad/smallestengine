#include "../include/MeshRendererSystem.h"
#include "../../Components/Transform.inl"
#include "../../Components/Render/MeshRenderer.h"
#include "../../Base/include/Entity.h"
#include "../../../Core/include/Core.h"
#include "../../../Core/include/Message.inl"

bool MeshRendererSystem::assertEntity(Entity* entity)
{
    meshRenderer = entity->getComponentPtr<MeshRenderer>();
    transform = entity->getComponentPtr<Transform>();
    return (meshRenderer != nullptr && transform != nullptr);
}

void MeshRendererSystem::start()
{
    meshRenderer->modelMatrix = transform->getModelMatrix();
    GetCore().messageBus.sendMessage(Message(Event::ADD_MESH, meshRenderer));
}