#pragma once
#include "../../Core/include/Module.inl"
#include "RendererDataStructures.h"
#include "MeshDataStructures.h"
#include "packets/NormalPacket.h"
#include "packets/InstancedPacket.h"
#include "../../ECS/Components/Render/CameraStructures.h"
#include <vector>
#include <deque>
#include <unordered_map>

struct GLFWwindow;
struct MeshRenderer;
struct Light;
struct Camera;
class Texture;

class RendererModule : public Module
{
public:
	RendererModule() = default;
	virtual ~RendererModule() = default;

	virtual void receiveMessage(Message msg);

	/// <summary>
	/// Initializes base renderer with values from RendererModuleCreate info and enables base instanced shapes
	/// </summary>
	/// <param name="window"> GLFW Window pointer</param>
	/// <param name="createInfo"> structure with render parameters </param>
	void initialize(GLFWwindow* window, RendererModuleCreateInfo createInfo);

	/// <summary>
	/// render current render queue
	/// </summary>
	void render();

	/// <summary>
	/// clean all allocated buffers
	/// </summary>
	void clean();

	Camera* mainCamera = nullptr;
	Texture* prefilterMap;

	static const uint MAX_SPOTLIGHTS = 5;
	static const uint SHADOW_WIDTH = 1024;
	static const uint SHADOW_HEIGHT = 1024;
	static constexpr glm::vec4 DIRECTIONAL_COLOR = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	static constexpr glm::vec4 SPOTLIGHT_COLOR = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	static bool firstRender;
	static bool day;
private:
	static const uint CUBEMAP_SIZE = 512;
	static const uint IRRADIANCE_SIZE = 64;
	static const uint PREFILTER_SIZE = 128;

	GLFWwindow* window = nullptr;
	RendererModuleCreateInfo createInfo;

	std::vector<NormalPacket> normalPackets;
	std::unordered_map<uint, InstancedPacket> instancedPackets; // ? +++++ size_t = mesh id << 32 | material id +++++

	inline uint key(unsigned int first, unsigned int second) { return first << 16 | second; }

	std::deque<RenderPacket*> opaqueQueue;
	std::deque<NormalPacket*> transparentQueue;

	std::vector<Light*> spotLights;
	std::vector<glm::mat4> lightsVP;

	Light* directionalLight;

	//UBOs
	uint cameraBuffer;
	uint lightBuffer;
	uint shadowBuffer;

	//FBOs
	uint directionalDepthMapFBO;
	uint spotDepthMapFBO[MAX_SPOTLIGHTS];
	uint cubeMapFBO;
	uint captureFBO;
	uint ssaoFBO;
	uint ssaoBlurFBO;
	uint gbufferFBO;
	uint sceneFBO;

	//RBOs;
	uint cubeMapRBO;
	uint captureRBO;
	uint gbufferRBO;
	uint sceneRBO;

	//Maps
	Texture* directionalDepthMap;
	Texture* enviromentalMap;
	Texture* skyboxMap;
	Texture* irradianceMap;
	Texture* windowReflectionMap;
	Texture* BRDFTexture;
	Texture* ssaoColorBuffer;
	Texture* ssaoBlurBuffer;
	Texture* gPosition;
	Texture* gNormal;
	Texture* scene;
	std::vector<Texture*> spotDepthMaps;

	glm::vec3 windowPosition;

	void calculateFrustumPlanes(const ViewFrustum frustum);
	void calculateFrustumPoints(const ViewFrustum frustum);
	bool objectInFrustum(Bounds& meshBounds, glm::mat4& modelMatrix);
	float pointToPlaneDistance(glm::vec3& pointOnPlane, glm::vec3& planeNormal, glm::vec3 point);

	void frustumCulling(const ViewFrustum frustum, RenderMode mode);
	void allPackets();

	enum { TOP = 0, BOTTOM, LEFT, RIGHT, NEARP, FARP };
	enum { NORMAL = 0, POINT };
	glm::vec3 frustumPlanes[6][2];
	glm::vec3 frustumPoints[8];
	bool frustumCullingEnabled = true;

	void renderSceneGraph(glm::mat4 VP, RenderMode mode);
	void renderTransparent(glm::mat4 VP);
	void shadowMapping();

	void renderToCubemap();
	void renderSkybox(Shader* shader, glm::mat4 VP);
	void renderQuad();
	uint skyboxVAO, skyboxVBO;
	uint quadVAO, quadVBO;
	Shader* skyboxShader;
	Shader* skyboxCountShader;
	Shader* irradianceShader;
	Shader* prefilterShader;
	Shader* brdfShader;
	Shader* gbufferShader;
	Shader* gbufferShaderInstanced;
	Shader* ssaoShader;
	Shader* ssaoBlurShader;
	Shader* combineShader;

	std::vector<glm::vec3> ssaoKernel;
	Texture* ssaoNoise;
	void ssao(glm::mat4 VP);
};

