#version 430 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

uniform mat4 MVP;
uniform mat4 model;

out vec4 FragPos;
out vec3 oNormal;

void main()
{
    FragPos = model * vec4(position, 1.0);
    oNormal = normalize(mat3(transpose(inverse(model))) * normal);

    gl_Position = MVP * vec4(position, 1.0);
}