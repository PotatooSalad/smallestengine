#version 430 core
out vec4 FragColor;

struct DirLight { // 4 * vec4
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 color;
};

struct SpotLight { // 5* vec4 + 2 * float
    float cutOff;
    float outerCutOff;
    vec3 position;
    vec3 direction;
    vec4 ambient;
    vec4 color;
    vec3 lightRange; //x - constant, y- linear, z- quadratic
};
#define MAX_SPOTLIGHTS 1

in vec3 FragPos;
in vec4 oFragPosDirLightSpace;
in vec4 oFragPosSpotLightSpace[MAX_SPOTLIGHTS];
in vec3 oNormal;
in vec2 oTexCoords;

layout (std140, binding = 0) uniform Camera
{
    mat4 projection;
    mat4 view;
    vec3 viewPos;
};

layout (std140, binding = 1) uniform Light
{
    DirLight dirLight;
    SpotLight spotLights0;
    //SpotLight spotLights1;
};

uniform sampler2D texAlbedo;
uniform sampler2D shadowMaps0;
uniform sampler2D shadowMaps1;
//uniform sampler2D shadowMaps2;

vec3 texSpecular;
float texShininess;

// function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec4 fragPosLightSpace, sampler2D shadowMap);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec4 fragPosLightSpace, sampler2D shadowMap);
float ShadowCalculation(vec4 fragPosLightSpace, vec3 lightPos, sampler2D shadowMap);

void main()
{    
    texSpecular = vec3(0.3, 0.3, 0.3);
    texShininess = 128.0;
    // properties
    vec3 norm = normalize(oNormal);
    vec3 viewDir = normalize(viewPos - FragPos);
    
    // == =====================================================
    // Our lighting is set up in 3 phases: directional, point lights and an optional flashlight
    // For each phase, a calculate function is defined that calculates the corresponding color
    // per lamp. In the main() function we take all the calculated colors and sum them up for
    // this fragment's final color.
    // == =====================================================
    // phase 1: directional lighting
    vec3 result = CalcDirLight(dirLight, norm, viewDir, oFragPosDirLightSpace, shadowMaps0);
    // phase 2: spot light
    result += CalcSpotLight(spotLights0, norm, FragPos, viewDir, oFragPosSpotLightSpace[0], shadowMaps1);
    //result += CalcSpotLight(spotLights1, norm, FragPos, viewDir, oFragPosSpotLightSpace[1], shadowMaps2);
    
    FragColor = vec4(result, 1.0);
}

// calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, vec4 fragPosLightSpace, sampler2D shadowMap)
{
    vec3 texColor = vec3(texture(texAlbedo, oTexCoords));
    vec3 lightDir = normalize(-light.direction);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), texShininess);
    // combine results
    vec3 ambient = light.ambient * texColor;
    vec3 diffuse = light.color.rgb * diff;
    vec3 specular = spec * texSpecular; //vec3(texture(material.specular, oTexCoords));
    float shadow = ShadowCalculation(fragPosLightSpace, light.position, shadowMap);
    return (ambient +  (1.0 - shadow) * (diffuse + specular)) * texColor;
}

// calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir, vec4 fragPosLightSpace, sampler2D shadowMap)
{
    vec3 texColor = vec3(texture(texAlbedo, oTexCoords));
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), texShininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.lightRange.x + light.lightRange.y * distance + light.lightRange.z * (distance * distance));    
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // combine results
    vec3 ambient = light.ambient * texColor;
    vec3 diffuse = light.color.rgb * diff;
    vec3 specular = spec * texSpecular;//vec3(texture(material.specular, oTexCoords));
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    float shadow = ShadowCalculation(fragPosLightSpace, light.position, shadowMap);
    return (ambient +  (1.0 - shadow) * (diffuse + specular)) * texColor;
}

float ShadowCalculation(vec4 fragPosLightSpace, vec3 lightPos, sampler2D shadowMap)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // calculate bias (based on depth map resolution and slope)
    vec3 normal = normalize(oNormal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);
    // check whether current frag pos is in shadow
    // float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;
    // PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    
    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;
        
    return shadow;
}