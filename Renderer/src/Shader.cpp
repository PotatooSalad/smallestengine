#include "../include/Shader.h"
#include "../../Core/include/globalDefs.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

Shader::Shader(const char* fragmentShaderSource, const char* vertexShaderSource, const char* geometryShaderSource)
{
    int fragmentShader = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
    int vertexShader = compileShader(vertexShaderSource, GL_VERTEX_SHADER);
    int geometryShader = geometryShaderSource != nullptr ? compileShader(geometryShaderSource, GL_GEOMETRY_SHADER) : -1;
    shaderID = glCreateProgram();
    glAttachShader(shaderID, vertexShader);
    glAttachShader(shaderID, fragmentShader);
    if (geometryShader != -1)
    {
        glAttachShader(shaderID, geometryShader);
    }
    glLinkProgram(shaderID);

    int success;
    char infoLog[512];
    // check for linking errors
    glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderID, 512, NULL, infoLog);
        LOGMSG(infoLog);
    }

    retrieveShaderInfo(GL_ACTIVE_ATTRIBUTES, attributes);
    retrieveShaderInfo(GL_ACTIVE_UNIFORMS, uniforms);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    if (geometryShader != -1)
    {
        glDeleteShader(geometryShader);
    }
}

Shader::~Shader()
{
    glDeleteProgram(shaderID);
}

void Shader::use()
{
    // TODO: optimization
    glUseProgram(shaderID);
}

int Shader::compileShader(const char* code, int shaderType)
{
    int shaderID = glCreateShader(shaderType);
    glShaderSource(shaderID, 1, &code, NULL);
    glCompileShader(shaderID);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shaderID, 512, NULL, infoLog);
        std::string info = (shaderType == GL_VERTEX_SHADER) ? "VERTEX " : (shaderType == GL_FRAGMENT_SHADER) ? "FRAGMENT " : "GEOMETRY ";
        info += infoLog;
        LOGMSG(info.c_str());
        return -1;
    }
    return shaderID;
}
#include <iostream>
void Shader::retrieveShaderInfo(uint shaderInfoType, std::unordered_map<std::string, uint>& buffer)
{
    if (shaderInfoType == GL_ACTIVE_ATTRIBUTES || shaderInfoType == GL_ACTIVE_UNIFORMS)
    {
        int count;
        glGetProgramiv(shaderID, shaderInfoType, &count);

        // Getting appropriate variable names from shader
        // Buffer size handled in constant expression
        char name[NAME_BUF_SIZE]; // Variable name in GLSL
        int length; // Name length
        int size; // Size of the variable
        uint type; // Type of the variable
        //std::cout << std::to_string(shaderInfoType) << " size: " << std::to_string(count) << std::endl;

        for (int i = 0; i < count; i++)
        {
            if (shaderInfoType == GL_ACTIVE_UNIFORMS)
            {
                glGetActiveUniform(shaderID, i, NAME_BUF_SIZE, &length, &size, &type, name);
                buffer[std::string(name)] = type; // Insert key value pair
                //std::cout << "Uniform: " << name << std::endl;
            }
            else if (shaderInfoType == GL_ACTIVE_ATTRIBUTES)
            {
                glGetActiveAttrib(shaderID, i, NAME_BUF_SIZE, &length, &size, &type, name);
                buffer[std::string(name)] = type;
                //std::cout << "Attribute: " << name << std::endl;
            }
        }
    }
    else
    {
        throw std::exception("Function received wrong shader info type\n");
    }
}

void Shader::setBool(const char* name, bool value) const
{
    glUniform1i(glGetUniformLocation(shaderID, name), (int)value);
}
void Shader::setBool(const std::string& name, bool value) const
{
    setBool(name.c_str(), value);
}

void Shader::setInt(const char* name, int value) const
{
    glUniform1i(glGetUniformLocation(shaderID, name), value);
}
void Shader::setInt(const std::string& name, int value) const
{
    setInt(name.c_str(), value);
}

void Shader::setFloat(const char* name, float value) const
{
    glUniform1f(glGetUniformLocation(shaderID, name), value);
}
void Shader::setFloat(const std::string& name, float value) const
{
    setFloat(name.c_str(), value);
}

void Shader::setVec2(const char* name, glm::vec2& vec) const
{
    glUniform2f(glGetUniformLocation(shaderID, name), vec.x, vec.y);
}
void Shader::setVec2(const std::string& name, glm::vec2& vec) const
{
    setVec2(name.c_str(), vec);
}

void Shader::setVec3(const char* name, float x, float y, float z) const
{
    glUniform3f(glGetUniformLocation(shaderID, name), x, y, z);
}
void Shader::setVec3(const char* name, const glm::vec3& vec) const
{
    setVec3(name, vec.x, vec.y, vec.z);
}
void Shader::setVec3(const std::string& name, float x, float y, float z) const
{
    setVec3(name.c_str(), x, y, z);
}
void Shader::setVec3(const std::string& name, const glm::vec3& vec) const
{
    setVec3(name.c_str(), vec.x, vec.y, vec.z);
}

void Shader::setVec4(const char* name, float x, float y, float z, float w) const
{
    glUniform4f(glGetUniformLocation(shaderID, name), x, y, z, w);
}
void Shader::setVec4(const char* name, const glm::vec4& vec) const
{
    setVec4(name, vec.x, vec.y, vec.z, vec.w);
}
void Shader::setVec4(const std::string& name, float x, float y, float z, float w) const
{
    setVec4(name.c_str(), x, y, z, w);
}
void Shader::setVec4(const std::string& name, const glm::vec4& vec) const
{
    setVec4(name.c_str(), vec.x, vec.y, vec.z, vec.w);
}

void Shader::setMat4(const char* name, const glm::mat4& mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(shaderID, name), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat4(const std::string& name, const glm::mat4& mat) const
{
    setMat4(name.c_str(), mat);
}

void Shader::setMat3(const char* name, const glm::mat3& mat) const
{
    glUniformMatrix3fv(glGetUniformLocation(shaderID, name), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat3(const std::string& name, const glm::mat3& mat) const
{
    setMat3(name.c_str(), mat);
}