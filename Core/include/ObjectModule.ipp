#pragma once

#include "../include/ObjectModule.h"
#include "../../ECS/Base/include/Entity.h"

template<typename T>
T* ObjectModule::newEmptyComponentForLastEntity()
{
	components.push_back(new T());
	entities[entities.size() - 1]->addComponent(components[components.size() - 1]);
	return dynamic_cast<T*>(components[components.size() - 1]);
}