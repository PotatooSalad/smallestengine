#include "../include/Cylinder.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

void Cylinder::getCylinderMesh(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	int indicesBase = vertices.size();
	basePos.y -= height / 2;
	Vertex v(basePos);

	float segHeight = height / (float)heightSegments;
	bool isThickness = thickness > 0.001f;
	if (!isThickness)
	{
		v.normal = glm::vec3(0.0f, -1.0f, 0.0f);
		v.texcoord = { 0.5, 0.5 };
		vertices.push_back(v); //bottom Vertex
		
		v.position = basePos + glm::vec3(0.0f, height, 0.0f);
		v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
		v.texcoord = { 0.5, 0.5 };
		vertices.push_back(v); //top vertex
	}

	for (int i = 0; i < baseSegments; ++i)
	{
		int indiceIndex = vertices.size();
		//bottom base
		v.normal = glm::vec3(0.0f, -1.0f, 0.0f);
		v.texcoord.x = -unitCircleVertices[i].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f;
		if (isThickness)
		{
			v.position = unitCircleVertices[i] * (baseRadius - thickness) + basePos;
			vertices.push_back(v);
		}

		v.position = unitCircleVertices[i] * baseRadius + basePos;
		vertices.push_back(v);

		v.texcoord.x = -unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
		if (isThickness)
		{
			// rotate vertex
			v.position = unitCircleVertices[(i + 1) % baseSegments] * (baseRadius - thickness) + basePos;
			vertices.push_back(v);
		}

		v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + basePos;
		vertices.push_back(v);

		if (isThickness)
		{
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}
		else
		{
			indices.push_back(indicesBase);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 1);
		}

		// side triangles
		for (int j = 1; j <= heightSegments; ++j)
		{
			indiceIndex = vertices.size();
			v.position = unitCircleVertices[i] * baseRadius + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //0

			v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //1

			v.position = unitCircleVertices[i] * baseRadius + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //2

			v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //3

			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}

		indiceIndex = vertices.size();

		//TOP
		v.position = unitCircleVertices[i] * baseRadius + glm::vec3(basePos.x, height + basePos.y, basePos.z);
		v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
		v.texcoord.x = unitCircleVertices[i].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f;
		vertices.push_back(v);

		if (isThickness)
		{
			v.position = unitCircleVertices[i] * (baseRadius - thickness) + glm::vec3(basePos.x, height + basePos.y, basePos.z);
			v.texcoord.x = unitCircleVertices[i].x * 0.5f + 0.5f * (baseRadius - thickness) / baseRadius;
			v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f * (baseRadius - thickness) / baseRadius;
			vertices.push_back(v);
		}

		v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + glm::vec3(basePos.x, height + basePos.y, basePos.z);
		v.texcoord.x = unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
		vertices.push_back(v);

		if (isThickness)
		{
			v.position = unitCircleVertices[(i + 1) % baseSegments] * (baseRadius - thickness) + glm::vec3(basePos.x, height + basePos.y, basePos.z);
			v.texcoord.x = unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f * (baseRadius - thickness) / baseRadius;
			v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f * (baseRadius - thickness) / baseRadius;
			vertices.push_back(v);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}
		else
		{
			indices.push_back(indicesBase + 1);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 1);
		}
		// inside triangles
		for (int j = heightSegments; j >= 1 && isThickness; --j)
		{
			indiceIndex = vertices.size();
			v.position = unitCircleVertices[i] * (baseRadius - thickness) + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //2

			v.position = unitCircleVertices[(i + 1) % baseSegments] * (baseRadius - thickness) + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //3

			v.position = unitCircleVertices[i] * (baseRadius - thickness) + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //0

			v.position = unitCircleVertices[(i + 1) % baseSegments] * (baseRadius - thickness) + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //1

			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}
	}
}

void Cylinder::getCylinderMeshBowl(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
	int indicesBase = vertices.size();
	basePos.y -= height / 2;
	Vertex v(basePos);
	v.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	v.texcoord = { 0.5, 0.5 };
	vertices.push_back(v); //bottom vertex

	float segHeight = height / (float)heightSegments;
	bool isThickness = thickness > 0.001f;

	if (!isThickness)
	{
		v.position = basePos + glm::vec3(0.0f, height, 0.0f);
		v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
		v.texcoord = { 0.5, 0.5 };
		vertices.push_back(v); //top vertex
	}
	int indiceIndex;
	float heightRadius;
	for (int i = 0; i < baseSegments; ++i)
	{
		//bottom base triangle
		indices.push_back(indicesBase);

		v.position = unitCircleVertices[i] * baseRadius + basePos;
		v.normal = glm::vec3(0.0f, -1.0f, 0.0f);
		v.texcoord.x = -unitCircleVertices[i].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f;

		vertices.push_back(v);
		indices.push_back(vertices.size() - 1);

		// rotate vertex
		v.position = unitCircleVertices[(i + 1) % baseSegments] * baseRadius + basePos;
		v.texcoord.x = -unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
		vertices.push_back(v);
		indices.push_back(vertices.size() - 1);

		// side triangles
		for (int j = 1; j <= heightSegments; ++j)
		{
			indiceIndex = vertices.size();
			heightRadius = baseRadius + (float)(j - 1) / heightSegments * (topRadius - baseRadius);
			v.position = unitCircleVertices[i] * heightRadius + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //0

			v.position = unitCircleVertices[(i + 1) % baseSegments] * heightRadius + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
			vertices.push_back(v); //1

			heightRadius = baseRadius + (float)(j) / heightSegments * (topRadius - baseRadius);
			v.position = unitCircleVertices[i] * heightRadius + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //2

			v.position = unitCircleVertices[(i + 1) % baseSegments] * heightRadius + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //3

			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}

		indiceIndex = vertices.size();

		//TOP
		v.position = unitCircleVertices[i] * topRadius + glm::vec3(basePos.x, height + basePos.y, basePos.z);
		v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
		v.texcoord.x = unitCircleVertices[i].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f;
		vertices.push_back(v);

		if (isThickness)
		{
			v.position = unitCircleVertices[i] * (topRadius - thickness) + glm::vec3(basePos.x, height + basePos.y, basePos.z);
			v.texcoord.x = unitCircleVertices[i].x * 0.5f + 0.5f * (topRadius - thickness) / topRadius;
			v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f * (topRadius - thickness) / topRadius;
			vertices.push_back(v);
		}
		
		v.position = unitCircleVertices[(i + 1) % baseSegments] * topRadius + glm::vec3(basePos.x, height + basePos.y, basePos.z);
		v.texcoord.x = unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f;
		v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
		vertices.push_back(v);

		if (isThickness)
		{
			v.position = unitCircleVertices[(i + 1) % baseSegments] * (topRadius - thickness) + glm::vec3(basePos.x, height + basePos.y, basePos.z);
			v.texcoord.x = unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f * (topRadius - thickness) / topRadius;;
			v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f * (topRadius - thickness) / topRadius;;
			vertices.push_back(v);

			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 2);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 3);
			indices.push_back(indiceIndex + 1);
		}
		else
		{
			indices.push_back(indicesBase + 1);
			indices.push_back(indiceIndex);
			indices.push_back(indiceIndex + 1);
		}

		// inside triangles
		for (int j = heightSegments; j >= 1 && isThickness; --j)
		{
			indiceIndex = vertices.size();
			heightRadius = baseRadius + (float)(j)/ heightSegments * (topRadius - baseRadius);
			v.position = unitCircleVertices[i] * (heightRadius - thickness) + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[i];
			v.texcoord.x = i / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;
			vertices.push_back(v); //2

			v.position = unitCircleVertices[(i + 1) % baseSegments] * (heightRadius - thickness) + glm::vec3(basePos.x, segHeight * j + basePos.y, basePos.z);
			v.normal = unitCircleVertices[(i + 1) % baseSegments];
			v.texcoord.x = (i + 1) / baseSegments;
			v.texcoord.y = 1.0f - float(j) / heightSegments;v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
	
			vertices.push_back(v); //3
			if ((segHeight * (j - 1)) > thickness)
			{
				heightRadius = baseRadius + (float)(j - 1)/ heightSegments * (topRadius - baseRadius);
				v.position = unitCircleVertices[i] * (heightRadius - thickness) + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
				v.normal = unitCircleVertices[i];
				v.texcoord.x = i / baseSegments;
				v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
				vertices.push_back(v); //0

				v.position = unitCircleVertices[(i + 1) % baseSegments] * (heightRadius - thickness) + glm::vec3(basePos.x, segHeight * (j - 1) + basePos.y, basePos.z);
				v.normal = unitCircleVertices[(i + 1) % baseSegments];
				v.texcoord.x = (i + 1) / baseSegments;
				v.texcoord.y = 1.0f - float(j - 1) / heightSegments;
				vertices.push_back(v); //1

				indices.push_back(indiceIndex);
				indices.push_back(indiceIndex + 2);
				indices.push_back(indiceIndex + 3);
				indices.push_back(indiceIndex);
				indices.push_back(indiceIndex + 3);
				indices.push_back(indiceIndex + 1);
			}
			else
			{
				heightRadius = baseRadius + (thickness / height) * (topRadius - baseRadius);
				v.position = unitCircleVertices[i] * (heightRadius - thickness) + glm::vec3(basePos.x, thickness + basePos.y, basePos.z);
				v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
				v.texcoord.x = unitCircleVertices[i].x * 0.5f + 0.5f;
				v.texcoord.y = -unitCircleVertices[i].z * 0.5f + 0.5f;
				vertices.push_back(v); //0

				v.position = unitCircleVertices[(i + 1) % baseSegments] * (heightRadius - thickness) + glm::vec3(basePos.x, thickness + basePos.y, basePos.z);
				v.texcoord.x = unitCircleVertices[(i + 1) % baseSegments].x * 0.5f + 0.5f;
				v.texcoord.y = -unitCircleVertices[(i + 1) % baseSegments].z * 0.5f + 0.5f;
				vertices.push_back(v); //1

				indices.push_back(indiceIndex);
				indices.push_back(indiceIndex + 2);
				indices.push_back(indiceIndex + 3);
				indices.push_back(indiceIndex);
				indices.push_back(indiceIndex + 3);
				indices.push_back(indiceIndex + 1);

				v.position = basePos + glm::vec3(0, thickness, 0);
				v.texcoord = { 0.5, 0.5 };
				vertices.push_back(v); //bottom base vertex
				indices.push_back(indiceIndex + 4);
				indices.push_back(indiceIndex + 2);
				indices.push_back(indiceIndex + 3);
				break;
			}
		}
	}
}