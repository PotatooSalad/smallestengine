#include "../include/Marble.h"
#include <glm/gtx/compatibility.hpp>

Marble::Marble(glm::ivec2 size, glm::vec3 baseColor, glm::vec3 additColor, ProceduralEngineSetup setup) : Procedural(size, baseColor, setup)
{
	this->size = size;
	this->additColor = additColor;
	countTexture();
	//saveData();
}

void Marble::countTexture()
{
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			double xyVal = ((double)x / 10) + ((double)y / 20) + turbulencePower * turbulence(x, y, turbulenceSize);
			float sineValue = (float)glm::sin(xyVal * PI);
			
			texData[y * size.x * 3 + x * 3 + 0] = doubleToByte(glm::lerp(baseColor.r, additColor.r, sineValue));
			texData[y * size.x * 3 + x * 3 + 1] = doubleToByte(glm::lerp(baseColor.g, additColor.g, sineValue));
			texData[y * size.x * 3 + x * 3 + 2] = doubleToByte(glm::lerp(baseColor.b, additColor.b, sineValue));
		}
	}
}