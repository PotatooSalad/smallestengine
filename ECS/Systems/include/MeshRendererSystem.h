#pragma once

#include "../../Base/include/System.h"

struct Transform;
struct MeshRenderer;

class MeshRendererSystem : public System
{
public:
	MeshRendererSystem() = default;
	virtual ~MeshRendererSystem() = default;
protected:
	virtual bool assertEntity(Entity* entity);
	virtual void start();
private:
	Transform* transform;
	MeshRenderer* meshRenderer;
};