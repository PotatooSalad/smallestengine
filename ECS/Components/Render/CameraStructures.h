#pragma once

#include <glm/vec3.hpp>

enum class CameraProjection 
{ 
	PERSPECTIVE,
	ORTHOGRAPHIC 
};

enum class CameraControl
{
	NONE,
	FREE,
	FIRST_PERSON,
	THIRD_PERSON
};

struct ViewFrustum
{
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;

	// Clipping planes dimensions
	float Hnear;
	float Wnear;
	float Hfar;
	float Wfar;

	float aspectRatio;

	float fov;
	float nearPlane;
	float farPlane;
};