#include "../include/Box.h"
#include <glm/gtc/epsilon.hpp>

uint Box::boxIndices[36] = {
    0, 2, 3,        0, 3, 1,    // front
    4, 6, 7,        4, 7, 5,    // back
    8, 10, 11,      8, 11, 9,  // right
    14, 12, 13,     14, 13, 15, // left
    17, 19, 18,     17, 18, 16, // bottom 
    20, 22, 23,     20, 23, 21  // top
};

Box::Box(glm::vec3 center, glm::vec3 size, glm::vec3 texOffset, glm::vec3 texDiv)
{
	this->center = center;
	this->size = size;
    leftDownFrontCorner = center - size / 2.0f;
    this->texOffset = texOffset;
    this->texDiv = texDiv;
}

void Box::getBox(std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
    int offset = vertices.size();

    leftDownFrontCorner.z += size.z;
    glm::vec2 innerDiv = { size.x + 2 * size.y,size.z + 2 * size.y };

    //-------------------------FRONT---------------------
    //LEFT DOWN (0)
    Vertex v(leftDownFrontCorner.x, leftDownFrontCorner.y, leftDownFrontCorner.z);
    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y -= size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    {
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x -= size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = 0.0f;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = 1.0f;

    vertices.push_back(v);

    //LEFT UP (1)
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    {
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x -= size.x;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = 1.0f;

    vertices.push_back(v);

    //RIGHT DOWN (2)
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y -= size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    {
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.y + size.x) / innerDiv.x;
        v.texcoord.y = 0.0f / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = 1.0f;

    vertices.push_back(v);

    //RIGHT UP (3)
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    {
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.y + size.x) / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = 1.0f;
    vertices.push_back(v);


    //-------------------------BACK---------------------

    //RIGHT DOWN (4) tex right up
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;
    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += (size.z + size.y);
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.y + size.x) / innerDiv.x;
        v.texcoord.y = 1.0f;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = -1.0f;
    vertices.push_back(v);

    //RIGHT UP (5) tex right down
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.y + size.x) / innerDiv.x;
        v.texcoord.y = (size.y + size.z) / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = -1.0f;
    vertices.push_back(v);

    //LEFT DOWN (6) tex left up
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += (size.z + size.y);
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += (size.z + size.x);
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = 1.0f;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = -1.0f;
    vertices.push_back(v);

    //LEFT UP (7) tex left down
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += (size.z + size.x);
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = (size.y + size.z) / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 0.0f;
    v.normal.z = -1.0f;
    vertices.push_back(v);

    //------------------------RIGHT----------------

    //Left DOWN (2) -8 tex right down
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x + size.y;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //left down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.x + size.y) / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //Left UP (3) -9 tex left down
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { // left up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.x + size.y) / innerDiv.x;
        v.texcoord.y = (size.y + size.z) / innerDiv.y;
    }

    v.normal.x = 1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //RIGHT DOWN (4) -10 tex right up
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += (size.x + size.y);
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //right down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += (size.x + size.z);
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = 1.0f;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //RIGHT UP (5) -11 tex left up
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //right up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += (size.x + size.z);
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = 1.0f;
        v.texcoord.y = (size.y + size.z) / innerDiv.y;
    }

    v.normal.x = 1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //-------------------LEFT------------------

    //Right DOWN (0) -12 tex left down
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x -= size.y;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //right down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = -1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //Right UP (1) -13 tex right down
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //right up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = (size.z + size.y) / innerDiv.y;
    }

    v.normal.x = -1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //LEFT DOWN (6) -14 tex left up
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x -= size.y;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //left down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x -= size.z;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = 0.0f;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = -1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //LEFT UP (7) -15 tex right up
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //left up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x -= size.z;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = 0.0f;
        v.texcoord.y = (size.z + size.y) / innerDiv.y;
    }

    v.normal.x = -1.0f;
    v.normal.y = 0.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //-----------------BOTTOM-----------------

    //LEFT UP (0) -16
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { // left up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y -= size.x;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = (size.y + size.z) / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = -1.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //LEFT DOWN (6) -17
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //left down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y -= size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y -= size.x;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = -1.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //RIGHT UP (2) -18
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { // right up
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.y + size.x) / innerDiv.x;
        v.texcoord.y = (size.y + size.z) / innerDiv.x;
    }

    v.normal.x = 0.0f;
    v.normal.y = -1.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //RIGHT DOWN (4) -19
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { //right down
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y -= size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.x + size.y) / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = -1.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //-------------------TOP------------------

    //LEFT DOWN (1) -20
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    { 
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += (size.y + size.x);
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }
    
    v.normal.x = 0.0f;
    v.normal.y = 1.0f;
    v.normal.z = 0.0f;

    vertices.push_back(v);

    //LEFT UP (7) -21
    v.position.x = leftDownFrontCorner.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += (size.y + size.z);
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right up
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += (size.y + size.x);
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = size.y / innerDiv.x;
        v.texcoord.y = (size.z + size.y) / innerDiv.y;
    }
    

    v.normal.x = 0.0f;
    v.normal.y = 1.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //RIGHT DOWN (3) -22
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += 0.0f;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // left down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += 0.0f;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.x + size.y) / innerDiv.x;
        v.texcoord.y = size.y / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 1.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);

    //RIGHT UP (5) -23
    v.position.x = leftDownFrontCorner.x + size.x;
    v.position.y = leftDownFrontCorner.y + size.y;
    v.position.z = leftDownFrontCorner.z - size.z;

    if (texDiv.y < 0.01f && texDiv.x > 0.01f && texDiv.z > 0.01f) //XZ
    {
        v.texcoord = { texOffset.x, texOffset.z };
        v.texcoord.x += size.x;
        v.texcoord.y += size.z;
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.z;
    }
    else if (texDiv.z < 0.01f && texDiv.x > 0.01f && texDiv.y > 0.01f) //XY
    {
        v.texcoord = { texOffset.x, texOffset.y };
        v.texcoord.x += size.x;
        v.texcoord.y += (size.y + size.z);
        v.texcoord.x /= texDiv.x;
        v.texcoord.y /= texDiv.y;
    }
    else if (texDiv.x < 0.01f && texDiv.z > 0.01f && texDiv.y > 0.01f) //ZY
    { // right down
        v.texcoord = { texOffset.z, texOffset.y };
        v.texcoord.x += size.z;
        v.texcoord.y += size.y;
        v.texcoord.x /= texDiv.z;
        v.texcoord.y /= texDiv.y;
    }
    else
    {
        v.texcoord.x = (size.x + size.y) / innerDiv.x;
        v.texcoord.y = (size.z + size.y) / innerDiv.y;
    }

    v.normal.x = 0.0f;
    v.normal.y = 1.0f;
    v.normal.z = 0.0f;
    vertices.push_back(v);


    //------------------INDICES-------------
    for (int i = 0; i < 36; ++i)
    {
        indices.push_back(offset + boxIndices[i]);
    }
}

void Box::getHoledBox(glm::vec3 holePosition, glm::vec3 holeSize, std::vector<Vertex>& vertices, std::vector<uint>& indices)
{
    glm::vec3 boxSize1, boxCenter1;
    glm::vec3 offset1;
    glm::vec3 boxSize2, boxCenter2;
    glm::vec3 offset2;
    glm::vec3 boxSize3, boxCenter3;
    glm::vec3 offset3;
    glm::vec3 boxSize4, boxCenter4;
    glm::vec3 offset4;
    glm::vec3 texSize = { 0, 0, 0 };

    float min = glm::min(glm::min(size.x, size.y), size.z);
    if (glm::epsilonEqual(min, size.z, 0.01f)) //hole is on XY axis
    {
        boxSize1 = {glm::distance(leftDownFrontCorner.x, holePosition.x), size.y, size.z};
        boxCenter1 = leftDownFrontCorner + (boxSize1 / 2.0f);
        offset1 = {size.z, size.z, 0.0f};

        boxSize2 = {holeSize.x, holePosition.y - leftDownFrontCorner.y, size.z};
        glm::vec3 ldfc2 = { leftDownFrontCorner.x + boxSize1.x, leftDownFrontCorner.y, leftDownFrontCorner.z };
        boxCenter2 = ldfc2 + (boxSize2 / 2.0f);
        offset2 = { boxSize1.x + size.z, size.z, 0.0f };

        boxSize3 = { holeSize.x, size.y - boxSize2.y - holeSize.y, size.z };
        glm::vec3 ldfc3 = { ldfc2.x, holePosition.y + holeSize.y, leftDownFrontCorner.z };
        boxCenter3 = ldfc3 + (boxSize3 / 2.0f);
        offset3 = { boxSize1.x + size.z, boxSize2.y + holeSize.y + size.z, 0.0f};

        boxSize4 = { size.x - holeSize.x - boxSize1.x, size.y, size.z };
        glm::vec3 ldfc4 = {leftDownFrontCorner.x + boxSize1.x + holeSize.x, leftDownFrontCorner.y, leftDownFrontCorner.z};
        boxCenter4 = ldfc4 + (boxSize4 / 2.0f);
        offset4 = { boxSize1.x + boxSize2.x + size.z, size.z, 0.0f };

        texSize.x = boxSize1.x + boxSize2.x + boxSize4.x + 2 * size.z;
        texSize.y = boxSize1.y + 2 * size.z;
        texSize.z = 0.0f;
    }
    else if(glm::epsilonEqual(min, size.x, 0.01f)) //hole is on ZY axis
    {
        boxSize1 = { size.x, size.y, glm::distance(leftDownFrontCorner.z, holePosition.z) };
        boxCenter1 = leftDownFrontCorner + (boxSize1 / 2.0f);
        offset1 = { 0.0f, size.x, size.x };

        boxSize2 = { size.x, holePosition.y - leftDownFrontCorner.y, holeSize.z };
        glm::vec3 ldfc2 = { leftDownFrontCorner.x, leftDownFrontCorner.y, leftDownFrontCorner.z + boxSize1.z };
        boxCenter2 = ldfc2 + (boxSize2 / 2.0f);
        offset2 = { 0.0f, size.x, boxSize1.z + size.x};

        boxSize3 = { size.x, size.y - boxSize2.y - holeSize.y, holeSize.z };
        glm::vec3 ldfc3 = { leftDownFrontCorner.x, holePosition.y + holeSize.y, ldfc2.z};
        boxCenter3 = ldfc3 + (boxSize3 / 2.0f);
        offset3 = { 0.0f, boxSize2.y + holeSize.y + size.x, boxSize1.z + size.x };

        boxSize4 = { size.x, size.y, size.z - holeSize.z - boxSize1.z };
        glm::vec3 ldfc4 = { leftDownFrontCorner.x, leftDownFrontCorner.y, leftDownFrontCorner.z + boxSize1.z + holeSize.z };
        boxCenter4 = ldfc4 + (boxSize4 / 2.0f);
        offset4 = { 0.0f, size.x, boxSize1.z + boxSize2.z + size.x };

        texSize.x = 0.0f;
        texSize.y = boxSize1.y + 2 * size.x;
        texSize.z = boxSize1.z + boxSize2.z + boxSize4.z + 2 * size.x;
    }
    else //hole on XZ axis
    {
        boxSize1 = { glm::distance(leftDownFrontCorner.x, holePosition.x), size.y, size.z };
        boxCenter1 = leftDownFrontCorner + (boxSize1 / 2.0f);
        offset1 = { size.y , 0.0f, size.y };

        boxSize2 = {holeSize.x, size.y, holePosition.z - leftDownFrontCorner.z};
        glm::vec3 ldfc2 = { leftDownFrontCorner.x + boxSize1.x, leftDownFrontCorner.y, leftDownFrontCorner.z };
        boxCenter2 = ldfc2 + (boxSize2 / 2.0f);
        offset2 = { boxSize1.x + size.y, 0.0f, size.y };

        boxSize3 = { holeSize.x, size.y, size.z - boxSize2.z - holeSize.z };
        glm::vec3 ldfc3 = { ldfc2.x, leftDownFrontCorner.y, holePosition.z + holeSize.z };
        boxCenter3 = ldfc3 + (boxSize3 / 2.0f);
        offset3 = { boxSize1.x + size.y, 0.0f, boxSize2.z + holeSize.z + size.y };

        boxSize4 = { size.x - holeSize.x - boxSize1.x, size.y, size.z };
        glm::vec3 ldfc4 = { leftDownFrontCorner.x + boxSize1.x + holeSize.x, leftDownFrontCorner.y, leftDownFrontCorner.z };
        boxCenter4 = ldfc4 + (boxSize4 / 2.0f);
        offset4 = { boxSize1.x + boxSize2.x + size.y, 0.0f, size.y };

        texSize.x = boxSize1.x + boxSize2.x + boxSize4.x + 2 * size.y;
        texSize.y = 0.0f;
        texSize.z = boxSize1.z + 2 * size.y;
    }

    /*printf("Offset1: %f, %f, %f\n", offset1.x, offset1.y, offset1.z);
    printf("Offset2: %f, %f, %f\n", offset2.x, offset2.y, offset2.z);
    printf("Offset3: %f, %f, %f\n", offset3.x, offset3.y, offset3.z);
    printf("Offset4: %f, %f, %f\n", offset4.x, offset4.y, offset4.z);

    printf("Box size 1: %f, %f, %f\n", boxSize1.x, boxSize1.y, boxSize1.z);
    printf("Box size 2: %f, %f, %f\n", boxSize2.x, boxSize2.y, boxSize2.z);
    printf("Box size 3: %f, %f, %f\n", boxSize3.x, boxSize3.y, boxSize3.z);
    printf("Box size 4: %f, %f, %f\n", boxSize4.x, boxSize4.y, boxSize4.z);

    printf("Tex Size: %f, %f, %f\n", texSize.x, texSize.y, texSize.z);
    printf("Size: %f, %f, %f\n\n\n", size.x, size.y, size.z);
    printf("======================================\n");*/

    if (boxSize1.x > 0.01f && boxSize1.y > 0.01f && boxSize1.z > 0.01f)
    {
        Box child(boxCenter1, boxSize1, offset1, texSize);
        child.getBox(vertices, indices);
    }

    if (boxSize2.x > 0.01f && boxSize2.y > 0.01f && boxSize2.z > 0.01f)
    {
        Box child(boxCenter2, boxSize2, offset2, texSize);
        child.getBox(vertices, indices);
    }

    if (boxSize3.x > 0.01f && boxSize3.y > 0.01f && boxSize3.z > 0.01f)
    {
        Box child(boxCenter3, boxSize3, offset3, texSize);
        child.getBox(vertices, indices);
    }

    if (boxSize4.x > 0.01f && boxSize4.y > 0.01f && boxSize4.z > 0.01f)
    {
        Box child(boxCenter4, boxSize4, offset4, texSize);
        child.getBox(vertices, indices);
    }
}
