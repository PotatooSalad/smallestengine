#pragma once

class Entity;

enum class UpdateType
{
	START,
	FRAME,
	FIXED
};

class System
{
public:
	System() {}

	void process(Entity* entity, UpdateType updateType);

protected:
	/// <summary>
	/// Checks if given entity contain all required components
	/// </summary>
	/// <param name="entity">pointer to asserted entity</param>
	/// <returns> true if entity has all required components</returns>
	virtual bool assertEntity(Entity* entity) = 0;
	/// <summary>
	/// initialize all processed entities, run before first update
	/// </summary>
	virtual void start() {}
	/// <summary>
	/// contain logic of the system, runs once per frame
	/// </summary>
	virtual void frameUpdate() {}
	/// <summary>
	/// contains logic of the system, runs in fixed time steps
	/// </summary>
	virtual void fixedUpdate() {}

	virtual ~System() = default;
};