#include "../include/SystemsModule.h"
#include "../include/Message.inl"
#include "../../ECS/Base/include/Entity.h"
#include <string>

void SystemsModule::receiveMessage(Message msg)
{
}

void SystemsModule::addSystem(System* system)
{
	systems.push_back(system);
}

void SystemsModule::run(UpdateType updateType)
{
	for (auto sys : systems)
	{
		for (Entity* ent : *entities)
		{
			sys->process(ent, updateType);
		}
	}
}