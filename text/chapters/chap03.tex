\chapter{Tekstury}
Tekstury są pierwszym z~zasobów generowanych przez aplikację. Od nich w~dużym stopniu zależy czy scena będzie realistyczna, czy nie. Można zaimplementować wiele efektów wchodzących w~skład PBR, ale jeśli użyje się prostych, jednokolorowych tekstur to scena nie będzie realistyczna. Podczas tworzenia algorytmów generujących kładziono nacisk na realizm, nawet jeśli dobieranie współczynników miało się odbywać metodą prób i~błędów.
	\section{Klasa Procedural}
Podczas tworzenia klas związanych z~generowaniem tekstur zdecydowano się na wprowadzenie abstrakcyjnej uogólniającej klasy \verb|Procedural|. Przechowuje ona wielkość i~dane generowanego obrazu, oraz jeden bazowy kolor. Dodatkowo, każda klasa dziedzicząca po \verb|Procedural| powinna zaimplementować funkcję \verb|countTexture()|, która ma za zadanie wypełniać tablicę danych tekstury.\par 
Klasa \verb|Procedural| implementuje generowanie pseudolosowych liczb zmiennoprzecinkowych przy użyciu generatora liczb pseudolosowych Mersenne Twister\cite{mt19937}. W~aplikacji generator wykorzystuje się głównie do stworzenia tablicy przechowującej szum, który jest później używany podczas obliczania wartości kolorów w~teksturach. Na potrzeby konfiguracji generatora w~zależności od potrzeb algorytmów generujących tekstury, została stworzona struktura przechowująca wszystkie istotne dane konfiguracyjne. Podczas konstruowania obiektów dziedziczących po klasie \verb|Procedural| można przekazać ziarno (seed) generatora, w~jakim przedziale mają się mieścić generowane liczby oraz czy w~ogóle powinna zostać wygenerowana tablica szumu.\par
Na potrzeby generowania tekstur zostały również zaimplementowane algorytmy szumu Perlina\cite{perlin} dla jednego (listing \ref{lst:perlin1D}) i~dwóch wymiarów (listing \ref{lst:perlin2D}). W~strukturze konfiguracyjnej generatora trzeba było z~tego powodu uwzględnić parametry związane z~tymi algorytmami -- liczbę iteracji algorytmu oraz amplitudę szumu. W~implementacji szumu jednowymiarowego wykorzystano interpolację trygonometryczną, a~przy dwuwymiarowym -- dwuliniową. 
\begin{lstlisting}[caption={Implementacja szumu Perlina dla jednego wymiaru (interpolacja trygonometryczna)}, label={lst:perlin1D}]
float Procedural::perlinSample(float x)
{
	float fract = glm::fract(x);
	float rand1 = randomValue();
	float rand2 = -randomValue();
	float loPos = rand1 * fract;
	float hiPos = rand2 * (1.0f - fract);
	float u = fract * fract * (3.0f - 2.0f * fract);
	float ft = u * PI;
	float f = (1 - glm::cos(ft)) * 0.5f;
	return loPos * (1 - f) + hiPos * f;
}
float Procedural::perlinNoise1D(float x)
{
	float result = 0.0f;
	float amplitude = setup.perlinAmplitude;
	for (int i = 0; i < setup.perlinOctaves; ++i)
	{
		result += amplitude * perlinSample(x);
		x *= 2.0f;
		amplitude *= 0.5f;
	}
	return result;
}
\end{lstlisting}
\begin{lstlisting}[caption={Implementacja szumu Perlina dla dwóch wymiarów (interpolacja dwuliniowa)}, label={lst:perlin2D}]
double Procedural::perlinNoise2D(double x, double y, double size)
{
	double value = 0.0;
	double initialSize = size;

	while (size >= 1)
	{
		value += smoothNoise(x / size, y / size) * size;
		size /= 2.0;
	}
	return value / initialSize;
}

double Procedural::smoothNoise(double x, double y)
{
	double fractX = x - int(x);
	double fractY = y - int(y);

	//wrap around
	int x1 = (int(x) + size.x) % size.x;
	int y1 = (int(y) + size.y) % size.y;

	//neighbor values
	int x2 = (x1 + size.x - 1) % size.x;
	int y2 = (y1 + size.y - 1) % size.y;

	//smooth the noise with bilinear interpolation
	double value = 0.0;
	value += fractX * fractY * noiseArr[y1 * size.x + x1];
	value += (1 - fractX) * fractY * noiseArr[y1 * size.x + x2];
	value += fractX * (1 - fractY) * noiseArr[y2 * size.x + x1];
	value += (1 - fractX) * (1 - fractY) * noiseArr[y2 * size.x + x2];

	return value;
}
\end{lstlisting}\par
	\section{Klasa NormalMap}
Klasa \verb|NormalMap| zajmuje się generowaniem map normalnych\cite{normal}. W~zależności od parametru \verb|generateNoise| generuje się gładka lub zaszumiona tekstura. Mapa normalnych z~szumem jest wykorzystywana do osiągnięcia efektu chropowatości. Przykładowa mapa normalnych oraz efekt jej użycia na powierzchni kuli została przedstawiona na rysunkach \ref{fig:orangeNormal} i~\ref{fig:orangez}. Mapy gładkie zostały zdefiniowane jako jednolita tekstura, której wartość w~każdym pikselu wynosi \verb|(x, y, z) = (0, 0, 1)|.
\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.47\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{figures/orangeNormal.png}
		\caption{Przykładowa mapa normalnych wygenerowana z~szumu w~przedziale ${[0.9, 1)}$}
		\label{fig:orangeNormal}
	\end{subfigure}%
	\hspace*{0.06\textwidth}%
	\begin{subfigure}[b]{0.47\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{figures/orangez.png}
		\caption{Zastosowanie mapy normalnych do imitacji skórki pomarańczy}
		\label{fig:orangez}
	\end{subfigure}
	\caption{Przykładowa mapa normalnych i~efekt jej użycia (opracowanie własne)}
\end{figure}\par 
Algorytm generowania normal map składa się z~dwóch kroków: obliczenia wartości XYZ w~danym pikselu oraz przeliczenia tej wartości do przestrzeni RGB, korzystając ze wzoru:
\begin{eqnarray}
	p_{R/G/B} = 127.5 * (p_{X/Y/Z} + 1.0)
\end{eqnarray}
Jako, że tekstury są przechowywane w~tablicach zmiennych typu \verb|byte|, przed zapisaniem wartości koloru do danych tekstury, jest ona ,,obcinana'' do wartości między 0 a~255. Analizując dokładnie podany wyżej wzór można zauważyć, że ${p_{R/G/B}}$ nie przekroczy 255 wtedy i tylko wtedy gdy wartości XYZ będą znormalizowane. Operacja obcinania jest tutaj tylko dodatkowym zabezpieczeniem, które nie zabiera dużo pamięci operacyjnej, a~zapewnia, że wartości kolorów w~teksturze będą prawidłowe.
	\section{Klasa Checker}
Klasa \verb|Checker| tworzy teksturę szachownicy. Konstruktor przyjmuje jako parametry (poza tymi dziedziczonymi z~klasy \verb|Procedural|) liczbę kwadratów w~osi X i~drugi kolor tekstury. Tekstura szachownicy dzięki temu może być w dowolnych kolorach. Wielkość kafelka jest wartością wielkości tekstury w~osi X podzieloną przez liczbę kwadratów. Wyznaczając liczbę pól jako wspólny dzielnik wysokości i~szerokości tekstury można osiągnąć efekt pełnych kwadratów, bez nagłego ucięcia kształtu np. w~połowie.\par
W trakcie obliczania tekstury szachownicy wyznaczany jest numer pola w~osi X i~osi Y. Numer pola dla osi X wyznacza kolor kafelka: dla wartości parzystej -- bazowy, a~dla nieparzystej -- dodatkowy. Numer pola w~osi Y odwraca zależność kolorystyczną, jeśli liczba jest nieparzysta. Bez tej zamiany tekstura składałaby się z~dwukolorowych kresek, a~nie z~szachownicy. Przykładowa wygenerowana tekstura została przedstawiona na rysunku \ref{fig:checker}. Zastosowanie tej tekstury na scenie zostało przedstawione na rysunku \ref{fig:sceneFloor}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../figures/checker.png}
	\caption{Przykładowa tekstura szachownicy (opracowanie własne)}
	\label{fig:checker}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{../figures/sceneChecker.png}
	\caption{Tekstura szachownicy użyta na podłodze (opracowanie własne)}
	\label{fig:sceneFloor}
\end{figure}
	\section{Klasa Marble}
Klasa \verb|Marble| zajmuje się generowaniem tekstury marmuru. Tak jak w~przypadku klasy generującej szachownicę, konstruktor klasy \verb|Marble| przyjmuje drugi kolor jako swój parametr. Wówczas, kolor bazowy określa kolor kresek tworzących wzór marmuru, a~drugi kolor -- kolor tła. Implementacja algorytmu została zaczerpnięta z~\cite{marble}.\par
Bazą dla marmurowych kresek jest funkcja ${\sin((x + y) * \pi)}$, która tworzy skośne pasy o~łagodnie przechodzących między sobą kolorach. Funkcję można modyfikować przez pomnożenie (lub podzielenie) jej składników przez czynniki o~różnych wartościach. W~zależności od tych czynników pasy są bardziej lub mniej nachylone względem osi X. W~algorytmie zdecydowano się na modyfikację składników funkcji przez podzielenie odpowiednio x przez 10, a~y przez 20. Baza dla tekstury marmuru została przedstawiona na rysunku \ref{fig:marbleBase}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/marbleBase.png}
	\caption{Przedstawienie graficzne funkcji ${\sin\bigg(\bigg(\frac{x}{10} + \frac{y}{20}\bigg) * \pi\bigg)}$ (opracowanie własne)}
	\label{fig:marbleBase}
\end{figure}\par
Do stworzenia tekstury marmuru potrzebny jest jeszcze jeden krok -- dołączenie funkcji turbulencji, czyli szumu Perlina dla dwóch wymiarów. Wartość funkcji turbulencji jest dołączana jako trzeci składnik sumy. Dla wzmocnienia, ta wartość jest mnożona przez 12. Za wielkość turbulencji, czyli trzeci parametr funkcji obliczającej szum, została wybrana liczba 32. Obie podane wartości wybrano metodą prób i~błędów, by wynikowa tekstura marmuru była realistyczna. Ostateczny wzór generujący teksturę marmuru przedstawia się w~następujący sposób:
\begin{equation}
	f(x, y) = \sin\bigg(\bigg(\frac{x}{10} + \frac{y}{20} + 12 * perlin(x, y, 32)\bigg) * \pi\bigg)
\end{equation}
Fragment wygenerowanej tekstury marmuru został przedstawiony na rysunku \ref{fig:marble}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/marble.png}
	\caption{Fragment tekstury marmuru (opracowanie własne)}
	\label{fig:marble}
\end{figure}
	\section{Klasa Wood}
Klasa \verb|Wood| generuje teksturę drewna. Konstruktor przyjmuje dodatkowo dwa kolory, które są używane do tworzenia zróżnicowanych kolorów słojów. W~oparciu o zagadnienia teoretyczne opisane w~,,Simulating the structure and texture of solid wood''\cite{wood} został stworzony algorytm generujący realistyczną teksturę drewna. Składa się on z~trzech kroków. W~pierwszym kroku jest wykonywana interpolacja liniowa między kolorem bazowym, a~pierwszym dodatkowym. Krokiem interpolacji jest wartość szumu Perlina dla punktu \verb|(uv.x, uv.x) * 10|, gdzie \verb|uv = (x / size.x, y / size.y)|. Ten krok tworzy nieregularnej szerokości pasy, których zagęszczenie określa drugi czynnik mnożenia współrzędnej. Im większy czynnik, tym więcej pasów. Przykładowa tekstura będąca wynikiem po obliczeniu kroku pierwszego została przedstawiona na rysunku \ref{fig:step1}.\par
W kroku drugim tworzone są linie słojów przy użyciu interpolacji liniowej między kolorem z~kroku pierwszego i~drugim dodatkowym kolorem. Dla tej interpolacji krokiem jest wartość tablicy szumu dla indeksu \verb|uv.x + size.y + uv.x| pomnożona przez 0,1. Tak jak w~poprzednim kroku, zastosowanie tylko współrzędnej x powoduje generowanie pionowych pasów, których grubość zależy od drugiego czynnika mnożenia. Im wyższy czynnik, tym grubsze pasy. Wynik drugiego kroku został przedstawiony na rysunku \ref{fig:step2}.\par
Ostatnim krokiem jest wprowadzenie turbulencji do tekstury. Dodaje on niedoskonałości struktury drewna. Wartość turbulencji jest wyznaczona przez funkcję \verb|smoothNoise()| pomnożoną przez 0,1. Wartość czynnika mnożenia wyznacza siłę turbulencji (im większy czynnik, tym większe turbulencje). Jako parametr funkcji szumu przekazywana jest wartość \verb|(uv.x * 500, uv.y * 20)|. W~tym przypadku, wartość współrzędnej x warunkuje gęstość turbulencji, a~wartość dla y -- częstotliwość podziału linii turbulencji. Porównanie wyników dla różnych czynników dla parametru szumu zostało przedstawione w~serii rysunków \ref{fig:steps3}. Wygenerowana tekstura drewna wykorzystana w~aplikacji została przedstawiona na rysunku \ref{fig:step3}. Implementacja algorytmu obliczającego teksturę drewna została zaprezentowana na listingu \ref{lst:wood}.
\begin{figure}[h]
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{../figures/step1.png}
		\caption{Krok pierwszy}
		\label{fig:step1}
	\end{subfigure}
	\hspace*{0.03\textwidth}%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{../figures/step2.png}
		\caption{Krok drugi}
		\label{fig:step2}
	\end{subfigure}
	\hspace*{0.03\textwidth}%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{figures/step3.png}
		\caption{Krok trzeci}
		\label{fig:step3}
	\end{subfigure}
	\caption{Zestawienie tekstur drewna po każdym kroku algorytmu (opracowanie własne)}
\end{figure}
\vspace{0.2\textwidth}
\begin{figure}
	\centering
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{figures/step3.png}
		\caption{Tekstura drewna dla \\parametrów (500, 20)}
	\end{subfigure}
	\hspace*{0.03\textwidth}%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{figures/step3_x100.png}
		\caption{Tekstura drewna dla \\parametrów (100, 20)}
	\end{subfigure}
	\hspace*{0.03\textwidth}%
	\begin{subfigure}{0.3\textwidth}
		\centering
		\includegraphics[width=1.0\textwidth]{figures/step3_y10.png}
		\caption{Tekstura drewna dla \\parametrów (500, 10)}
	\end{subfigure}
	\caption{Porównanie wyników kroku trzeciego dla różnych wartości parametrów (opracowanie własne)}
	\label{fig:steps3}
\end{figure}
\begin{lstlisting}[caption={Implementacja algorytmu obliczającego teksturę drewna}, label={lst:wood}]
for (int y = 0; y < size.y; ++y)
{
	for (int x = 0; x < size.x; ++x)
	{
		vec2 fragCoord = vec2(x, y) + vec2(shift);
		vec2 uv = fragCoord / (vec2)size;
		float ratio = size.x / (float)size.y;
		uv.y *= ratio;

		vec3 value = baseColor;

		value = mix(value, additColor1, vec3(perlinNoise2D(vec2(uv.x) * 10.0f)));
		value = mix(value, additColor2, noiseArr[(int)(uv.x * size.y + uv.x)] * 0.1f);
		value -= smoothNoise(uv * vec2(500.0f, 20.0f)) * 0.1f;

		texData[y * size.x * 3 + (x * 3)] = floatToByte(value.x);
		texData[y * size.x * 3 + (x * 3) + 1] = floatToByte(value.y);
		texData[y * size.x * 3 + (x * 3) + 2] = floatToByte(value.z);

	}
}
\end{lstlisting}
	\section{Klasa Wallpaper}
Klasa \verb|Wallpaper| służy do generowania tekstury o~jednolitym lub lekko zaszumionym kolorze. Wykorzystywana jest do wszystkich innych powierzchni, do których nie zastosowano wymienionych wcześniej tekstur. Dodawany do jednolicie kolorowych tekstur szum o~bardzo małym zakresie przedziału (do 0,1) urozmaica teksturę i~nadaje realizmu. Warto zwrócić uwagę na to, że przy powierzchniach metalowych nie jest zalecane używanie tekstury z~szumem, ponieważ wyglądają one nienaturalnie. Przykłady użycia tekstur jednolitej oraz zniekształconej szumem został przedstawiony na rysunkach \ref{fig:metal} i~\ref{fig:wallpaper}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/metallic.png}
		\caption{Tekstura o~jednolitym kolorze}
		\label{fig:metal}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{../figures/wallpaper.png}
		\caption{Tekstura zniekształcona szumem}
		\label{fig:wallpaper}
	\end{subfigure}
	\cprotect\caption{Zastosowanie tekstur wygenerowanych przez klasę \verb|Wallpaper| (opracowanie własne)}
\end{figure}\par
Na potrzeby wygenerowania tekstury arbuza został dołączony do klasy \verb|Wallpaper| algorytm spełniający to zadanie. Trzeba było też stworzyć alternatywny konstruktor klasy, który przyjmuje drugi kolor. Algorytm wykorzystuje funkcję ${\sin\bigg(\frac{1}{2}x\bigg)}$ do interpolowania wartości kolorów. Wynikiem obliczeń jest tekstura z~łagodnie przechodzącymi między sobą kreskami. Dane tekstury i~jej zastosowanie w~scenie zostały przedstawione na rysunkach \ref{fig:watermel} i~\ref{fig:watermelone}
\begin{figure}[h]
	\centering
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.8\textwidth]{figures/watermel.png}
		\caption{Tekstura arbuza}
		\label{fig:watermel}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{figures/watermelone.png}
		\caption{Tekstura nałożona na sferę}
		\label{fig:watermelone}
	\end{subfigure}
	\caption{Tekstura arbuza i~wykorzystanie jej w~scenie (opracowanie własne)}
\end{figure}