#include "../include/NormalMap.h"

void NormalMap::countTexture()
{
	for (int y = 0; y < size.y; ++y)
	{
		for (int x = 0; x < size.x; ++x)
		{
			glm::vec3 val = setup.generateNoise ? glm::vec3(noiseArr[y * size.x + x], noiseArr[y * size.x + x], 1.0f) : glm::vec3(0, 0, 1.0f);
			val = glm::normalize(val);
			texData[y * size.x * 3 + x * 3] = (byte)glm::clamp((int)(127.5 * (val.x + 1.0)), 0, 255);
			texData[y * size.x * 3 + x * 3 + 1] = (byte)glm::clamp((int)(127.5 * (val.y + 1.0)), 0, 255);
			texData[y * size.x * 3 + x * 3 + 2] = (byte)glm::clamp((int)(127.5 * (val.z + 1.0)), 0, 255);
		}
	}
	//saveData();
}
