#pragma once

#include "../../Core/include/globalDefs.h"
#include "RendererDataStructures.h"
#include <string>
#include <glm/glm.hpp>
#include <map>
#include <unordered_map>

class Shader;
class Texture;

enum class RenderType { Opaque, Transparent };

class Material
{
public:
    Material(Shader* shader, bool enableInstancing = false, std::string name = "", RenderType type = RenderType::Opaque);
	~Material() = default;

	/// <summary>
	/// use the material - set shader etc
	/// </summary>
	void use(RenderMode mode = RenderMode::NORMAL);

	const Shader* getShaderPtr() { return sceneShader; }
	const uint getID() { return ID; }
    const char* getName() { return name.c_str(); }

    bool hasShadowShader() { return shadowMappingShader != nullptr; }
    bool hasCubemapShader() { return cubemapShader != nullptr; }
    bool isMaterialGlass() { return isGlass; }
    bool isInstancingEnabled() { return enableInstancing; }

    void setMVP(const glm::mat4& M, const glm::mat4& VP, std::string name = "MVP");
    void setVP(const glm::mat4& VP, std::string name = "VP");
    void setTransformMatrices(const glm::mat4& M, const glm::mat4& VP, bool shadowMapping = false);
    void setShadowMappingShader(Shader* shader);
    void setCubemapShader(Shader* shader);
    void setShadowMaps(Texture* directionalShadowMap, Texture* spotShadowMaps[]);
    void setGlass(bool isGlass) { this->isGlass = isGlass; }

    /// Set texture of given name in material
    void setTexture(std::string name, Texture* value);
    /// Set float of given name in material
    void setFloat(std::string name, float value);
    /// Set vec2 of given name in material
    void setVec2(std::string name, glm::vec2 value);
    /// Set vec3 of given name in material
    void setVec3(std::string name, glm::vec3 value);
    /// Set vec4 of given name in material
    void setVec4(std::string name, glm::vec4 value);
    /// Set mat4 of given name in material
    void setMat4(std::string name, glm::mat4 value);

    /// Get texture of given name
    const Texture* getTexturePtr(std::string name);

    RenderType getRenderType() { return type; }

private:
    Shader* sceneShader = nullptr;
    Shader* shadowMappingShader = nullptr;
    Shader* cubemapShader = nullptr;
	uint ID;
    static uint idCount;
    bool enableInstancing;
    std::string name;
    RenderType type;
    bool isGlass = false;

    bool texturesChanged = true;
    std::map<std::string, Texture*> textures;
    bool floatsChanged = true;
    std::unordered_map<std::string, float> floats;
    bool vec2sChanged = true;
    std::unordered_map<std::string, glm::vec2> vec2s;
    bool vec3sChanged = true;
    std::unordered_map<std::string, glm::vec3> vec3s;
    bool vec4sChanged = true;
    std::unordered_map<std::string, glm::vec4> vec4s;
    bool mat4sChanged = true;
    std::unordered_map<std::string, glm::mat4> mat4s;
};