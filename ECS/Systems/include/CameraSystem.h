#pragma once

#include "../../Base/include/System.h"
#include "../../../Core/include/Module.inl"

#include <glm/glm.hpp>

struct Transform;
struct Camera;

class CameraSystem : public System, public Module
{
public:
	CameraSystem() = default;
	virtual ~CameraSystem() = default;

	static Camera* mainCamera;

	virtual void receiveMessage(Message msg);

	static bool setAsMain(Entity* entity);

	virtual bool assertEntity(Entity* entity);

	virtual void fixedUpdate();
	virtual void frameUpdate();

private:
	bool cameraChanged = true;
	bool freeCamera = false;
	Transform* transform;
	Camera* camera;

	float speed = 0.5f;
	float sensitivity = 0.1f;
	bool usingMouse = false;

	glm::vec3 movementVector = glm::vec3(0.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	float yaw = -90.0f;
	float pitch = 0.0f;
};

