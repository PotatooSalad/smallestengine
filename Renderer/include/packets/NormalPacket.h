#pragma once

#include "../RendererDataStructures.h"

/// <summary>
/// Regular rendering packet class
/// </summary>
struct NormalPacket : RenderPacket
{
	NormalPacket(Mesh* mesh, Material* material, glm::mat4 modelMatrix, bool window = false) : RenderPacket(mesh, material), modelMatrix(modelMatrix), window(window) {}
	NormalPacket() = default;
	virtual ~NormalPacket() = default;

	void render(glm::mat4 & VP, RenderMode mode, Texture* directionalShadowMap, Texture* spotShadowMaps[], Texture* irradianceMap, Texture* prefilteredMap, Texture* brdf);
	void renderTransparent(glm::mat4& VP, Texture* prefilteredMap);
	void renderWithShader(Shader * shader, glm::mat4 & VP);
	glm::mat4 modelMatrix;
	bool window = false;
};