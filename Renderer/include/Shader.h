#pragma once

#include <unordered_map>
#include <string>
#include "../../Core/include/globalDefs.h"
#include <glm/glm.hpp>

constexpr int NAME_BUF_SIZE = 64;

/// <summary>
/// Shader class
/// </summary>
class Shader
{
public:
	/// <summary>
	/// Shader class constructor
	/// </summary>
	/// <param name="fragmentShaderSource">Source of fragment shader</param>
	/// <param name="vertexShaderSource">Source of vertex shader</param>
	/// <param name="geometryShaderSource">Source of geometry shader (may be null)</param>
	Shader(const char* fragmentShaderSource, const char* vertexShaderSource, const char* geometryShaderSource = nullptr);
	~Shader();

	/// <summary>
	/// shader ID getter
	/// </summary>
	/// <returns>ID of compiled shader program</returns>
	int getShaderID() { return shaderID; }

	/// <summary>
	/// Switch the shader currently used by OpenGL to this
	/// </summary>
	void use();

	/// get shader attributes (?)
	std::unordered_map<std::string, uint>& getAttributes() { return attributes; }
	/// get shader uniforms
	std::unordered_map<std::string, uint>& getUniforms() { return uniforms; }

	/// set bool value in shader
	void setBool(const std::string& name, bool value) const;
	void setBool(const char* name, bool value) const;

	/// set int value in shader
	void setInt(const std::string& name, int value) const;
	void setInt(const char* name, int value) const;

	/// set float value in shader
	void setFloat(const std::string& name, float value) const;
	void setFloat(const char* name, float value) const;

	/// set vec2 value in shader
	void setVec2(const std::string& name, glm::vec2& vec) const;
	void setVec2(const char* name, glm::vec2& vec) const;

	/// set vec3 value in shader
	void setVec3(const std::string& name, float x, float y, float z) const;
	void setVec3(const std::string& name, const glm::vec3& vec) const;
	void setVec3(const char* name, float x, float y, float z) const;
	void setVec3(const char* name, const glm::vec3& vec) const;

	/// set vec4 value in shader
	void setVec4(const std::string& name, float x, float y, float z, float w) const;
	void setVec4(const std::string& name, const glm::vec4& vec) const;
	void setVec4(const char* name, float x, float y, float z, float w) const;
	void setVec4(const char* name, const glm::vec4& vec) const;

	/// set mat4 value in shader
	void setMat4(const std::string& name, const glm::mat4& mat) const;
	void setMat4(const char* name, const glm::mat4& mat) const;

	/// set mat4 value in shader
	void setMat3(const std::string& name, const glm::mat3& mat) const;
	void setMat3(const char* name, const glm::mat3& mat) const;

private:
	/// <summary>
	/// shader compilation method
	/// </summary>
	/// <param name="code"> code of shader</param>
	/// <param name="shaderType"> type of shader (eg. GL_VERTEX_SHADER)</param>
	/// <returns>id of compiled shader or -1 when smth went wrong</returns>
	int compileShader(const char* code, int shaderType);

	/// <summary>
	/// Retrieves info about shader variables - uniforms and attributes
	/// </summary>
	/// <param name="shaderInfoType">Variable type to get, should be GL_ACTIVE_ATTRIBUTES or GL_ACTIVE_UNIFORMS</param>
	/// <param name="buffer">A buffer to write to</param>
	void retrieveShaderInfo(uint shaderInfoType, std::unordered_map<std::string, uint>& buffer);

	/// <summary>
	/// id of shader program
	/// </summary>
	int shaderID;

	std::unordered_map<std::string, uint> attributes;
	std::unordered_map<std::string, uint> uniforms;
};